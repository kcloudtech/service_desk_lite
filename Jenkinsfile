properties([
  parameters([
    string(name: 'BRANCH', defaultValue: "${BRANCH_NAME}", description: 'Deploy a specific branch', )
   ])
])

pipeline {
  agent any

  stages {
    stage('Build') {
      steps {
        script {
            echo 'determine our environment'
            env.VAULT_ENV = 'dev'
            if (env.BRANCH_NAME == 'master') {
                env.VAULT_ENV = 'live'
            }
        }

        echo 'Composer Install'
        sshagent(credentials: ["kct_deployment_key"]) {
          sh '''COMPOSER=composer-deploy.json composer install --quiet --no-interaction --no-scripts --no-progress'''
        }

        echo('Building yarn assets')
        sh '''yarn install && yarn encore production'''
      }
    }

    stage('Pre Deploy') {
        steps {
            echo 'Getting configurations from vault'
            withCredentials([[$class: 'VaultTokenCredentialBinding', credentialsId: "kct_app_role", vaultAddr: "${VAULT_ADDRESS}"]]) {
                echo 'running console-template...'

                echo 'Getting deployment details'
                sh '''BRANCH=${BRANCH_NAME} /opt/consul-template/bin/consul-template -once -template "${WORKSPACE}/hosts.yml.ctmpl:${WORKSPACE}/hosts.yml" '''

                echo 'Getting application parameters'
                sh '''/opt/consul-template/bin/consul-template -once -template "${WORKSPACE}/.env.ctmpl:${WORKSPACE}/.env" '''
            }
        }
    }

    stage('Deploy') {
      steps {
        echo 'Deploying'
        sshagent(credentials: ["kct_deployment_key", "utils_deploy_key"]) {
           sh '''php ./bin/dep deploy prod -vvv'''
        }
      }
    }
  }

  post {
      always {
          echo 'Cleaning up'
          sh '''rm -f hosts.yml .env'''
          echo 'POST TO SLACK - @todo'
      }
  }
}
