<?php

namespace Deployer;

require 'recipe/symfony4.php';

// Settings

// Project name
set('application', 'service-desk-lite');

// Project repository
set('repository', 'https://kcloudtech@bitbucket.org/kcloudtech/service_desk_lite.git');

// Get the deployment server and settings from an external file - provided by jenkins
inventory('hosts.yml');

// Shared files/dirs between deploys
add('shared_files', []);
add('shared_dirs', [
    'public/uploads/user',
]);

// Writable dirs by web server
add('writable_dirs', [
    'var/log',
    'var/cache',
    'public/uploads/user',
]);

// set env vars for all deployer tasks
set('env', [
    'APP_ENV' => 'prod',
    'APP_DEBUG' => 0
]);

// do not send telemetry data to deployer devs
set('allow_anonymous_stats', false);

// Releases to keep
set('keep_releases', 3);

// Hooks - functions to call before/after deployment stages
after('deploy:update_code', 'config:prepare');
after('deploy', 'database:migrate');
after('deploy', 'fpm:restart');
after('deploy:failed', 'deploy:unlock');
after('rollback', 'fpm:restart');
after('rollback', 'deploy:unlock');

//use symfony performance recommendations on composer tasks
set('composer_options', '{{composer_action}} --prefer-dist --no-progress --no-interaction --no-suggest --no-dev --classmap-authoritative --optimize-autoloader');

// Tasks
// reload php-fpm after we make file changes to ensure they are picked up
task("fpm:restart", static function (){
    run('sudo service php-fpm restart');
});


// move our generated config and assets prior to composer install tasks
task('config:prepare', static function() {
    writeln('uploading the .env file');
    upload(__DIR__ . '/.env', '{{ release_path}}/.env');
    writeln('uploading the yarn assets');
    upload(__DIR__ . '/public/build', '{{ release_path}}/public');
});
