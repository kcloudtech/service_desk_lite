<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201212235928 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'base project entities for user and application ';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE ext_log_entries (id INT AUTO_INCREMENT NOT NULL, action VARCHAR(8) NOT NULL, logged_at DATETIME NOT NULL, object_id VARCHAR(64) DEFAULT NULL, object_class VARCHAR(255) NOT NULL, version INT NOT NULL, data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', username VARCHAR(255) DEFAULT NULL, INDEX log_class_lookup_idx (object_class), INDEX log_date_lookup_idx (logged_at), INDEX log_user_lookup_idx (username), INDEX log_version_lookup_idx (object_id, object_class, version), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB ROW_FORMAT = DYNAMIC');
        $this->addSql('CREATE TABLE menu_item (menu_item_id INT AUTO_INCREMENT NOT NULL, parent_menu_item_id INT DEFAULT NULL, name VARCHAR(64) NOT NULL, route VARCHAR(64) NOT NULL, params LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\',icon VARCHAR(32) DEFAULT NULL, menu_order INT NOT NULL, INDEX IDX_D754D550CADA12CE (parent_menu_item_id), PRIMARY KEY(menu_item_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE menu_item_translation (translation_id INT AUTO_INCREMENT NOT NULL, menu_item_id INT NOT NULL, locale VARCHAR(8) NOT NULL, field VARCHAR(32) NOT NULL, content LONGTEXT DEFAULT NULL, INDEX IDX_683EE3A69AB44FE0 (menu_item_id), UNIQUE INDEX lookup_unique_idx (locale, menu_item_id, field), PRIMARY KEY(translation_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE permission (permission_id INT AUTO_INCREMENT NOT NULL, role_id INT NOT NULL, page_id INT NOT NULL, can_create TINYINT(1) NOT NULL, can_read TINYINT(1) NOT NULL, can_update TINYINT(1) NOT NULL, can_delete TINYINT(1) NOT NULL, INDEX IDX_E04992AAD60322AC (role_id), INDEX IDX_E04992AAC4663E4 (page_id), UNIQUE INDEX unique_permission_idx (role_id, page_id), PRIMARY KEY(permission_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(128) NOT NULL, archived DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE secure_page (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(40) NOT NULL, alias VARCHAR(40) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE translation (translation_id INT AUTO_INCREMENT NOT NULL, domain VARCHAR(64) NOT NULL, locale VARCHAR(8) NOT NULL, translation_key VARCHAR(128) NOT NULL, content LONGTEXT NOT NULL, archived DATETIME DEFAULT NULL, UNIQUE INDEX unique_entity_idx (domain, locale, translation_key), PRIMARY KEY(translation_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, role_id INT NOT NULL, created DATETIME NOT NULL, updated DATETIME DEFAULT NULL, googleAuthenticatorSecret VARCHAR(255) DEFAULT NULL, totp_confirmed TINYINT(1) DEFAULT \'0\' NOT NULL, profile_picture VARCHAR(128) DEFAULT NULL, first_name VARCHAR(128) NOT NULL, last_name VARCHAR(128) NOT NULL, email VARCHAR(128) NOT NULL, password VARCHAR(128) NOT NULL, last_login DATETIME DEFAULT NULL, last_failed_login DATETIME DEFAULT NULL, failed_login_attempts INT DEFAULT NULL, token VARCHAR(20) DEFAULT NULL, token_expires DATETIME DEFAULT NULL, is_active TINYINT(1) NOT NULL, archived DATETIME DEFAULT NULL, INDEX IDX_8D93D649D60322AC (role_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_permission (user_permission_id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, page_id INT NOT NULL, can_create TINYINT(1) NOT NULL, can_read TINYINT(1) NOT NULL, can_update TINYINT(1) NOT NULL, can_delete TINYINT(1) NOT NULL, INDEX IDX_472E5446A76ED395 (user_id), INDEX IDX_472E5446C4663E4 (page_id), UNIQUE INDEX unique_user_permission_idx (user_id, page_id), PRIMARY KEY(user_permission_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL, available_at DATETIME NOT NULL, delivered_at DATETIME DEFAULT NULL, INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE menu_item ADD CONSTRAINT FK_D754D550CADA12CE FOREIGN KEY (parent_menu_item_id) REFERENCES menu_item (menu_item_id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE menu_item_translation ADD CONSTRAINT FK_683EE3A69AB44FE0 FOREIGN KEY (menu_item_id) REFERENCES menu_item (menu_item_id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE permission ADD CONSTRAINT FK_E04992AAD60322AC FOREIGN KEY (role_id) REFERENCES role (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE permission ADD CONSTRAINT FK_E04992AAC4663E4 FOREIGN KEY (page_id) REFERENCES secure_page (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649D60322AC FOREIGN KEY (role_id) REFERENCES role (id)');
        $this->addSql('ALTER TABLE user_permission ADD CONSTRAINT FK_472E5446A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_permission ADD CONSTRAINT FK_472E5446C4663E4 FOREIGN KEY (page_id) REFERENCES secure_page (id) ON DELETE CASCADE');
        $this->addSql('CREATE TABLE sessions (`sess_id` VARCHAR(128) NOT NULL PRIMARY KEY, `sess_data` BLOB NOT NULL,  `sess_time` INTEGER UNSIGNED NOT NULL,   `sess_lifetime` MEDIUMINT NOT NULL) COLLATE utf8_bin, ENGINE = InnoDB;');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE menu_item DROP FOREIGN KEY FK_D754D550CADA12CE');
        $this->addSql('ALTER TABLE menu_item_translation DROP FOREIGN KEY FK_683EE3A69AB44FE0');
        $this->addSql('ALTER TABLE permission DROP FOREIGN KEY FK_E04992AAD60322AC');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649D60322AC');
        $this->addSql('ALTER TABLE permission DROP FOREIGN KEY FK_E04992AAC4663E4');
        $this->addSql('ALTER TABLE user_permission DROP FOREIGN KEY FK_472E5446C4663E4');
        $this->addSql('ALTER TABLE user_permission DROP FOREIGN KEY FK_472E5446A76ED395');
        $this->addSql('DROP TABLE ext_log_entries');
        $this->addSql('DROP TABLE menu_item');
        $this->addSql('DROP TABLE menu_item_translation');
        $this->addSql('DROP TABLE permission');
        $this->addSql('DROP TABLE role');
        $this->addSql('DROP TABLE secure_page');
        $this->addSql('DROP TABLE translation');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_permission');
        $this->addSql('DROP TABLE messenger_messages');
        $this->addSql('DROP TABLE sessions');
    }
}
