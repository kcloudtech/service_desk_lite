<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210110181140 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Added contact and customer table and unique key to user email';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE contact (id INT AUTO_INCREMENT NOT NULL, customer_id INT NOT NULL, first_name VARCHAR(64) NOT NULL, last_name VARCHAR(64) NOT NULL, role VARCHAR(64) NOT NULL, landline VARCHAR(12) DEFAULT NULL, mobile VARCHAR(12) DEFAULT NULL, email VARCHAR(128) DEFAULT NULL, archived DATETIME DEFAULT NULL, INDEX IDX_4C62E6389395C3F3 (customer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE customer (id INT AUTO_INCREMENT NOT NULL, created DATETIME NOT NULL, updated DATETIME DEFAULT NULL, name VARCHAR(128) NOT NULL, active TINYINT(1) NOT NULL, archived DATETIME DEFAULT NULL, address1 VARCHAR(128) NOT NULL, address2 VARCHAR(128) DEFAULT NULL, town_or_city VARCHAR(64) NOT NULL, county VARCHAR(64) NOT NULL, postcode VARCHAR(9) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE contact ADD CONSTRAINT FK_4C62E6389395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON user (email)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE contact DROP FOREIGN KEY FK_4C62E6389395C3F3');
        $this->addSql('DROP TABLE contact');
        $this->addSql('DROP TABLE customer');
        $this->addSql('DROP INDEX UNIQ_8D93D649E7927C74 ON user');
    }
}
