<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210110181445 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Added status and progress points';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE progress_point (id INT AUTO_INCREMENT NOT NULL, description VARCHAR(32) NOT NULL, position INT NOT NULL, archived DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE progress_point_translation (translation_id INT AUTO_INCREMENT NOT NULL, progress_point_id INT NOT NULL, content LONGTEXT DEFAULT NULL, locale VARCHAR(8) NOT NULL, field VARCHAR(32) NOT NULL, INDEX IDX_7ECD8B7D123D4DF1 (progress_point_id), UNIQUE INDEX lookup_unique_idx (locale, progress_point_id, field), PRIMARY KEY(translation_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE status (id INT AUTO_INCREMENT NOT NULL, progress_point_id INT NOT NULL, description VARCHAR(32) NOT NULL, archived DATETIME DEFAULT NULL, INDEX IDX_7B00651C123D4DF1 (progress_point_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE status_translation (translation_id INT AUTO_INCREMENT NOT NULL, status_id INT NOT NULL, content LONGTEXT DEFAULT NULL, locale VARCHAR(8) NOT NULL, field VARCHAR(32) NOT NULL, INDEX IDX_B0B3309E6BF700BD (status_id), UNIQUE INDEX lookup_unique_idx (locale, status_id, field), PRIMARY KEY(translation_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE progress_point_translation ADD CONSTRAINT FK_7ECD8B7D123D4DF1 FOREIGN KEY (progress_point_id) REFERENCES status (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE status ADD CONSTRAINT FK_7B00651C123D4DF1 FOREIGN KEY (progress_point_id) REFERENCES progress_point (id)');
        $this->addSql('ALTER TABLE status_translation ADD CONSTRAINT FK_B0B3309E6BF700BD FOREIGN KEY (status_id) REFERENCES status (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE status DROP FOREIGN KEY FK_7B00651C123D4DF1');
        $this->addSql('ALTER TABLE progress_point_translation DROP FOREIGN KEY FK_7ECD8B7D123D4DF1');
        $this->addSql('ALTER TABLE status_translation DROP FOREIGN KEY FK_B0B3309E6BF700BD');
        $this->addSql('DROP TABLE progress_point');
        $this->addSql('DROP TABLE progress_point_translation');
        $this->addSql('DROP TABLE status');
        $this->addSql('DROP TABLE status_translation');

    }
}
