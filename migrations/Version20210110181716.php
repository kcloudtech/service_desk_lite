<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210110181716 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Added tickets, attachments, issue types and messages';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE attachment (id INT AUTO_INCREMENT NOT NULL, ticket_id INT NOT NULL, created DATETIME NOT NULL, updated DATETIME DEFAULT NULL, file VARCHAR(255) NOT NULL, filename VARCHAR(64) NOT NULL, size VARCHAR(8) NOT NULL, archived DATETIME DEFAULT NULL, INDEX IDX_795FD9BB700047D2 (ticket_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE issue_type (id INT AUTO_INCREMENT NOT NULL, description VARCHAR(64) NOT NULL, icon VARCHAR(32) NOT NULL, archived DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE message (id INT AUTO_INCREMENT NOT NULL, ticket_id INT NOT NULL, created DATETIME NOT NULL, user_id INT NOT NULL, username VARCHAR(128) NOT NULL, content VARCHAR(255) NOT NULL, internal_only TINYINT(1) NOT NULL, archived DATETIME DEFAULT NULL, INDEX IDX_B6BD307F700047D2 (ticket_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ticket (id INT AUTO_INCREMENT NOT NULL, customer_id INT NOT NULL, assigned_to_id INT NOT NULL, reporter_id INT NOT NULL, issue_type_id INT NOT NULL, status_id INT NOT NULL, created DATETIME NOT NULL, updated DATETIME DEFAULT NULL, subject VARCHAR(64) NOT NULL, description LONGTEXT NOT NULL, priority INT NOT NULL, start_by DATETIME DEFAULT NULL, due_by DATETIME DEFAULT NULL, archived DATETIME DEFAULT NULL, INDEX IDX_97A0ADA39395C3F3 (customer_id), INDEX IDX_97A0ADA3F4BD7827 (assigned_to_id), INDEX IDX_97A0ADA3E1CFE6F5 (reporter_id), INDEX IDX_97A0ADA360B4C972 (issue_type_id), INDEX IDX_97A0ADA36BF700BD (status_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE attachment ADD CONSTRAINT FK_795FD9BB700047D2 FOREIGN KEY (ticket_id) REFERENCES ticket (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F700047D2 FOREIGN KEY (ticket_id) REFERENCES ticket (id)');
        $this->addSql('ALTER TABLE ticket ADD CONSTRAINT FK_97A0ADA39395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id)');
        $this->addSql('ALTER TABLE ticket ADD CONSTRAINT FK_97A0ADA3F4BD7827 FOREIGN KEY (assigned_to_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE ticket ADD CONSTRAINT FK_97A0ADA3E1CFE6F5 FOREIGN KEY (reporter_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE ticket ADD CONSTRAINT FK_97A0ADA360B4C972 FOREIGN KEY (issue_type_id) REFERENCES issue_type (id)');
        $this->addSql('ALTER TABLE ticket ADD CONSTRAINT FK_97A0ADA36BF700BD FOREIGN KEY (status_id) REFERENCES status (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ticket DROP FOREIGN KEY FK_97A0ADA360B4C972');
        $this->addSql('ALTER TABLE attachment DROP FOREIGN KEY FK_795FD9BB700047D2');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307F700047D2');
        $this->addSql('DROP TABLE attachment');
        $this->addSql('DROP TABLE issue_type');
        $this->addSql('DROP TABLE message');
        $this->addSql('DROP TABLE ticket');
    }
}
