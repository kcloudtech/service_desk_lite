## A Simple Demo Project

This project is for the purpose of demonstrating skill set. It's a very simple ticket system that uses a tailored implementation of MVC, MVVM and the Repository design patterns.

### NOTE

This project uses a licenced bootstrap UI kit that has been excluded from the public repo. It also uses two private packages so it would not be possible to clone and install this project to see it in action. 

#### Disclaimer
This is a simple project that was done over the course of weekend and is not fully finished and only has tests with limited coverage so I do not expect it to be perfect.

### Screenshots

![Login](demoImages/login.png)
![Login](demoImages/customer_view.png)
![Login](demoImages/view_ticket.png)
![Login](demoImages/list_view.png)
![Login](demoImages/user_form.png)
![Login](demoImages/user_form_2.png)
![Login](demoImages/user_list.png)
