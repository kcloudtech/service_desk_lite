<?php

namespace App\Controller;

use Exception;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Utils\FormType;
use Utils\MessageTypes;
use App\Services\UserService;
use App\ViewModels\User\ForgottenPasswordVM;
use App\ViewModels\User\RegistrationVM;
use App\ViewModels\User\ResetPasswordVM;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class AuthController
 *
 * Provides authentication endpoints
 *
 * @author Kellsey
 */
class AuthController extends AbstractController
{
    /**
     * @var UserService
     */
    private $service;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * UserController constructor.
     * @param UserService $service
     * @param LoggerInterface $logger
     */
    public function __construct(UserService $service, LoggerInterface $logger)
    {
        $this->service = $service;
        $this->logger = $logger;
    }

    /**
     * @Route("/login", name="login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();


        if (isset($error) && !empty($lastUsername)) {
            if ($error->getMessage() === 'Bad credentials.') {
                $error = 'The username or password entered was incorrect';
            } elseif ($error->getMessage() === 'User account is disabled.') {
                $error = 'Sorry, your account is inactive';
            } else {
                $this->logger->error($error);
                $error = 'Sorry, we seem to have run into a snag trying to authenticate you';
            }
        }

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout(): void
    {
        //doesn't matter because it's handled by symfony
    }

    /**
     * @Route("/forgotten-password", name="forgotten_password")
     *
     * @param Request $request
     * @param ForgottenPasswordVM $vm
     * @return RedirectResponse|Response
     */
    public function forgottenPassword(Request $request, ForgottenPasswordVM $vm)
    {
        $form = $this->createForm(FormType::class, $vm);
        $form->add('Reset', SubmitType::class);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            try {
                $this->service->generateResetToken($vm->getEmail(), true);
                    return $this->redirectToRoute('resetting_password');
            } catch (Exception | TransportExceptionInterface $e) {
                $this->logger->error($e);
            }
            $this->addFlash(MessageTypes::ErrorMessage,'Sorry, we seem to have run into a snag processing your request');
        }
        return $this->render('security/reset.html.twig',['form' => $form->createView()]);
    }

    /**
     * @Route("/resetting", name="resetting_password")
     */
    public function resetting(): Response
    {
        return $this->render('security/check_email.html.twig');
    }

    /**
     * @Route("/reset/{token}", name="reset_password")
     * @param Request $request
     * @param string $token
     * @param ResetPasswordVM $vm
     * @return RedirectResponse|Response
     */
    public function resetPassword(Request $request, string $token, ResetPasswordVM $vm)
    {
        if($this->service->validateToken($token)){
            $form = $this->createForm(FormType::class, $vm);
            $form->add('Reset', SubmitType::class);

            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()) {
                try {
                    if($this->service->resetPassword($vm, $token)) {
                        $this->addFlash(MessageTypes::SuccessMessage, 'Your password has been updated');
                        return $this->redirectToRoute('login');
                    }
                } catch (Exception $e) {
                    $this->logger->error($e);
                }
                $this->addFlash(MessageTypes::ErrorMessage,'Sorry, we seem to have run into a snag processing your request');
            }
            return $this->render('security/change_password.html.twig', ['form' => $form->createView()]);

        }
        return $this->render('security/token_invalid.html.twig');
    }

    /**
     * @Route("/connect/{type}", name="connect_start")
     * @param ClientRegistry $clientRegistry
     * @param string $type
     * @return RedirectResponse
     */
    public function connectAction(ClientRegistry $clientRegistry, string $type): RedirectResponse
    {
        $scope = ['email'];
        switch ($type){
            case 'microsoft':
                $scope = [$_ENV['MS_SCOPE']];
                break;
            case 'linkedin':
                $scope = explode(',',$_ENV['LINKEDIN_SCOPE']);
                break;
        }

        return $clientRegistry->getClient($type)->redirect($scope, []);
    }

    /**
     * @Route("/connect/facebook/check", name="connect_facebook_check")
     * @Route("/connect/google/check", name="connect_google_check")
     * @Route("/connect/linkedin/check", name="connect_linkedin_check")
     * @Route("/connect/microsoft/check", name="connect_microsoft_check")
     * @return RedirectResponse
     */
    public function connectCheckAction(): RedirectResponse
    {
        return $this->redirectToRoute('home');
    }

}