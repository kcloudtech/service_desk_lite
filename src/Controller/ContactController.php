<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Utils\FormType;
use Utils\MessageTypes;
use App\ViewModels\TableFilter;
use App\Services\ContactService;
use App\ViewModels\Contact\ContactListVM;
use App\ViewModels\Contact\ContactVM;
use Psr\Log\LoggerInterface;
use App\ViewModels\HistoryListVM;
use App\Entity\Contact;

/**
 * Class ContactController
 * @author kells
 * @package Controller
 *
 */
class ContactController extends AbstractController
{
    /**
     * @var ContactService
     */
    private $service;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * ContactControllerController constructor.
     * @param ContactService $service
     * @param LoggerInterface $logger
     */
    public function __construct(ContactService $service, LoggerInterface $logger)
    {
        $this->service = $service;
        $this->logger = $logger;
    }


    /**
     * @Route("contact/", name="list_contacts")
     * @Security("is_granted('read', 'contact')")
     */
    public function index(): Response
    {
        $results = $this->service->fetchAll();
        return $this->render('@generic/generic_list.html.twig', [
            'rows' => $results,
            'headers' => ContactListVM::getHeaders(),
            'title' => 'Contact',
            'new_path' => 'new_contact',
            'new_text' => 'New Contact',
            'center_columns' => [],
            'table_filters_title' => 'Filters',
            'table_filters' => [
                //new TableFilter('filter_id', 'filter_icon', 'filter_title', 0,'filter_by_value'),
            ],
            'auth_object' => 'contact',
            'excluded_delete_ids' => [],
            'excluded_edit_ids' => [],
            'recycle_path' => 'list_archived_contacts',
            'is_versioned' => true
        ]);
    }

    /**
     * @Route("contact/new", name="new_contact")
     * @Security("is_granted('create', 'contact')")
     * @param Request $request
     * @param ContactVM $vm
     * @return Response
     */
    public function add(Request $request, ContactVM $vm): Response
    {
        $form = $this->createForm(FormType::class, $vm);
        $form->add('Save', SubmitType::class, ['attr' => ['class' => 'btn btn-primary float-right']]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($this->service->addContact($vm)) {
                    return $this->redirectToRoute('list_contacts');
                }
            } catch (\Exception $e) {
                $this->logger->error($e);
            }
            $this->addFlash(MessageTypes::ErrorMessage, 'Sorry, we seem to have run into a snag');
        }

        return $this->render('@generic/generic_form.html.twig', [
            'form' => $form->createView(),
            'title' => 'New Contact'
        ]);
    }

    /**
     * @Route("contact/update/{hashid}", name="update_contact")
     * @Security("is_granted('update', 'contact')")
     * @param Request $request
     * @param ContactVM $vm
     * @return Response
     */
    public function update(Request $request, ContactVM $vm): Response
    {

        if ($vm->getId() <= 0) {
            $this->addFlash(MessageTypes::WarningMessage, "Sorry, we couldn't find that record");
            return $this->redirectToRoute('list_contacts');
        }

        $form = $this->createForm(FormType::class, $vm);
        $form->add('Save', SubmitType::class, ['attr' => ['class' => 'btn btn-primary float-right']]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($this->service->updateContact($vm)) {
                    return $this->redirectToRoute('list_contacts');
                }
            } catch (\Exception $e) {
                $this->logger->error($e);
            }
            $this->addFlash(MessageTypes::ErrorMessage, 'Sorry, we seem to have run into a snag');
        }

        return $this->render('@generic/generic_form.html.twig', [
            'form' => $form->createView(),
            'title' => 'Update Contact'
        ]);
    }

    /**
     * @Route("contact/delete/{hashid}", name="delete_contact")
     * @Security("is_granted('delete', 'contact')")
     * @param int $hashid
     * @return Response
     */
    public function delete(int $hashid): Response
    {
        try {
            if ($this->service->deleteContact($hashid)) {
                $this->addFlash(MessageTypes::SuccessMessage, 'The contact has been deleted');
            }
        } catch (\Exception $e) {
            $this->logger->error($e);
            $this->addFlash(MessageTypes::ErrorMessage, 'Sorry, we seem to have run into a snag');
        }
        return $this->redirectToRoute('list_contacts');
    }

    /**
     * @Route("contact/archived", name="list_archived_contacts")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     */
    public function archived()
    {
        $results = ContactListVM::mapCollection($this->service->fetchArchived(Contact::class));

        if ($results === null) {
            $this->addFlash(MessageTypes::WarningMessage, 'No history found');
            return $this->redirectToRoute('list_contacts');
        }

        return $this->render('@generic/generic_list.html.twig', [
            'rows' => $results,
            'headers' => ContactListVM::getHeaders(),
            'title' => 'Recycle Bin',
            'entity' => 'contact',
            'center_columns' => [''],
        ]);
    }

    /**
     * @Route("contact/restore/{hashid}", name="restore_contact")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param int $hashid
     * @return Response
     */
    public function restore(int $hashid): Response
    {
        if ($this->service->restore($hashid, Contact::class)) {
            $this->addFlash(MessageTypes::SuccessMessage, 'The contact has been restored');
        }
        return $this->redirectToRoute('list_archived_contacts');
    }

    /**
     * @Route("contact/history/{hashid}", name="contact_history")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param int $hashid
     * @return Response
     */
    public function history(int $hashid): Response
    {
        $results = $this->service->fetchHistory($hashid, Contact::class);

        return $this->render('@generic/generic_list.html.twig', [
            'rows' => $results,
            'headers' => HistoryListVM::getHeaders(),
            'title' => 'History',
            'entity' => 'contact',
            'center_columns' => ['version'],
        ]);
    }

    /**
     * @Route("contact/revert/{hashid}/{version}", name="revert_contact")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param int $hashid
     * @param int $version
     * @return Response
     */
    public function revert(int $hashid, int $version): Response
    {
        try {
            if ($this->service->revert($hashid, $version, Contact::class)) {
                $this->addFlash(MessageTypes::SuccessMessage, 'The contact has been reverted');
            }
        } catch (\Exception $e) {
            $this->logger->error($e);
            $this->addFlash(MessageTypes::ErrorMessage,
                'Sorry, we seem to have run into a snag processing your request');
        }
        return $this->redirectToRoute('list_contacts');
    }
}