<?php

namespace App\Controller;

use App\Helpers\CountyHelper;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Utils\FormType;
use Utils\MessageTypes;
use App\ViewModels\TableFilter;
use App\Services\CustomerService;
use App\ViewModels\Customer\CustomerListVM;
use App\ViewModels\Customer\CustomerVM;
use Psr\Log\LoggerInterface;
use App\ViewModels\HistoryListVM;
use App\Entity\Customer;

/**
 * Class CustomerController
 * @author kells
 * @package Controller
 *
 */
class CustomerController extends AbstractController
{
    /**
     * @var CustomerService
     */
    private $service;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * CustomerControllerController constructor.
     * @param CustomerService $service
     * @param LoggerInterface $logger
     */
    public function __construct(CustomerService $service, LoggerInterface $logger)
    {
        $this->service = $service;
        $this->logger = $logger;
    }

    /**
     * @Route("customer/", name="list_customers")
     * @Security("is_granted('read', 'customer')")
     */
    public function index(): Response
    {
        $results = $this->service->fetchAll();
        return $this->render('@generic/generic_list.html.twig', [
            'rows' => $results,
            'headers' => CustomerListVM::getHeaders(),
            'title' => 'Customer',
            'new_path' => 'new_customer',
            'new_text' => 'New Customer',
            'center_columns' => ['active', 'contacts', 'products', 'tickets'],
            'table_filters_title' => 'Filters',
            'table_filters' => [
                //new TableFilter('filter_id', 'filter_icon', 'filter_title', 0,'filter_by_value'),
            ],
            'auth_object' => 'customer',
            'excluded_delete_ids' => [],
            'excluded_edit_ids' => [],
            'recycle_path' => 'list_archived_customers',
            'is_versioned' => true
        ]);
    }

    /**
     * @Route("customer/view/{hashid}", name="view_customer")
     * @Security("is_granted('read', 'customer')")
     * @param CustomerVM $vm
     * @return Response
     */
    public function view(CustomerVM $vm): Response
    {
        return $this->render('customer/view.html.twig', [
            'title' => ucwords($vm->getName()),
            'customer' => $vm
        ]);
    }

    /**
     * @Route("customer/new", name="new_customer")
     * @Security("is_granted('create', 'customer')")
     * @param Request $request
     * @param CustomerVM $vm
     * @return Response
     */
    public function add(Request $request, CustomerVM $vm): Response
    {
        $form = $this->createForm(FormType::class, $vm);
        $form->add('Save', SubmitType::class, ['attr' => ['class' => 'btn btn-primary float-right']]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($this->service->addCustomer($vm)) {
                    return $this->redirectToRoute('list_customers');
                }
            } catch (Exception $e) {
                $this->logger->error($e);
            }
            $this->addFlash(MessageTypes::ErrorMessage, 'Sorry, we seem to have run into a snag');
        }

        return $this->render('customer/form.html.twig', [
            'form' => $form->createView(),
            'title' => 'New Customer',
            'values' => CountyHelper::getCountyList()
        ]);
    }

    /**
     * @Route("customer/update/{hashid}", name="update_customer")
     * @Security("is_granted('update', 'customer')")
     * @param Request $request
     * @param CustomerVM $vm
     * @return Response
     */
    public function update(Request $request, CustomerVM $vm): Response
    {
        if ($vm->getId() <= 0) {
            $this->addFlash(MessageTypes::WarningMessage, "Sorry, we couldn't find that record");
            return $this->redirectToRoute('list_customers');
        }

        $vm->mapFrom($vm->getAddress());

        $form = $this->createForm(FormType::class, $vm);
        $form->add('Save', SubmitType::class, ['attr' => ['class' => 'btn btn-primary float-right']]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($this->service->updateCustomer($vm)) {
                    return $this->redirectToRoute('list_customers');
                }
            } catch (Exception $e) {
                $this->logger->error($e);
            }
            $this->addFlash(MessageTypes::ErrorMessage, 'Sorry, we seem to have run into a snag');
        }

        return $this->render('customer/form.html.twig', [
            'form' => $form->createView(),
            'title' => 'Update Customer',
            'values' => CountyHelper::getCountyList()
        ]);
    }

    /**
     * @Route("customer/delete/{hashid}", name="delete_customer")
     * @Security("is_granted('delete', 'customer')")
     * @param int $hashid
     * @return Response
     */
    public function delete(int $hashid): Response
    {
        try {
            if ($this->service->deleteCustomer($hashid)) {
                $this->addFlash(MessageTypes::SuccessMessage, 'The customer has been deleted');
            }
        } catch (Exception $e) {
            $this->logger->error($e);
            $this->addFlash(MessageTypes::ErrorMessage, 'Sorry, we seem to have run into a snag');
        }
        return $this->redirectToRoute('list_customers');
    }

    /**
     * @Route("customer/archived", name="list_archived_customers")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     */
    public function archived()
    {
        $results = CustomerListVM::mapCollection($this->service->fetchArchived(Customer::class));

        if ($results === null) {
            $this->addFlash(MessageTypes::WarningMessage, 'No history found');
            return $this->redirectToRoute('list_customers');
        }

        return $this->render('@generic/generic_list.html.twig', [
            'rows' => $results,
            'headers' => CustomerListVM::getHeaders(),
            'title' => 'Recycle Bin',
            'entity' => 'customer',
            'center_columns' => ['active', 'contacts', 'products', 'tickets'],
        ]);
    }

    /**
     * @Route("customer/restore/{hashid}", name="restore_customer")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param int $hashid
     * @return Response
     */
    public function restore(int $hashid): Response
    {
        if ($this->service->restore($hashid, Customer::class)) {
            $this->addFlash(MessageTypes::SuccessMessage, 'The customer has been restored');
        }
        return $this->redirectToRoute('list_archived_customers');
    }

    /**
     * @Route("customer/history/{hashid}", name="customer_history")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param int $hashid
     * @return Response
     */
    public function history(int $hashid): Response
    {
        $results = $this->service->fetchHistory($hashid, Customer::class);

        return $this->render('@generic/generic_list.html.twig', [
            'rows' => $results,
            'headers' => HistoryListVM::getHeaders(),
            'title' => 'History',
            'entity' => 'customer',
            'center_columns' => ['version'],
        ]);
    }

    /**
     * @Route("customer/revert/{hashid}/{version}", name="revert_customer")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param int $hashid
     * @param int $version
     * @return Response
     */
    public function revert(int $hashid, int $version): Response
    {
        try {
            if ($this->service->revert($hashid, $version, Customer::class)) {
                $this->addFlash(MessageTypes::SuccessMessage, 'The customer has been reverted');
            }
        } catch (Exception $e) {
            $this->logger->error($e);
            $this->addFlash(MessageTypes::ErrorMessage,
                'Sorry, we seem to have run into a snag processing your request');
        }
        return $this->redirectToRoute('list_customers');
    }
}