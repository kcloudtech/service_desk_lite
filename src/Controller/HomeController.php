<?php

namespace App\Controller;

use Scheb\TwoFactorBundle\Security\TwoFactor\Provider\Google\GoogleAuthenticator;
use Utils\FormType;
use Utils\MessageTypes;
use App\Services\HomeService;
use App\ViewModels\User\ChangePasswordVM;
use App\ViewModels\User\Enable2FaVM;
use App\ViewModels\User\UserProfileVM;
use App\Entity\User;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\ExpressionLanguage\Expression;

/**
 * Class HomeController
 *
 * Endpoint for dashboard, settings and user profile
 *
 * @author Kellsey
 *
 * @IsGranted("IS_AUTHENTICATED_FULLY")
 */
class HomeController extends AbstractController
{

    /**
     * @var HomeService
     */
    private $service;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var GoogleAuthenticator
     */
    private $authenticator;

    /**
     * HomeController constructor.
     * @param HomeService $service
     * @param LoggerInterface $logger
     * @param GoogleAuthenticator $authenticator
     */
    public function __construct(HomeService $service, LoggerInterface $logger, GoogleAuthenticator $authenticator)
    {
        $this->service = $service;
        $this->logger = $logger;
        $this->authenticator = $authenticator;
    }

    /**
     * @Route("/", name="home")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        return $this->render('home/dashboard.html.twig', [
            'title' => 'Home',

        ]);
    }

    /**
     * @Route("/change-password", name="change_password")
     *
     * @param Request $request
     * @param ChangePasswordVM $vm
     * @return RedirectResponse|Response
     */
    public function changePassword(Request $request, ChangePasswordVM $vm)
    {
        $form = $this->createForm(FormType::class, $vm);
        $form->add('Save', SubmitType::class, ['attr' => ['class' => 'btn btn-primary float-right']]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($this->service->changePassword($vm->setId($this->getUser()->getId()))) {
                    $this->addFlash(MessageTypes::SuccessMessage,'Your password has been changed');
                    return $this->redirectToRoute('home');
                }
            } catch (\Exception $e) {
                $this->logger->error($e);
            }
            $this->addFlash(MessageTypes::ErrorMessage, 'Sorry, we seem to have run into a snag');
        }

        return $this->render('@generic/generic_form.html.twig', [
            'form' => $form->createView(),
            'title' => 'Change Password',
            'sub_heading' => 'The password must include at least one letter, one number and have at least one capital letter. If you don\'t know your password or you used a social login, you can logout and press reset password',
        ]);
    }

    /**
     * @Route("/profile/{hashid}", name="user_profile")
     * @param Request $request
     * @param UserProfileVM $vm
     * @return RedirectResponse|Response
     */
    public function profile(Request $request, UserProfileVM $vm)
    {
        if(!$this->isGranted(new Expression('user.getId() === object'),$vm->getId()))
        {
            return $this->redirectToRoute('home');
        }

        $form = $this->createForm(FormType::class, $vm);
        $form->add('Save', SubmitType::class, ['attr' => ['class' => 'btn btn-primary float-right']]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                //Check if all users details are being overwritten
                /** @var User $user */
                $user = $this->getUser();
                if($user->getFirstName() !== $vm->getFirstName()
                && $user->getLastName() !== $vm->getLastName()
                && $user->getEmail() !== $vm->getEmail() ){
                    $this->addFlash(MessageTypes::ErrorMessage, 'Users cannot be overwritten');
                    return $this->redirectToRoute('home');
                }

                if ($this->service->updateUser($vm->setId($user->getId())->setUpdated(date_create()))) {
                    $this->addFlash(MessageTypes::SuccessMessage, 'Your details have been updated');
                    return $this->redirectToRoute('home');
                }
            } catch (\Exception $e) {
                $this->logger->error($e);
            }
            $this->addFlash(MessageTypes::ErrorMessage, 'Sorry, we seem to have run into a snag');
        }

        return $this->render('@generic/generic_form.html.twig', [
            'form' => $form->createView(),
            'title' => 'My Profile',
            'sub_heading' => 'Note: you may update your details for a change of name, typos and contact information but you cannot change all your details',
        ]);
    }

    /**
     * @Route("/enable-2fa", name="enable_2fa")
     * @param Enable2FaVM $vm
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function enable2Fa(Enable2FaVM $vm, Request $request)
    {
        /** @var User $user */
        $user = $this->service->get($this->getUser()->getId(), User::class);

        //reset the user's code
        if(($request->getMethod() === Request::METHOD_GET) && empty($user->getGoogleAuthenticatorSecret())) {
            $secret = $this->authenticator->generateSecret();

            //Store the secret
            $this->service->setUserSecret($secret, $user->getId());
        }

        $form = $this->createForm(FormType::class, $vm);
        $form->add('Test_Code', SubmitType::class, ['attr' => ['class' => 'btn btn-primary float-right']]);

        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            try {
                //Validate the code
                if($this->authenticator->checkCode($user, $vm->getCode())) {
                    $this->service->confirmCode($user->getId());
                    $this->addFlash(MessageTypes::SuccessMessage, '2FA has been enabled');
                    return $this->redirectToRoute('home');
                }

                $this->addFlash(MessageTypes::ErrorMessage, 'The code was not valid');
                //remove the secret
                $this->service->setUserSecret('', $user->getId());

            } catch (\Exception $e) {
                $this->logger->error($e);
            }
        }

        $qrCodeContent = $this->authenticator->getQRContent($user);

        return $this->render('home/enable_2fa.html.twig', [
            'message' => $qrCodeContent,
            'title' => 'Enable 2FA',
            'form' => $form->createView(),
            'page_title' => 'Enable 2FA',
            'sub_heading' => "Enable 2FA using one of the authenticator apps by scanning the QR code and then confirm your code to activate",
        ]);
    }

    /**
     * @Route("/disable-2fa", name="disable_2fa")
     * @param Request $request
     * @return RedirectResponse
     */
    public function disable2Fa(Request $request): RedirectResponse
    {
        $this->service->setUserSecret(null, $this->getUser()->getId());

        $referer = $request->headers->get('referer');

        return !(empty($referer)) ? $this->redirect($referer) : $this->redirectToRoute('home');
    }

}