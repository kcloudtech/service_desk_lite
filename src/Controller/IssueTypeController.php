<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Utils\FormType;
use Utils\MessageTypes;
use App\ViewModels\TableFilter;
use App\Services\IssueTypeService;
use App\ViewModels\IssueType\IssueTypeListVM;
use App\ViewModels\IssueType\IssueTypeVM;
use Psr\Log\LoggerInterface;
use App\ViewModels\HistoryListVM;
use App\Entity\IssueType;

/**
 * Class IssueTypeController
 * @author kells
 * @package Controller
 *
 */
class IssueTypeController extends AbstractController
{
    /**
     * @var IssueTypeService
     */
    private $service;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * IssueTypeControllerController constructor.
     * @param IssueTypeService $service
     * @param LoggerInterface $logger
     */
    public function __construct(IssueTypeService $service, LoggerInterface $logger)
    {
        $this->service = $service;
        $this->logger = $logger;
    }


    /**
     * @Route("issue-type/", name="list_issue_types")
     * @Security("is_granted('read', 'issue_type')")
     */
    public function index(): Response
    {
        $results = $this->service->fetchAll();
        return $this->render('@generic/generic_list.html.twig', [
            'rows' => $results,
            'headers' => IssueTypeListVM::getHeaders(),
            'title' => 'IssueType',
            'new_path' => 'new_issue_type',
            'new_text' => 'New IssueType',
            'center_columns' => [],
            'table_filters_title' => 'Filters',
            'table_filters' => [
                //new TableFilter('filter_id', 'filter_icon', 'filter_title', 0,'filter_by_value'),
            ],
            'auth_object' => 'issue_type',
            'excluded_delete_ids' => [],
            'excluded_edit_ids' => [],
            'recycle_path' => 'list_archived_issue_types',
            'is_versioned' => true
        ]);
    }

    /**
     * @Route("issue-type/new", name="new_issue_type")
     * @Security("is_granted('create', 'issue_type')")
     * @param Request $request
     * @param IssueTypeVM $vm
     * @return Response
     */
    public function add(Request $request, IssueTypeVM $vm): Response
    {
        $form = $this->createForm(FormType::class, $vm);
        $form->add('Save', SubmitType::class, ['attr' => ['class' => 'btn btn-primary float-right']]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($this->service->addIssueType($vm)) {
                    return $this->redirectToRoute('list_issue_types');
                }
            } catch (\Exception $e) {
                $this->logger->error($e);
            }
            $this->addFlash(MessageTypes::ErrorMessage, 'Sorry, we seem to have run into a snag');
        }

        return $this->render('@generic/generic_form.html.twig', [
            'form' => $form->createView(),
            'title' => 'New Issue Type'
        ]);
    }

    /**
     * @Route("issue-type/update/{hashid}", name="update_issue_type")
     * @Security("is_granted('update', 'issue_type')")
     * @param Request $request
     * @param IssueTypeVM $vm
     * @return Response
     */
    public function update(Request $request, IssueTypeVM $vm): Response
    {

        if ($vm->getId() <= 0) {
            $this->addFlash(MessageTypes::WarningMessage, "Sorry, we couldn't find that record");
            return $this->redirectToRoute('list_issue_types');
        }

        $form = $this->createForm(FormType::class, $vm);
        $form->add('Save', SubmitType::class, ['attr' => ['class' => 'btn btn-primary float-right']]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($this->service->updateIssueType($vm)) {
                    return $this->redirectToRoute('list_issue_types');
                }
            } catch (\Exception $e) {
                $this->logger->error($e);
            }
            $this->addFlash(MessageTypes::ErrorMessage, 'Sorry, we seem to have run into a snag');
        }

        return $this->render('@generic/generic_form.html.twig', [
            'form' => $form->createView(),
            'title' => 'Update Issue Type'
        ]);
    }

    /**
     * @Route("issue-type/delete/{hashid}", name="delete_issue_type")
     * @Security("is_granted('delete', 'issue_type')")
     * @param int $hashid
     * @return Response
     */
    public function delete(int $hashid): Response
    {
        try {
            if ($this->service->deleteIssueType($hashid)) {
                $this->addFlash(MessageTypes::SuccessMessage, 'The issue type has been deleted');
            }
        } catch (\Exception $e) {
            $this->logger->error($e);
            $this->addFlash(MessageTypes::ErrorMessage, 'Sorry, we seem to have run into a snag');
        }
        return $this->redirectToRoute('list_issue_types');
    }

    /**
     * @Route("issue-type/archived", name="list_archived_issue_types")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     */
    public function archived()
    {
        $results = IssueTypeListVM::mapCollection($this->service->fetchArchived(IssueType::class));

        if ($results === null) {
            $this->addFlash(MessageTypes::WarningMessage, 'No history found');
            return $this->redirectToRoute('list_issue_types');
        }

        return $this->render('@generic/generic_list.html.twig', [
            'rows' => $results,
            'headers' => IssueTypeListVM::getHeaders(),
            'title' => 'Recycle Bin',
            'entity' => 'issue_type',
            'center_columns' => [''],
        ]);
    }

    /**
     * @Route("issue-type/restore/{hashid}", name="restore_issue_type")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param int $hashid
     * @return Response
     */
    public function restore(int $hashid): Response
    {
        if ($this->service->restore($hashid, IssueType::class)) {
            $this->addFlash(MessageTypes::SuccessMessage, 'The issue type has been restored');
        }
        return $this->redirectToRoute('list_archived_issue_types');
    }

    /**
     * @Route("issue-type/history/{hashid}", name="issue_type_history")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param int $hashid
     * @return Response
     */
    public function history(int $hashid): Response
    {
        $results = $this->service->fetchHistory($hashid, IssueType::class);

        return $this->render('@generic/generic_list.html.twig', [
            'rows' => $results,
            'headers' => HistoryListVM::getHeaders(),
            'title' => 'History',
            'entity' => 'issue_type',
            'center_columns' => ['version'],
        ]);
    }

    /**
     * @Route("issue-type/revert/{hashid}/{version}", name="revert_issue_type")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param int $hashid
     * @param int $version
     * @return Response
     */
    public function revert(int $hashid, int $version): Response
    {
        try {
            if ($this->service->revert($hashid, $version, IssueType::class)) {
                $this->addFlash(MessageTypes::SuccessMessage, 'The issue type has been reverted');
            }
        } catch (\Exception $e) {
            $this->logger->error($e);
            $this->addFlash(MessageTypes::ErrorMessage,
                'Sorry, we seem to have run into a snag processing your request');
        }
        return $this->redirectToRoute('list_issue_types');
    }
}