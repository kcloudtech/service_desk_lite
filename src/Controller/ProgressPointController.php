<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Utils\FormType;
use Utils\MessageTypes;
use App\ViewModels\TableFilter;
use App\Services\ProgressPointService;
use App\ViewModels\ProgressPoint\ProgressPointListVM;
use App\ViewModels\ProgressPoint\ProgressPointVM;
use Psr\Log\LoggerInterface;
use App\ViewModels\HistoryListVM;
use App\Entity\ProgressPoint;

/**
 * Class ProgressPointController
 * @author kells
 * @package Controller
 *
 */
class ProgressPointController extends AbstractController
{
    /**
     * @var ProgressPointService
     */
    private $service;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * ProgressPointControllerController constructor.
     * @param ProgressPointService $service
     * @param LoggerInterface $logger
     */
    public function __construct(ProgressPointService $service, LoggerInterface $logger)
    {
        $this->service = $service;
        $this->logger = $logger;
    }


    /**
     * @Route("progress-point/", name="list_progress_points")
     * @Security("is_granted('read', 'progress_point')")
     */
    public function index(): Response
    {
        $results = $this->service->fetchAll();

        return $this->render('@generic/generic_list.html.twig', [
            'rows' => $results,
            'headers' => ProgressPointListVM::getHeaders(),
            'title' => 'ProgressPoint',
            'new_path' => 'new_progress_point',
            'new_text' => 'New Progress Point',
            'center_columns' => ['position', 'statuses'],
            'table_filters_title' => 'Filters',
            'table_filters' => [
                //new TableFilter('filter_id', 'filter_icon', 'filter_title', 0,'filter_by_value'),
            ],
            'auth_object' => 'progress_point',
            'excluded_delete_ids' => [],
            'excluded_edit_ids' => [],
            'recycle_path' => 'list_archived_progress_points',
            'is_versioned' => true
        ]);
    }

    /**
     * @Route("progress-point/new", name="new_progress_point")
     * @Security("is_granted('create', 'progress_point')")
     * @param Request $request
     * @param ProgressPointVM $vm
     * @return Response
     */
    public function add(Request $request, ProgressPointVM $vm): Response
    {
        $form = $this->createForm(FormType::class, $vm);
        $form->add('Save', SubmitType::class, ['attr' => ['class' => 'btn btn-primary float-right']]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($this->service->addProgressPoint($vm)) {
                    return $this->redirectToRoute('list_progress_points');
                }
            } catch (\Exception $e) {
                $this->logger->error($e);
            }
            $this->addFlash(MessageTypes::ErrorMessage, 'Sorry, we seem to have run into a snag');
        }

        return $this->render('@generic/generic_form.html.twig', [
            'form' => $form->createView(),
            'title' => 'New Progress Point'
        ]);
    }

    /**
     * @Route("progress-point/update/{hashid}", name="update_progress_point")
     * @Security("is_granted('update', 'progress_point')")
     * @param Request $request
     * @param ProgressPointVM $vm
     * @return Response
     */
    public function update(Request $request, ProgressPointVM $vm): Response
    {

        if ($vm->getId() <= 0) {
            $this->addFlash(MessageTypes::WarningMessage, "Sorry, we couldn't find that record");
            return $this->redirectToRoute('list_progress_points');
        }

        $form = $this->createForm(FormType::class, $vm);
        $form->add('Save', SubmitType::class, ['attr' => ['class' => 'btn btn-primary float-right']]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($this->service->updateProgressPoint($vm)) {
                    return $this->redirectToRoute('list_progress_points');
                }
            } catch (\Exception $e) {
                $this->logger->error($e);
            }
            $this->addFlash(MessageTypes::ErrorMessage, 'Sorry, we seem to have run into a snag');
        }

        return $this->render('@generic/generic_form.html.twig', [
            'form' => $form->createView(),
            'title' => 'Update Progress Point'
        ]);
    }

    /**
     * @Route("progress-point/delete/{hashid}", name="delete_progress_point")
     * @Security("is_granted('delete', 'progress_point')")
     * @param int $hashid
     * @return Response
     */
    public function delete(int $hashid): Response
    {
        try {
            if ($this->service->deleteProgressPoint($hashid)) {
                $this->addFlash(MessageTypes::SuccessMessage, 'The progress point has been deleted');
            }
        } catch (\Exception $e) {
            $this->logger->error($e);
            $this->addFlash(MessageTypes::ErrorMessage, 'Sorry, we seem to have run into a snag');
        }
        return $this->redirectToRoute('list_progress_points');
    }

    /**
     * @Route("progress-point/archived", name="list_archived_progress_points")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     */
    public function archived()
    {
        $results = ProgressPointListVM::mapCollection($this->service->fetchArchived(ProgressPoint::class));

        if ($results === null) {
            $this->addFlash(MessageTypes::WarningMessage, 'No history found');
            return $this->redirectToRoute('list_progress_points');
        }

        return $this->render('@generic/generic_list.html.twig', [
            'rows' => $results,
            'headers' => ProgressPointListVM::getHeaders(),
            'title' => 'Recycle Bin',
            'entity' => 'progress_point',
            'center_columns' => [''],
        ]);
    }

    /**
     * @Route("progress-point/restore/{hashid}", name="restore_progress_point")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param int $hashid
     * @return Response
     */
    public function restore(int $hashid): Response
    {
        if ($this->service->restore($hashid, ProgressPoint::class)) {
            $this->addFlash(MessageTypes::SuccessMessage, 'The progress point has been restored');
        }
        return $this->redirectToRoute('list_archived_progress_points');
    }

    /**
     * @Route("progress-point/history/{hashid}", name="progress_point_history")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param int $hashid
     * @return Response
     */
    public function history(int $hashid): Response
    {
        $results = $this->service->fetchHistory($hashid, ProgressPoint::class);

        return $this->render('@generic/generic_list.html.twig', [
            'rows' => $results,
            'headers' => HistoryListVM::getHeaders(),
            'title' => 'History',
            'entity' => 'progress_point',
            'center_columns' => ['version'],
        ]);
    }

    /**
     * @Route("progress-point/revert/{hashid}/{version}", name="revert_progress_point")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param int $hashid
     * @param int $version
     * @return Response
     */
    public function revert(int $hashid, int $version): Response
    {
        try {
            if ($this->service->revert($hashid, $version, ProgressPoint::class)) {
                $this->addFlash(MessageTypes::SuccessMessage, 'The progress point has been reverted');
            }
        } catch (\Exception $e) {
            $this->logger->error($e);
            $this->addFlash(MessageTypes::ErrorMessage,
                'Sorry, we seem to have run into a snag processing your request');
        }
        return $this->redirectToRoute('list_progress_points');
    }
}