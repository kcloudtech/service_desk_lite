<?php

namespace App\Controller;

use App\Form\FormTypes\PermissionType;
use App\Form\FieldTypes\PermissionCollectionType;
use App\ViewModels\HistoryListVM;
use App\ViewModels\TableFilter;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\ExpressionLanguage\Expression;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Utils\FormType;
use Utils\MessageTypes;
use App\Services\RoleService;
use App\ViewModels\Role\RoleListVM;
use App\ViewModels\Role\RoleVM;
use Psr\Log\LoggerInterface;

/**
* Class RoleController
 * @author kells
 * @package Controller
*
*/
class RoleController extends AbstractController
{
    /**
     * @var RoleService     */
    private $service;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * RoleController constructor.
     * @param RoleService $service
     * @param LoggerInterface $logger
     */
    public function __construct(RoleService $service, LoggerInterface $logger)
    {
        $this->service = $service;
        $this->logger = $logger;
    }

    /**
     * @Route("/admin/roles", name="list_roles")
     * @Security("is_granted('read', 'role')")
     */
    public function index(): Response
    {
        $results = $this->service->fetchAll();

        /** @var RoleListVM $superUser */
        $superUser = array_filter(
            $results,  static function ($e)  {
                return $e->getName() === 'Super Admin';
            }
        );

        usort($results, array($this,'sortByName'));

        $roleIndex = array_search('name', RoleListVM::getHeaders(), true);

            return $this->render('@generic/generic_list.html.twig', [
                'rows' => $results,
                'headers' => RoleListVM::getHeaders(),
                'sub_heading' =>"Roles types are defined by their suffix. A role with the suffix 'Admin' is an admin role and a role with the suffix 'Manager' is a manager role. All others are user roles.",
                'title' => 'User Roles',
                'new_path' => 'new_role',
                'new_text' => 'New Role',
                'center_columns' => ['permissions', 'users'],
                'table_filters_title' => 'Filters',
                'table_filters' => [
                    new TableFilter('all', 'user-friends', 'All Roles', $roleIndex,''),
                    new TableFilter('admin', 'user-shield', 'Admin Roles', $roleIndex ,'Admin'),
                    new TableFilter('manager', 'users', 'Manger Roles', $roleIndex ,'Manager'),
                    new TableFilter('user', 'user', 'User Roles', $roleIndex,'^(?!.*?(?:Admin|Manager)).*'),
                ],
                'auth_object' => 'role',
                'excluded_delete_ids' => [current($superUser)->getId()],
                'excluded_edit_ids' => [current($superUser)->getId()],
                'recycle_path' => 'list_archived_roles',
                'is_versioned' => true
                ]);
        }

    /**
     * @Route("/admin/roles/new", name="new_role")
     * @Security("is_granted('create', 'role')")
     * @param Request $request
     * @param RoleVM $vm
     * @return Response
     */
    public function add(Request $request, RoleVM $vm): Response
    {

        $data = ['role' => $vm, 'permissions' => $this->service->fetchPermissionSet()];

        $vm->setPermissions($this->service->fetchPermissionSet());

        $builder = $this->createFormBuilder($data);

        $builder->add($builder->create('role', FormType::class, array('data_class' => RoleVM::class)));
        $builder->add($builder->create('permissions', PermissionCollectionType::class, array('entry_type' => PermissionType::class)));
        $builder->add('Save', SubmitType::class, ['attr' => ['class' => 'btn btn-primary float-right']]);

        $form = $builder->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            try {
                if ($this->service->addRole($vm->setPermissions($data['permissions'])->setAccount($this->getUser()->getSubscriber()))) {
                    return $this->redirectToRoute('list_roles');
                }
            } catch (Exception $e) {
                $this->logger->error($e);
            }
            $this->addFlash(MessageTypes::ErrorMessage,'Sorry, we seem to have run into a snag');
        }

        return $this->render('@generic/generic_form.html.twig', [
            'form' => $form->createView(),
            'title' => 'New Role'
        ]);
    }

    /**
     * @Route("/admin/roles/update/{hashid}", name="update_role")
     * @Security("is_granted('update', 'role')")
     * @param Request $request
     * @param RoleVM $vm
     * @return Response
     */
    public function update(Request $request, RoleVM $vm): Response
    {
        if($vm->getId() <= 0) {
            $this->addFlash(MessageTypes::WarningMessage, "Sorry, we couldn't find that record");
            return $this->redirectToRoute('list_roles');
        }

        if(!$this->isGranted(new Expression('user.getId() !== object'),$vm->getId()))
        {
            $this->addFlash(MessageTypes::WarningMessage, 'You cannot update your own role');
            return $this->redirectToRoute('list_roles');
        }

        $data = ['role' => $vm, 'permissions' => $vm->getPermissions()];

        $builder = $this->createFormBuilder($data);

        $builder->add($builder->create('role', FormType::class, array('data_class' => RoleVM::class)));
        $builder->add($builder->create('permissions', PermissionCollectionType::class, array('entry_type' => PermissionType::class)));
        $builder->add('Save', SubmitType::class, ['attr' => ['class' => 'btn btn-primary float-right']]);

        $form = $builder->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            try {
                if ($this->service->updateRole($vm)) {
                    return $this->redirectToRoute('list_roles');
                }
            } catch (Exception $e) {
                $this->logger->error($e);
            }
            $this->addFlash(MessageTypes::ErrorMessage,'Sorry, we seem to have run into a snag');
        }

        return $this->render('@generic/generic_form.html.twig', [
            'form' => $form->createView(),
            'title' => 'Update Role',
            'sub_heading' => 'Changes to the role permissions will apply to all users within the role'
        ]);
    }

    /**
     * @Route("/admin/roles/delete/{hashid}", name="delete_role")
     * @Security("is_granted('delete', 'role')")
     * @param int $hashid
     * @return Response
     */
    public function delete(int $hashid): Response
    {
        try {
            if ($this->service->deleteRole($hashid)) {
                $this->addFlash(MessageTypes::SuccessMessage, 'The role has been deleted');
            } 
        } catch(Exception $e) {
            $this->logger->error($e);
            $this->addFlash(MessageTypes::ErrorMessage, 'Sorry, we seem to have run into a snag processing your request');
        }
        return $this->redirectToRoute('list_roles');
    }

    /**
     * @Route("/admin/roles/archived", name="list_archived_roles")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     */
    public function archived(): Response
    {
        $results = $this->service->fetchArchived();

        return $this->render('@generic/generic_list.html.twig', [
            'rows' => $results,
            'headers' => RoleListVM::getHeaders(),
            'title' => 'Recycle Bin',
            'entity' => 'role',
            'center_columns' => ['permissions', 'users'],
        ]);
    }

    /**
     * @Route("/admin/roles/restore/{hashid}", name="restore_role")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param int $hashid
     * @return Response
     */
    public function restore(int $hashid): Response
    {
        if($this->service->restore($hashid)) {
            $this->addFlash(MessageTypes::SuccessMessage,'The role has been restored');
        }
        return $this->redirectToRoute('list_archived_roles');
    }

    /**
     * @Route("/admin/roles/history/{hashid}", name="role_history")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param int $hashid
     * @return Response
     */
    public function history(int $hashid): Response
    {
        $results = $this->service->fetchHistory($hashid);

        if($results === null) {
            $this->addFlash(MessageTypes::WarningMessage, 'No history found');
            return  $this->redirectToRoute('list_roles');
        }

        return $this->render('@generic/generic_list.html.twig', [
            'rows' => $results,
            'headers' => HistoryListVM::getHeaders(),
            'title' => 'History',
            'entity' => 'role',
            'center_columns' => ['version'],
        ]);
    }

    /**
     * @Route("/admin/roles/revert/{hashid}/{version}", name="revert_role")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param int $hashid
     * @param int $version
     * @return Response
     */
    public function revert(int $hashid, int $version): Response
    {
        try {
            if ($this->service->revert($hashid, $version)) {
                $this->addFlash(MessageTypes::SuccessMessage, 'The role has been reverted');
            }
        } catch (Exception $e) {
            $this->logger->error($e);
            $this->addFlash(MessageTypes::ErrorMessage, 'Sorry, we seem to have run into a snag processing your request');
        }
        return $this->redirectToRoute('list_roles');
    }

    /**
     * @param RoleListVM $object1
     * @param RoleListVM $object2
     * @return bool
     */
    public function sortByName($object1, $object2): bool
    {
        return $object1->getName() > $object2->getName();
    }
}