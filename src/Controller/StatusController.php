<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Utils\FormType;
use Utils\MessageTypes;
use App\ViewModels\TableFilter;
use App\Services\StatusService;
use App\ViewModels\Status\StatusListVM;
use App\ViewModels\Status\StatusVM;
use Psr\Log\LoggerInterface;
use App\ViewModels\HistoryListVM;
use App\Entity\Status;

/**
 * Class StatusController
 * @author kells
 * @package Controller
 *
 */
class StatusController extends AbstractController
{
    /**
     * @var StatusService
     */
    private $service;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * StatusControllerController constructor.
     * @param StatusService $service
     * @param LoggerInterface $logger
     */
    public function __construct(StatusService $service, LoggerInterface $logger)
    {
        $this->service = $service;
        $this->logger = $logger;
    }


    /**
     * @Route("status/", name="list_stati")
     * @Security("is_granted('read', 'status')")
     */
    public function index(): Response
    {
        $results = $this->service->fetchAll();

        $points = $this->service->fetchProgressPoints();

        $filters = [];
        foreach ($points as $point) {
            $filters[] = new TableFilter($point, '', $point, 1, $point);
        }

        return $this->render('@generic/generic_list.html.twig', [
            'rows' => $results,
            'headers' => StatusListVM::getHeaders(),
            'title' => 'Status',
            'new_path' => 'new_status',
            'new_text' => 'New Status',
            'center_columns' => [],
            'table_filters_title' => 'Filters',
            'table_filters' => $filters,
            'auth_object' => 'status',
            'excluded_delete_ids' => [],
            'excluded_edit_ids' => [],
            'recycle_path' => 'list_archived_statuses',
            'is_versioned' => true
        ]);
    }

    /**
     * @Route("status/new", name="new_status")
     * @Security("is_granted('create', 'status')")
     * @param Request $request
     * @param StatusVM $vm
     * @return Response
     */
    public function add(Request $request, StatusVM $vm): Response
    {
        $form = $this->createForm(FormType::class, $vm);
        $form->add('Save', SubmitType::class, ['attr' => ['class' => 'btn btn-primary float-right']]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($this->service->addStatus($vm)) {
                    return $this->redirectToRoute('list_statuses');
                }
            } catch (\Exception $e) {
                $this->logger->error($e);
            }
            $this->addFlash(MessageTypes::ErrorMessage, 'Sorry, we seem to have run into a snag');
        }

        return $this->render('@generic/generic_form.html.twig', [
            'form' => $form->createView(),
            'title' => 'New Status'
        ]);
    }

    /**
     * @Route("status/update/{hashid}", name="update_status")
     * @Security("is_granted('update', 'status')")
     * @param Request $request
     * @param StatusVM $vm
     * @return Response
     */
    public function update(Request $request, StatusVM $vm): Response
    {

        if ($vm->getId() <= 0) {
            $this->addFlash(MessageTypes::WarningMessage, "Sorry, we couldn't find that record");
            return $this->redirectToRoute('list_statuses');
        }

        $form = $this->createForm(FormType::class, $vm);
        $form->add('Save', SubmitType::class, ['attr' => ['class' => 'btn btn-primary float-right']]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($this->service->updateStatus($vm)) {
                    return $this->redirectToRoute('list_statuses');
                }
            } catch (\Exception $e) {
                $this->logger->error($e);
            }
            $this->addFlash(MessageTypes::ErrorMessage, 'Sorry, we seem to have run into a snag');
        }

        return $this->render('@generic/generic_form.html.twig', [
            'form' => $form->createView(),
            'title' => 'Update Status'
        ]);
    }

    /**
     * @Route("status/delete/{hashid}", name="delete_status")
     * @Security("is_granted('delete', 'status')")
     * @param int $hashid
     * @return Response
     */
    public function delete(int $hashid): Response
    {
        try {
            if ($this->service->deleteStatus($hashid)) {
                $this->addFlash(MessageTypes::SuccessMessage, 'The status has been deleted');
            }
        } catch (\Exception $e) {
            $this->logger->error($e);
            $this->addFlash(MessageTypes::ErrorMessage, 'Sorry, we seem to have run into a snag');
        }
        return $this->redirectToRoute('list_statuses');
    }

    /**
     * @Route("status/archived", name="list_archived_statuses")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     */
    public function archived()
    {
        $results = StatusListVM::mapCollection($this->service->fetchArchived(Status::class));

        if ($results === null) {
            $this->addFlash(MessageTypes::WarningMessage, 'No history found');
            return $this->redirectToRoute('list_statuses');
        }

        return $this->render('@generic/generic_list.html.twig', [
            'rows' => $results,
            'headers' => StatusListVM::getHeaders(),
            'title' => 'Recycle Bin',
            'entity' => 'status',
            'center_columns' => [''],
        ]);
    }

    /**
     * @Route("status/restore/{hashid}", name="restore_status")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param int $hashid
     * @return Response
     */
    public function restore(int $hashid): Response
    {
        if ($this->service->restore($hashid, Status::class)) {
            $this->addFlash(MessageTypes::SuccessMessage, 'The status has been restored');
        }
        return $this->redirectToRoute('list_archived_statuses');
    }

    /**
     * @Route("status/history/{hashid}", name="status_history")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param int $hashid
     * @return Response
     */
    public function history(int $hashid): Response
    {
        $results = $this->service->fetchHistory($hashid, Status::class);

        return $this->render('@generic/generic_list.html.twig', [
            'rows' => $results,
            'headers' => HistoryListVM::getHeaders(),
            'title' => 'History',
            'entity' => 'status',
            'center_columns' => ['version'],
        ]);
    }

    /**
     * @Route("status/revert/{hashid}/{version}", name="revert_status")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param int $hashid
     * @param int $version
     * @return Response
     */
    public function revert(int $hashid, int $version): Response
    {
        try {
            if ($this->service->revert($hashid, $version, Status::class)) {
                $this->addFlash(MessageTypes::SuccessMessage, 'The status has been reverted');
            }
        } catch (\Exception $e) {
            $this->logger->error($e);
            $this->addFlash(MessageTypes::ErrorMessage,
                'Sorry, we seem to have run into a snag processing your request');
        }
        return $this->redirectToRoute('list_statuses');
    }
}