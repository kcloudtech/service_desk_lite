<?php

namespace App\Controller;

use App\Helpers\Converters;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Utils\FormType;
use Utils\MessageTypes;
use App\ViewModels\TableFilter;
use App\Services\TicketService;
use App\ViewModels\Ticket\TicketListVM;
use App\ViewModels\Ticket\TicketVM;
use Psr\Log\LoggerInterface;
use App\ViewModels\HistoryListVM;
use App\Entity\Ticket;

/**
 * Class TicketController
 * @author kells
 * @package Controller
 *
 */
class TicketController extends AbstractController
{
    /**
     * @var TicketService
     */
    private $service;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * TicketControllerController constructor.
     * @param TicketService $service
     * @param LoggerInterface $logger
     */
    public function __construct(TicketService $service, LoggerInterface $logger)
    {
        $this->service = $service;
        $this->logger = $logger;
    }

    /**
     * @Route("ticket/", name="list_tickets")
     * @Security("is_granted('read', 'ticket')")
     */
    public function index(): Response
    {
        $results = $this->service->fetchAll();

        $index = array_search('priority', TicketListVM::getHeaders(), true);

        return $this->render('@generic/generic_list.html.twig', [
            'rows' => $results,
            'headers' => TicketListVM::getHeaders(),
            'title' => 'Tickets',
            'new_path' => 'new_ticket',
            'new_text' => 'New Ticket',
            'center_columns' => [],
            'table_filters_title' => 'Filters',
            'table_filters' => [
                new TableFilter('Urgent', 'fas fa-angle-double-up', 'Urgent', $index,4),
                new TableFilter('High', 'fas fa-angle-up', 'High', $index,3),
                new TableFilter('Normal', 'fas fa-circle', 'Normal', $index,2),
                new TableFilter('Low', 'fas fa-angle-down', 'Low', $index,1),
            ],
            'auth_object' => 'ticket',
            'excluded_delete_ids' => [],
            'excluded_edit_ids' => [],
            'recycle_path' => 'list_archived_tickets',
            'is_versioned' => true
        ]);
    }

    /**
     * @Route("ticket/overdue", name="list_overdue_tickets")
     * @Security("is_granted('read', 'ticket')")
     */
    public function overdue(): Response
    {
        $results = $this->service->fetchOverdue();

        $statusIndex = array_search('status', TicketListVM::getHeaders(), true);

        $statues = $this->service->fetchStatuses();

        $filters = [];
        foreach ($statues as $status) {
            $filters[] = new TableFilter($status, '', $status, $statusIndex, $status);
        }

        return $this->render('@generic/generic_list.html.twig', [
            'rows' => $results,
            'headers' => TicketListVM::getHeaders(),
            'title' => 'Overdue Tickets',
            'new_path' => 'new_ticket',
            'new_text' => 'New Ticket',
            'center_columns' => [],
            'table_filters_title' => 'Filters',
            'table_filters' => $filters,
            'auth_object' => 'ticket',
            'excluded_delete_ids' => [],
            'excluded_edit_ids' => [],
            'recycle_path' => 'list_archived_tickets',
            'is_versioned' => true
        ]);
    }

    /**
     * @Route("ticket/my-tickets", name="list_my_tickets")
     * @Security("is_granted('read', 'ticket')")
     */
    public function myTickets(): Response
    {
        $results = $this->service->fetchUserTickets($this->getUser());

        $statusIndex = array_search('status', TicketListVM::getHeaders(), true);

        $statues = $this->service->fetchStatuses();

        $filters = [];
        foreach ($statues as $status) {
            $filters[] = new TableFilter($status, '', $status, $statusIndex, $status);
        }

        return $this->render('@generic/generic_list.html.twig', [
            'rows' => $results,
            'headers' => TicketListVM::getHeaders(),
            'title' => 'My Tickets',
            'new_path' => 'new_ticket',
            'new_text' => 'New Ticket',
            'center_columns' => [],
            'table_filters_title' => 'Filters',
            'table_filters' => $filters,
            'auth_object' => 'ticket',
            'excluded_delete_ids' => [],
            'excluded_edit_ids' => [],
            'recycle_path' => 'list_archived_tickets',
            'is_versioned' => true
        ]);
    }

    /**
     * @Route("ticket/view/{hashid}", name="view_ticket")
     * @Security("is_granted('read', 'ticket')")
     * @param TicketVM $vm
     * @return Response
     */
    public function view(TicketVM $vm): Response
    {
        if(!$vm->getId()) {
            $this->addFlash(MessageTypes::WarningMessage, 'Ticket not found');
            return  $this->redirectToRoute('list_tickets');
        }

        return $this->render('ticket/view.html.twig', [
            'title' =>  Converters::truncateText(ucwords($vm->getSubject()), 10),
            'ticket' => $vm,
            'points' => $this->service->fetchProgressPoints(),
            'current_point' => $vm->getStatus()->getProgressPoint()->getDescription()
        ]);
    }

    /**
     * @Route("ticket/new", name="new_ticket")
     * @Security("is_granted('create', 'ticket')")
     * @param Request $request
     * @param TicketVM $vm
     * @return Response
     */
    public function add(Request $request, TicketVM $vm): Response
    {
        $form = $this->createForm(FormType::class, $vm);
        $form->add('Save', SubmitType::class, ['attr' => ['class' => 'btn btn-primary float-right']]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($this->service->addTicket($vm)) {
                    return $this->redirectToRoute('list_tickets');
                }
            } catch (\Exception $e) {
                $this->logger->error($e);
            }
            $this->addFlash(MessageTypes::ErrorMessage, 'Sorry, we seem to have run into a snag');
        }

        return $this->render('@generic/generic_form.html.twig', [
            'form' => $form->createView(),
            'title' => 'New Ticket'
        ]);
    }

    /**
     * @Route("ticket/update/{hashid}", name="update_ticket")
     * @Security("is_granted('update', 'ticket')")
     * @param Request $request
     * @param TicketVM $vm
     * @return Response
     */
    public function update(Request $request, TicketVM $vm): Response
    {

        if ($vm->getId() <= 0) {
            $this->addFlash(MessageTypes::WarningMessage, "Sorry, we couldn't find that record");
            return $this->redirectToRoute('list_tickets');
        }

        $form = $this->createForm(FormType::class, $vm);
        $form->add('Save', SubmitType::class, ['attr' => ['class' => 'btn btn-primary float-right']]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($this->service->updateTicket($vm)) {
                    return $this->redirectToRoute('list_tickets');
                }
            } catch (\Exception $e) {
                $this->logger->error($e);
            }
            $this->addFlash(MessageTypes::ErrorMessage, 'Sorry, we seem to have run into a snag');
        }

        return $this->render('@generic/generic_form.html.twig', [
            'form' => $form->createView(),
            'title' => 'Update Ticket'
        ]);
    }

    /**
     * @Route("ticket/delete/{hashid}", name="delete_ticket")
     * @Security("is_granted('delete', 'ticket')")
     * @param int $hashid
     * @return Response
     */
    public function delete(int $hashid): Response
    {
        try {
            if ($this->service->deleteTicket($hashid)) {
                $this->addFlash(MessageTypes::SuccessMessage, 'The ticket has been deleted');
            }
        } catch (\Exception $e) {
            $this->logger->error($e);
            $this->addFlash(MessageTypes::ErrorMessage, 'Sorry, we seem to have run into a snag');
        }
        return $this->redirectToRoute('list_tickets');
    }

    /**
     * @Route("ticket/archived", name="list_archived_tickets")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     */
    public function archived()
    {
        $results = TicketListVM::mapCollection($this->service->fetchArchived(Ticket::class));

        if ($results === null) {
            $this->addFlash(MessageTypes::WarningMessage, 'No history found');
            return $this->redirectToRoute('list_tickets');
        }

        return $this->render('@generic/generic_list.html.twig', [
            'rows' => $results,
            'headers' => TicketListVM::getHeaders(),
            'title' => 'Recycle Bin',
            'entity' => 'ticket',
            'center_columns' => [''],
        ]);
    }

    /**
     * @Route("ticket/restore/{hashid}", name="restore_ticket")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param int $hashid
     * @return Response
     */
    public function restore(int $hashid): Response
    {
        if ($this->service->restore($hashid, Ticket::class)) {
            $this->addFlash(MessageTypes::SuccessMessage, 'The ticket has been restored');
        }
        return $this->redirectToRoute('list_archived_tickets');
    }

    /**
     * @Route("ticket/history/{hashid}", name="ticket_history")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param int $hashid
     * @return Response
     */
    public function history(int $hashid): Response
    {
        $results = $this->service->fetchHistory($hashid, Ticket::class);

        return $this->render('@generic/generic_list.html.twig', [
            'rows' => $results,
            'headers' => HistoryListVM::getHeaders(),
            'title' => 'History',
            'entity' => 'ticket',
            'center_columns' => ['version'],
        ]);
    }

    /**
     * @Route("ticket/revert/{hashid}/{version}", name="revert_ticket")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param int $hashid
     * @param int $version
     * @return Response
     */
    public function revert(int $hashid, int $version): Response
    {
        try {
            if ($this->service->revert($hashid, $version, Ticket::class)) {
                $this->addFlash(MessageTypes::SuccessMessage, 'The ticket has been reverted');
            }
        } catch (\Exception $e) {
            $this->logger->error($e);
            $this->addFlash(MessageTypes::ErrorMessage,
                'Sorry, we seem to have run into a snag processing your request');
        }
        return $this->redirectToRoute('list_tickets');
    }
}