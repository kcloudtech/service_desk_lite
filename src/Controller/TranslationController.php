<?php

namespace App\Controller;

use App\Entity\Translation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Utils\FormType;
use Utils\MessageTypes;
use App\ViewModels\TableFilter;
use App\Services\TranslationService;
use App\ViewModels\Translation\TranslationListVM;
use App\ViewModels\Translation\TranslationVM;
use Psr\Log\LoggerInterface;

/**
 * Class TranslationController
 * @author Kellsey
 * @IsGranted("IS_AUTHENTICATED_FULLY")
 */
class TranslationController extends AbstractController
{
    /**
     * @var TranslationService
     */
    private $service;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * RoleController constructor.
     * @param TranslationService $service
     * @param LoggerInterface $logger
     */
    public function __construct(TranslationService $service, LoggerInterface $logger)
    {
        $this->service = $service;
        $this->logger = $logger;
    }


    /**
     * @Route("translation/", name="list_translations")
     * @Security("is_granted('read', 'translation')")
     */
    public function index(): Response
    {
        $results = $this->service->fetchAll();
        $locales = $this->service->getAvailableLocales();

        $filters = [];

        foreach ($locales as $locale) {
            $filters[] = new TableFilter($locale, '', $locale, 1, $locale);
        }


        return $this->render('@generic/generic_list.html.twig', [
            'rows' => $results,
            'headers' => TranslationListVM::getHeaders(),
            'title' => 'Translation',
            'center_columns' => [],
            'table_filters_title' => 'Filters',
            'table_filters' => $filters,
            'auth_object' => 'translation',
            'excluded_delete_ids' => [],
            'excluded_edit_ids' => [],
            'is_versioned' => true,
            'recycle_path' => 'list_archived_translations'
        ]);
    }

    /**
     * @Route("translation/new", name="new_translation")
     * @Security("is_granted('create', 'translation')")
     * @param Request $request
     * @param TranslationVM $vm
     * @return Response
     */
    public function add(Request $request, TranslationVM $vm): Response
    {
        $form = $this->createForm(FormType::class, $vm);
        $form->add('Save', SubmitType::class, ['attr' => ['class' => 'btn btn-primary float-right']]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($this->service->addTranslation($vm)) {
                    return $this->redirectToRoute('list_translations');
                }
            } catch (\Exception $e) {
                $this->logger->error($e);
            }
            $this->addFlash(MessageTypes::ErrorMessage, 'Sorry, we seem to have run into a snag');
        }

        return $this->render('@generic/generic_form.html.twig', [
            'form' => $form->createView(),
            'title' => 'New Translation'
        ]);
    }

    /**
     * @Route("translation/update/{hashid}", name="update_translation")
     * @Security("is_granted('update', 'translation')")
     * @param Request $request
     * @param TranslationVM $vm
     * @return Response
     */
    public function update(Request $request, TranslationVM $vm): Response
    {

        if ($vm->getId() <= 0) {
            $this->addFlash(MessageTypes::WarningMessage, "Sorry, we couldn't find that record");
            return $this->redirectToRoute('list_translations');
        }

        $form = $this->createForm(FormType::class, $vm);
        $form->add('Save', SubmitType::class, ['attr' => ['class' => 'btn btn-primary float-right']]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($this->service->updateTranslation($vm)) {
                    return $this->redirectToRoute('list_translations');
                }
            } catch (\Exception $e) {
                $this->logger->error($e);
            }
            $this->addFlash(MessageTypes::ErrorMessage, 'Sorry, we seem to have run into a snag');
        }

        return $this->render('@generic/generic_form.html.twig', [
            'form' => $form->createView(),
            'title' => 'Update Translation'
        ]);
    }

    /**
     * @Route("translation/delete/{hashid}", name="delete_translation")
     * @Security("is_granted('delete', 'translation')")
     * @param int $hashid
     * @return Response
     */
    public function delete(int $hashid): Response
    {
        try {
            if ($this->service->deleteTranslation($hashid)) {
                $this->addFlash(MessageTypes::SuccessMessage, 'The translation has been deleted');
            }
        } catch (\Exception $e) {
            $this->logger->error($e);
            $this->addFlash(MessageTypes::ErrorMessage, 'Sorry, we seem to have run into a snag');
        }
        return $this->redirectToRoute('list_translations');
    }

    /**
     * @Route("translation/archived", name="list_archived_translations")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     */
    public function archived()
    {
        $results = $this->service->fetchArchived(Translation::class, TranslationListVM::class);

        if ($results === null) {
            $this->addFlash(MessageTypes::WarningMessage, 'No history found');
            return $this->redirectToRoute('list_translations');
        }

        return $this->render('@generic/generic_list.html.twig', [
            'rows' => $results,
            'headers' => TranslationListVM::getHeaders(),
            'title' => 'Recycle Bin',
            'entity' => 'translation',
            'center_columns' => [''],
        ]);
    }

    /**
     * @Route("translation/restore/{hashid}", name="restore_translation")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param int $hashid
     * @return Response
     */
    public function restore(int $hashid): Response
    {
        if ($this->service->restore($hashid, Translation::class)) {
            $this->addFlash(MessageTypes::SuccessMessage, 'The translation has been restored');
        }
        return $this->redirectToRoute('list_archived_translations');
    }

    /**
     * @Route("translation/history/{hashid}", name="translation_history")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param int $hashid
     * @return Response
     */
    public function history(int $hashid): Response
    {
        $results = $this->service->fetchHistory($hashid, Translation::class);

        return $this->render('@generic/generic_list.html.twig', [
            'rows' => $results,
            'headers' => TranslationListVM::getHeaders(),
            'title' => 'History',
            'entity' => 'translation',
            'center_columns' => ['version'],
        ]);
    }

    /**
     * @Route("translation/revert/{hashid}/{version}", name="revert_translation")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param int $hashid
     * @param int $version
     * @return Response
     */
    public function revert(int $hashid, int $version): Response
    {
        try {
            if ($this->service->revert($hashid, $version, Translation::class)) {
                $this->addFlash(MessageTypes::SuccessMessage, 'The translation has been reverted');
            }
        } catch (\Exception $e) {
            $this->logger->error($e);
            $this->addFlash(MessageTypes::ErrorMessage,
                'Sorry, we seem to have run into a snag processing your request');
        }
        return $this->redirectToRoute('list_translations');
    }
}