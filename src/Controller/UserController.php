<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\FormTypes\UserForm;
use App\Form\FieldTypes\PermissionCollectionType;
use App\Form\FormTypes\UserPermissionType;
use App\Helpers\RoleRestrictions;
use App\ViewModels\HistoryListVM;
use App\ViewModels\TableFilter;
use App\ViewModels\User\UserPermissionsVM;
use Symfony\Component\ExpressionLanguage\Expression;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Utils\FormType;
use Utils\MessageTypes;
use App\Services\UserService;
use App\ViewModels\User\UserListVM;
use App\ViewModels\User\UserVM;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 *
 * Endpoints for managing users
 *
 * @author Kellsey
 */
class UserController extends AbstractController
{

    /**
     * @var UserService
     */
    private $service;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * RoleController constructor.
     * @param UserService $service
     * @param LoggerInterface $logger
     */
    public function __construct(UserService $service, LoggerInterface $logger)
    {
        $this->service = $service;
        $this->logger = $logger;
    }

    /**
     * @Route("/admin/users", name="list_users")
     * @Security("is_granted('read', 'user')")
     */
    public function index(): Response
    {
        $results = $this->service->fetchAll();

        $index = array_search('isActive', UserListVM::getHeaders(), true);

        $userRole = $this->getUser()->getRole();

       $exclude = array_map(static function (UserListVM $row) use ($userRole){
            return RoleRestrictions::hasHigherPrivileges($row->getRoleObject(), $userRole) ? $row->getId() : null;
        }, $results);

        $exclude=  array_filter($exclude, 'strlen');

        return $this->render('@generic/generic_list.html.twig', [
            'rows' => $results,
            'headers' => UserListVM::getHeaders(),
            'title' => 'System Users & Roles',
            'page_title' => 'Users',
            'new_path' => 'new_user',
            'new_text' => 'New User',
            'center_columns' => ['failedLoginAttempts', 'isActive'],
            'table_filters_title' => 'Filters',
            'table_filters' => [
                new TableFilter('all', 'users', 'All', $index,'0|1'),
                new TableFilter('active', 'toggle-on', 'Active',   $index,  '1'),
                new TableFilter('inactive', 'user-slash', 'Inactive',   $index,  '0')
            ],
            'auth_object' => 'user',
            'excluded_delete_ids' => array_merge([$this->getUser()->getId()], $exclude),
            'excluded_edit_ids' => array_merge([$this->getUser()->getId()], $exclude),
            'recycle_path' => 'list_archived_users',
            'is_versioned' => true
        ]);
    }

    /**
     * @Route("/admin/users/new", name="new_user")
     * @Security("is_granted('create', 'User')")
     * @param Request $request
     * @param UserVM $vm
     * @return Response
     */
    public function add(Request $request, UserVM $vm): Response
    {
        $form = $this->createForm(UserForm::class, $vm, ['allow_file_upload' => true]);
        $form->add('Save', SubmitType::class, ['attr' => ['class' => 'btn btn-primary float-right']]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {

                if ($this->service->registerUser($vm)) {
                    return $this->redirectToRoute('list_users');
                }
            } catch (\Exception $e) {
                $this->logger->error($e);
            } catch (TransportExceptionInterface $e) {
                $this->addFlash(MessageTypes::WarningMessage, 'The email has failed to send. Please check the email address and ask the user to use the forgotten password link');
                return $this->redirectToRoute('list_users');
            }
            $this->addFlash(MessageTypes::ErrorMessage, 'Sorry, we seem to have run into a snag');
        }

        return $this->render('@generic/generic_form.html.twig', [
            'form' => $form->createView(),
            'title' => 'New User',
            'sub_heading' => 'A registration confirmation will be sent to the new user',
        ]);
    }

    /**
     * @Route("/admin/users/update/{hashid}", name="update_user")
     * @Security("is_granted('update', 'user')")
     * @param Request $request
     * @param UserVM $vm
     * @return Response
     */
    public function update(Request $request, UserVM $vm): Response
    {
        if(!$this->isGranted(new Expression('user.getId() !== object'),$vm->getId()))
        {
            $this->addFlash(MessageTypes::WarningMessage, 'Please select profile from the user menu drop down (top right) to edit your own user record');
            return $this->redirectToRoute('list_users');
        }

        $userDetails = new UserPermissionsVM();

        $userDetails->setUserDetails($vm);
        $userDetails->setPermissions($vm->getUserPermissions());

        $builder = $this->createFormBuilder($userDetails);

        $builder->add($builder->create('userDetails', FormType::class, array('data_class' => UserVM::class)));

        $builder->add($builder->create('permissions', PermissionCollectionType::class,
            array('entry_type' => UserPermissionType::class)));

        $form = $builder->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                    if ($this->service->updateUser($userDetails->getUserDetails())) {
                    return $this->redirectToRoute('list_users');
                }
            } catch (\Exception $e) {
                $this->logger->error($e);
            } catch (TransportExceptionInterface $e) {
                $this->logger->error($e);
                $this->addFlash(MessageTypes::WarningMessage, 'The email has failed to send but the user has been updated');
                return $this->redirectToRoute('list_users');
            }
            $this->addFlash(MessageTypes::ErrorMessage, 'Sorry, we seem to have run into a snag');
        }

        return $this->render('@generic/generic_wizard.html.twig', [
            'form' => $form->createView(),
            'title' => ucwords(sprintf('%s %s', $vm->getFirstName(), $vm->getLastName())),
            'sub_heading' => 'Password changes can only be made via the profile page or using the forgotten password option on the login page',
        ]);
    }

    /**
     * @Route("/admin/users/delete/{hashid}", name="delete_user")
     * @Security("is_granted('delete', 'user')")
     * @param int $hashid
     * @return Response
     */
    public function delete(int $hashid): Response
    {
        if(!$this->isGranted(new Expression('user.getId() !== object'),$hashid))
        {
            $this->addFlash(MessageTypes::ErrorMessage, 'You cannot delete your own user record');
            return $this->redirectToRoute('list_users');
        }

        if ($this->service->delete($hashid, User::class)) {
            $this->addFlash(MessageTypes::SuccessMessage, 'The user has been deleted');
        }
        return $this->redirectToRoute('list_users');
    }

    /**
     * @Route("/admin/users/archived", name="list_archived_users")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     */
    public function archived(): Response
    {
        $results = UserListVM::mapCollection($this->service->fetchArchived(User::class));

        return $this->render('@generic/generic_list.html.twig', [
            'rows' => $results,
            'headers' => UserListVM::getHeaders(),
            'title' => 'Recycle Bin',
            'entity' => 'user',
            'center_columns' => ['failedLoginAttempts', 'isActive'],
        ]);
    }

    /**
     * @Route("/admin/users/restore/{hashid}", name="restore_user")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param int $hashid
     * @return Response
     */
    public function restore(int $hashid): Response
    {
        if($this->service->restore($hashid, User::class)) {
            $this->addFlash(MessageTypes::SuccessMessage,'The user has been restored');
        }
        return $this->redirectToRoute('list_archived_users');
    }

    /**
     * @Route("/admin/users/history/{hashid}", name="user_history")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param int $hashid
     * @return Response
     */
    public function history(int $hashid): Response
    {
        $results = $this->service->fetchHistory($hashid, User::class);

        if($results === null) {
            $this->addFlash(MessageTypes::WarningMessage, 'No history found');
            return  $this->redirectToRoute('list_users');
        }

        return $this->render('@generic/generic_list.html.twig', [
            'rows' => $results,
            'headers' => HistoryListVM::getHeaders(),
            'title' => 'History',
            'entity' => 'user',
            'center_columns' => ['version'],
        ]);
    }

    /**
     * @Route("/admin/users/revert/{hashid}/{version}", name="revert_user")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param int $hashid
     * @param int $version
     * @return Response
     */
    public function revert(int $hashid, int $version): Response
    {
        try {
            if ($this->service->revert($hashid, $version, User::class)) {
                $this->addFlash(MessageTypes::SuccessMessage, 'The user has been reverted');
            }
        } catch (\Exception $e) {
            $this->logger->error($e);
            $this->addFlash(MessageTypes::ErrorMessage, 'Sorry, we seem to have run into a snag processing your request');
        }
        return $this->redirectToRoute('list_users');
    }

    /**
     * @Route("/admin/users/check-email", name="user_email_unique")
     * @Security("is_granted('read', 'user')")
     * @param Request $request
     * @return Response
     */
    public function uniqueEmailCallback(Request $request): Response
    {
        $data =$request->get('user_form');

        if(!$data) {
            $data = $request->get('form')['userDetails'];
        }

        $result = false;

        try {
            if (key_exists('email', $data)) {
                $user = $this->service->findUserByEmail($data['email']);
                if($request->get('additionalfield')) {
                    $result = $user === null || $user->getId() === (int)$request->get('additionalfield');
                }else{
                    $result = $user === null;
                }
            }
        } catch (\Exception $e) {
            $this->logger->error($e);
        }
        return new Response($result === true ? 'true' : 'false',Response::HTTP_OK);
    }
}