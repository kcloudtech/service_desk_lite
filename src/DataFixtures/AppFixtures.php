<?php

namespace App\DataFixtures;

use App\Entity\Translation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $translation = (new Translation())->setTranslationKey('App Brand')->setDomain('messages')->setLocale('en')->setContent($_ENV['PROJECT_NAME']);
        $manager->persist($translation);

        $appStrings = [
            'Search',
            'Page',
            'Create',
            'Read',
            'Update',
            'Delete',
            'upload',
            'Recycle Bin',
            'Back to List',
            'Show',
            'Per Page',
            'Edit',
            'View',
            'Revert',
            'Restore',
            'Switch User',
            'History',
            'Are you sure you want to delete the record?',
            'Yes, delete it',
            'Cancel',
            'Are you sure you want to revert the record?',
            'Yes, revert it',
            'Save',
            'Next',
            'Previous',
            'welcome to',
            'Password Reset',
            'error',
            'take me home',
            'forbidden',
            'you do not have permission to access the requested page',
            'not found',
            'the page you are looking for can\'t be found. To try again,',
            'the page you are looking for can\'t be found.',
            'please check you\'ve entered the url correctly, or',
            'your online portal',
            'hi',
            'Your account has been created and is ready to use.',
            'To start using your account simply set a password.',
            'set password',
            'This link will expire in 24 hours',
            'if you did not request an account, please contact support',
            'kind regards',
            'password reset request',
            'Looks like you lost your password? Click on the button below to set a new password',
            'reset password',
            'if you did not request a password reset, please contact support',
            'Key Stats',
            'Use a trusted 2FA authenticator',
            'Profile',
            'Change Password',
            'Enable 2FA',
            'Disable 2FA',
            'Log Out',
            'home',
            'Exit impersonation of',
            'Powered by',
            'Loading',
            'New',
            'Already have an account?',
            'Sign In',
            '2FA Check',
            'Please open your 2FA app and generate a new code',
            'Enter your code',
            'Code',
            'Confirm',
            'Cancel Login?',
            'Enter and confirm your new password',
            'Ready to Login?',
            'Back to Login',
            'Password Reset Email Sent',
            'An email has been sent that contains a link to reset your password. This link will expire in 24 hours',
            'If you don\'t receive an email please check your spam folder',
            'good morning',
            'good afternoon',
            'good evening',
            'lets get started',
            'Email',
            'Password',
            'Remember Me',
            'don\'t have an account?',
            'register',
            'or sign in/register with',
            'forgot password?',
            'reset',
            'Forgotten Password',
            'Enter your email to request a password reset',
            'This link is no longer valid. You can try to reset your password again',
            'Try Again'
        ];

        foreach ($appStrings as $value) {
            $translation = (new Translation())->setTranslationKey($value)->setDomain('messages')->setLocale('en')->setContent($value);
            $manager->persist($translation);
        }

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['Default'];
    }
}
