<?php

namespace App\DataFixtures;

use App\Entity\MenuItem;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class MenuFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
         $menuItem = (new MenuItem())->setName('Home')->setRoute('home')->setMenuOrder(1)->setIcon('fas fa-home');
         $manager->persist($menuItem);

        $menuItem = (new MenuItem())->setName('Customers')->setRoute('list_customers')->setMenuOrder(2)->setIcon('fas fa-users');
        $manager->persist($menuItem);

        $ticketMenuItem = (new MenuItem())->setName('Tickets')->setRoute('#')->setMenuOrder(3)->setIcon('fas fa-ticket-alt');
        $manager->persist($ticketMenuItem);

        $menuItem = (new MenuItem())->setName('Overdue')->setRoute('list_overdue_tickets')->setMenuOrder(1)->setParentMenuItem($ticketMenuItem);
        $manager->persist($menuItem);

        $menuItem = (new MenuItem())->setName('My Tickets')->setRoute('list_my_tickets')->setMenuOrder(1)->setParentMenuItem($ticketMenuItem);
        $manager->persist($menuItem);

        $menuItem = (new MenuItem())->setName('All')->setRoute('list_tickets')->setMenuOrder(1)->setParentMenuItem($ticketMenuItem);
        $manager->persist($menuItem);

        $adminMenuItem = (new MenuItem())->setName('Admin')->setRoute('#')->setMenuOrder(4)->setIcon('fas fa-shield-alt');
        $manager->persist($adminMenuItem);

        $menuItem = (new MenuItem())->setName('Users')->setRoute('list_users')->setMenuOrder(1)->setParentMenuItem($adminMenuItem);
        $manager->persist($menuItem);

        $menuItem = (new MenuItem())->setName('Roles')->setRoute('list_roles')->setMenuOrder(2)->setParentMenuItem($adminMenuItem);
        $manager->persist($menuItem);

        $settingsMenuItem = (new MenuItem())->setName('Settings')->setRoute('#')->setMenuOrder(5)->setIcon('fas fa-tools');
        $manager->persist($settingsMenuItem);

        $menuItem = (new MenuItem())->setName('Translations')->setRoute('list_translations')->setMenuOrder(1)->setParentMenuItem($settingsMenuItem);
        $manager->persist($menuItem);

        $menuItem = (new MenuItem())->setName('Progress Points')->setRoute('list_progress_points')->setMenuOrder(1)->setParentMenuItem($settingsMenuItem);
        $manager->persist($menuItem);

        $menuItem = (new MenuItem())->setName('Statuses')->setRoute('list_stati')->setMenuOrder(1)->setParentMenuItem($settingsMenuItem);
        $manager->persist($menuItem);

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['Default'];
    }
}
