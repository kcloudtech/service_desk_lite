<?php

namespace App\DataFixtures;

use App\Entity\Role;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class RoleFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {

        $userRole = (new Role())->setName('ROLE_READ_ONLY');
        $manager->persist($userRole);
        $this->setReference('ROLE_READ_ONLY', $userRole);

        $userRole = (new Role())->setName('ROLE_USER');
        $manager->persist($userRole);
        $this->setReference('ROLE_USER', $userRole);

        $adminRole = (new Role())->setName('ROLE_ADMIN');
        $manager->persist($adminRole);
        $this->setReference('ROLE_ADMIN', $adminRole);

        $superAdminRole = (new Role())->setName('ROLE_SUPER_ADMIN');
        $manager->persist($superAdminRole);
        $this->setReference('ROLE_SUPER_ADMIN', $superAdminRole);

        $manager->flush();

    }

    public static function getGroups(): array
    {
        return ['Default', 'Test'];
    }
}
