<?php

namespace App\DataFixtures;

use App\Entity\Permission;
use App\Entity\SecurePage;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class RolePermissionFixtures extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {

        ## Role Admin ##
        $rolePermission = (new Permission())
            ->setCreate(true)
            ->setRead(true)
            ->setUpdate(true)
            ->setDelete(true)
            ->setPage($user = (new SecurePage())->setName('user'))
            ->setRole($this->getReference('ROLE_ADMIN'));
         $manager->persist($rolePermission);

        $rolePermission = (new Permission())
            ->setCreate(true)
            ->setRead(true)
            ->setUpdate(true)
            ->setDelete(true)
            ->setPage($role = (new SecurePage())->setName('role'))
            ->setRole($this->getReference('ROLE_ADMIN'));
        $manager->persist($rolePermission);

        ## Role User ##
        $rolePermission = (new Permission())
            ->setCreate(false)
            ->setRead(false)
            ->setUpdate(false)
            ->setDelete(false)
            ->setPage($user)
            ->setRole($this->getReference('ROLE_USER'));
        $manager->persist($rolePermission);

        $rolePermission = (new Permission())
            ->setCreate(true)
            ->setRead(false)
            ->setUpdate(false)
            ->setDelete(false)
            ->setPage($role)
            ->setRole($this->getReference('ROLE_USER'));
        $manager->persist($rolePermission);


    }

    public function getDependencies(): array
    {
        return array(
            RoleFixtures::class
        );
    }

    public static function getGroups(): array
    {
        return ['Default', 'Test'];
    }
}
