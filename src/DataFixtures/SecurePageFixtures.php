<?php

namespace App\DataFixtures;

use App\Entity\SecurePage;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class SecurePageFixtures extends Fixture  implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $page = (new SecurePage())->setName('user');
        $manager->persist($page);

        $page = (new SecurePage())->setName('role');
        $manager->persist($page);

        $page = (new SecurePage())->setName('settings');
        $manager->persist($page);

        $page = (new SecurePage())->setName('translation');
        $manager->persist($page);


        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['Default'];
    }
}
