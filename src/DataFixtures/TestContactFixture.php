<?php

namespace App\DataFixtures;

use App\Entity\Contact;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TestContactFixture extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $contact = (new Contact())
            ->setCustomer($this->getReference('TEST_CUSTOMER'))
            ->setFirstName('test')
        ->setLastName('contact')
        ->setEmail('testcontact@test.com')
        ->setLandline('01282 000000')
        ->setMobile('07872930600')
        ->setRole('Director');
        $manager->persist($contact);


        $contact = (new Contact())
            ->setCustomer($this->getReference('TEST_CUSTOMER'))
            ->setFirstName('deleted')
            ->setLastName('contact')
            ->setEmail('deletedcontact@test.com')
            ->setLandline('01282 100000')
            ->setMobile('07872930601')
            ->setRole('Manager')
        ->setArchived(date_create());
        $manager->persist($contact);

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return array(
            TestCustomerFixture::class
        );
    }

    public static function getGroups(): array
    {
        return ['Test'];
    }
}