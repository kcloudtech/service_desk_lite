<?php

namespace App\DataFixtures;

use App\Entity\Customer;
use App\Entity\Embedded\Address;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class TestCustomerFixture extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $customer = (new Customer())
            ->setName('test customer')
            ->setAddress((new Address())
                ->setAddress1('test street 1')
                ->setAddress2('test street')
                ->setTownOrCity('Town')
                ->setCounty('County')
                ->setPostcode('TT11 1TT'));
        $manager->persist($customer);
        $this->setReference('TEST_CUSTOMER', $customer);

        $customer = (new Customer())
            ->setName('deleted customer')
            ->setAddress((new Address())
                ->setAddress1('test street 1')
                ->setAddress2('test street')
                ->setTownOrCity('Town')
                ->setCounty('County')
                ->setPostcode('TT11 1TT'))
            ->setArchived(date_create());
        $manager->persist($customer);

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['Test'];
    }
}
