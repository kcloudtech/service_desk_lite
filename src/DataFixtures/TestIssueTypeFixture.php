<?php

namespace App\DataFixtures;

use App\Entity\IssueType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class TestIssueTypeFixture extends Fixture  implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
         $issueType = (new IssueType())
         ->setDescription('Type 1')
         ->setIcon('fa fa-bug');
          $this->setReference('TEST_ISSUE_TYPE', $issueType);
         $manager->persist($issueType);

        $issueType = (new IssueType())
            ->setDescription('deleted type')
            ->setIcon('fa fa-trash')
            ->setArchived(date_create());
        $manager->persist($issueType);

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['Test'];
    }
}
