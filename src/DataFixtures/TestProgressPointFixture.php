<?php

namespace App\DataFixtures;

use App\Entity\ProgressPoint;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class TestProgressPointFixture extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $point = (new ProgressPoint())
            ->setDescription('progress point')
            ->setPosition(1);
        $manager->persist($point);
        $this->setReference('TEST_POINT', $point);

        $point = (new ProgressPoint())
            ->setDescription('deleted point')
            ->setPosition(1)
            ->setArchived(date_create());
        $manager->persist($point);

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['Test'];
    }
}
