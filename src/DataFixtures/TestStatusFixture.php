<?php

namespace App\DataFixtures;

use App\Entity\Status;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TestStatusFixture extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $status = (new Status())
            ->setProgressPoint($this->getReference('TEST_POINT'))
            ->setDescription('status 1');
        $this->setReference('TEST_STATUS', $status);
        $manager->persist($status);


        $status = (new Status())
            ->setProgressPoint($this->getReference('TEST_POINT'))
            ->setDescription('deleted status')
            ->setArchived(date_create());
        $manager->persist($status);

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return array(
            TestProgressPointFixture::class
        );
    }

    public static function getGroups(): array
    {
        return ['Test'];
    }
}