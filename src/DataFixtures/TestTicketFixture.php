<?php

namespace App\DataFixtures;

use App\Entity\Ticket;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TestTicketFixture extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $ticket = (new Ticket())
            ->setSubject('test ticket')
            ->setDescription('test description for the test ticket')
            ->setCustomer($this->getReference('TEST_CUSTOMER'))
            ->setAssignedTo($this->getReference('SUPER_ADMIN'))
            ->setReporter($this->getReference('SUPER_ADMIN'))
            ->setIssueType($this->getReference('TEST_ISSUE_TYPE'))
            ->setStatus($this->getReference('TEST_STATUS'))
            ->setPriority(1);
        $manager->persist($ticket);

        $ticket = (new Ticket())
            ->setSubject('deleted ticket')
            ->setDescription('test description for the test ticket')
            ->setCustomer($this->getReference('TEST_CUSTOMER'))
            ->setAssignedTo($this->getReference('SUPER_ADMIN'))
            ->setReporter($this->getReference('SUPER_ADMIN'))
            ->setIssueType($this->getReference('TEST_ISSUE_TYPE'))
            ->setStatus($this->getReference('TEST_STATUS'))
            ->setPriority(1)
        ->setArchived(date_create());
        $manager->persist($ticket);


        $manager->flush();
    }

    public function getDependencies(): array
    {
        return array(
            TestStatusFixture::class,
            UserFixtures::class,
            TestIssueTypeFixture::class,
            TestCustomerFixture::class
        );
    }

    public static function getGroups(): array
    {
        return ['Test'];
    }
}
