<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TestUserFixtures extends Fixture implements FixtureGroupInterface, DependentFixtureInterface {

    public function load(ObjectManager $manager)
    {

        $user = (new User())
            ->setRole($this->getReference('ROLE_USER'))
            ->setFirstName('deleted')
            ->setLastName('user')
            ->setPassword('Password1')
            ->setEmail('deleteduser@test.com')
            ->setArchived(date_create());
        $manager->persist($user);

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return array(
            RoleFixtures::class
        );
    }

    public static function getGroups(): array
    {
        return ['Test'];
    }
}
