<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture implements FixtureGroupInterface, DependentFixtureInterface {

    public function load(ObjectManager $manager)
    {

        $user = (new User())
            ->setRole($this->getReference('ROLE_SUPER_ADMIN'))
            ->setFirstName('super')
            ->setLastName('admin')
            ->setPassword('Password1')
            ->setEmail('superadmin@test.com');
        $this->setReference('SUPER_ADMIN', $user);
        $manager->persist($user);


        $user = (new User())
            ->setRole($this->getReference('ROLE_USER'))
            ->setFirstName('test')
            ->setLastName('user')
            ->setPassword('Password1')
            ->setEmail('testuser@test.com');
        $manager->persist($user);

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return array(
            RoleFixtures::class
        );
    }

    public static function getGroups(): array
    {
        return ['Default'];
    }
}
