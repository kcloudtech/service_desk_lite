<?php

namespace App\Doctrine;

use AutoMapperPlus\AutoMapper;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use App\Entity\Permission;
use App\Entity\Role;
use App\Entity\User;
use App\Entity\UserPermission;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

/**
 * Class UserListener
 *
 * Set the default role of 'ROLE_SUPER_ADMIN' for new users who are created without a role i.e. self registration
 *
 * @package Listener
 */
class UserListener implements EventSubscriber
{

    /**
     * Returns a list of the events subscribed to
     *
     * @return string[]
     */
    public function getSubscribedEvents() :array
    {
        return [
            Events::prePersist
        ];
    }

    /**
     * Sets the user permissions for a new user from the role permission template
     *
     * @param LifecycleEventArgs $args
     * @throws UnregisteredMappingException
     */
    public function prePersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getEntity();
        if (!$entity instanceof User) {
            return;
        }

        if($entity->getRole() === null) {
            return;
        }

        if(count($entity->getUserPermissions()->getValues()) === 0){
            //Map the permissions from the role
            $mapper = AutoMapper::initialize(static function (AutoMapperConfig $config)  {
                $config->registerMapping(Permission::class, UserPermission::class);
            });

            foreach($entity->getRole()->getPermissions()->getValues() as $permission) {
                $userPermission = new UserPermission();
                $userPermission= $mapper->mapToObject($permission, $userPermission);
                $entity->addUserPermission($userPermission);
            }
        }
    }
}