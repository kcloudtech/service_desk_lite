<?php

namespace App\Entity;

use App\Repository\AttachmentRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=AttachmentRepository::class)
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="archived")
 * @Vich\Uploadable
 */
class Attachment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"view", "list"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Ticket::class, inversedBy="attachments")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"view", "list"})
     */
    private $ticket;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @Groups({"list"})
     */
    private $created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     * @Groups({"list"})
     */
    private $updated;

    /**
     * @ORM\Column(name="file", type="string", length=255)
     * @Gedmo\Versioned()
     *
     */
    private $path;

    /**
     * @var File | null
     * @Vich\UploadableField(mapping="ticket", fileNameProperty="path", size="size")
     */
    private $file;

    /**
     * @ORM\Column(type="string", length=64)
     * @Groups({"list"})
     * @Gedmo\Versioned()
     */
    private $filename;

    /**
     * @ORM\Column(type="string", length=8)
     * @Groups({"list"})
     * @Gedmo\Versioned()
     */
    private $size;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Versioned()
     */
    private $archived;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTicket(): ?Ticket
    {
        return $this->ticket;
    }

    public function setTicket(?Ticket $ticket): self
    {
        $this->ticket = $ticket;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(?\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * @return File|null
     */
    public function getFile(): ?File
    {
        return $this->file;
    }

    /**
     * @param File|null $file
     * @return Attachment
     */
    public function setFile(?File $file): self
    {
        $this->file = $file;
        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    public function getSize(): ?string
    {
        return $this->size;
    }

    public function setSize(string $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getArchived(): ?\DateTimeInterface
    {
        return $this->archived;
    }

    public function setArchived(?\DateTimeInterface $archived): self
    {
        $this->archived = $archived;

        return $this;
    }


}
