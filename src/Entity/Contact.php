<?php

namespace App\Entity;

use App\Repository\ContactRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ContactRepository::class)
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="archived")
 */
class Contact
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"view", "list"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Customer::class, inversedBy="contacts")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"view", "list"})
     */
    private $customer;

    /**
     * @ORM\Column(type="string", length=64)
     * @Gedmo\Versioned()
     * @Groups({"view", "list"})
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=64)
     * @Gedmo\Versioned()
     * @Groups({"view", "list"})
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=64)
     * @Gedmo\Versioned()
     * @Groups({"view", "list"})
     */
    private $role;

    /**
     * @ORM\Column(type="string", length=12, nullable=true)
     * @Gedmo\Versioned()
     * @Groups({"view", "list"})
     */
    private $landline;

    /**
     * @ORM\Column(type="string", length=12, nullable=true)
     * @Gedmo\Versioned()
     * @Groups({"view", "list"})
     */
    private $mobile;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     * @Gedmo\Versioned()
     * @Groups({"view", "list"})
     */
    private $email;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Versioned()
     */
    private $archived;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getLandline(): ?string
    {
        return $this->landline;
    }

    public function setLandline(?string $landline): self
    {
        $this->landline = $landline;

        return $this;
    }

    public function getMobile(): ?string
    {
        return $this->mobile;
    }

    public function setMobile(?string $mobile): self
    {
        $this->mobile = $mobile;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getArchived(): ?\DateTimeInterface
    {
        return $this->archived;
    }

    public function setArchived(?\DateTimeInterface $archived): self
    {
        $this->archived = $archived;

        return $this;
    }

    public function __toString(): string
    {
        return  ucwords(sprintf('%s %s', $this->firstName, $this->lastName));
    }

}
