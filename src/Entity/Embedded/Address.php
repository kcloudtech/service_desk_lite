<?php

namespace App\Entity\Embedded;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping\Embeddable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class Address
 *  @Embeddable
 * @Gedmo\Loggable()
 */
class Address
{
    /**
     * @ORM\Column(type="string", length=128)
     * @Gedmo\Versioned()
     * @Groups({"view", "list"})
     */
    private $address1;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     * @Gedmo\Versioned()
     * @Groups({"view"})
     */
    private $address2;

    /**
     * @ORM\Column(type="string", length=64)
     * @Gedmo\Versioned()
     * @Groups({"view", "list"})
     */
    private $townOrCity;

    /**
     * @ORM\Column(type="string", length=64)
     * @Gedmo\Versioned()
     * @Groups({"view", "list"})
     */
    private $county;

    /**
     * @ORM\Column(type="string", length=9)
     * @Gedmo\Versioned()
     * @Groups({"view", "list"})
     */
    private $postcode;

    public function getAddress1(): ?string
    {
        return $this->address1;
    }

    public function setAddress1(string $address1): self
    {
        $this->address1 = $address1;

        return $this;
    }

    public function getAddress2(): ?string
    {
        return $this->address2;
    }

    public function setAddress2(?string $address2): self
    {
        $this->address2 = $address2;

        return $this;
    }

    public function getTownOrCity(): ?string
    {
        return $this->townOrCity;
    }

    public function setTownOrCity(string $townOrCity): self
    {
        $this->townOrCity = $townOrCity;

        return $this;
    }

    public function getCounty(): ?string
    {
        return $this->county;
    }

    public function setCounty(string $county): self
    {
        $this->county = $county;

        return $this;
    }

    public function getPostcode(): ?string
    {
        return $this->postcode;
    }

    public function setPostcode(string $postcode): self
    {
        $this->postcode = $postcode;

        return $this;
    }

    public function __toString(): string
    {
        return sprintf('%s, %s', $this->address1, $this->townOrCity);
    }
}