<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Translatable;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MenuItemRepository")
 * @Gedmo\TranslationEntity(class="App\Entity\MenuItemTranslation")
 */
class MenuItem implements Translatable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(name="menu_item_id", type="integer")
     * @Groups({"view", "list"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=64)
     * @Gedmo\Translatable
     * @Groups({"view", "list"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=64)
     * @Groups({"view", "list"})
     */
    private $route;

    /**
     * @var array
     *
     * @ORM\Column(type="json", nullable=true)
     */
    private $params;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     * @Groups({"view", "list"})
     */
    private $icon;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    private $menuOrder = 0;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MenuItemTranslation", mappedBy="object", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $translations;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MenuItem" , fetch="EAGER", cascade={"persist", "remove"}, inversedBy="childMenuItems")
     * @ORM\JoinColumn(name="parent_menu_item_id", referencedColumnName="menu_item_id", onDelete="CASCADE")
     */
    private $parentMenuItem;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MenuItem", mappedBy="parentMenuItem")
     * @ORM\OrderBy({"menuOrder"="ASC"})
     */
    private $childMenuItems;

    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRoute(): ?string
    {
        return $this->route;
    }

    public function setRoute(string $route): self
    {
        $this->route = $route;

        return $this;
    }

    public function getParams(): array
    {
        return $this->params ?? [];
    }

    public function setParams($params): MenuItem
    {
        $this->params = $params;
        return $this;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(?string $icon): self
    {
        $this->icon = $icon;
        return $this;
    }

    public function getMenuOrder(): int
    {
        return $this->menuOrder;
    }

    public function setMenuOrder($menuOrder): MenuItem
    {
        $this->menuOrder = $menuOrder;
        return $this;
    }

    /**
     * @return Collection|MenuItemTranslation[]
     */
    public function getTranslations(): Collection
    {
        return $this->translations;
    }
    public function setTranslations($translations)
    {
        $this->translations = $translations;
    }

    public function addTranslation(MenuItemTranslation $t)
    {
        if (!$this->translations->contains($t)) {
            $this->translations[] = $t;
            $t->setObject($this);
        }
    }

    public function getParentMenuItem(): ?self
    {
        return $this->parentMenuItem;
    }

    public function setParentMenuItem(?self $parentMenuItem): self
    {
        $this->parentMenuItem = $parentMenuItem;

        return $this;
    }

    /**
     * @return MenuItem[]
     */
    public function getChildMenuItems()
    {
        return $this->childMenuItems;
    }

}
