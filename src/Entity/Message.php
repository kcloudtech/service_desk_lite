<?php

namespace App\Entity;

use App\Repository\MessageRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=MessageRepository::class)
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="archived")
 */
class Message
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"view", "list"})
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @Groups({"list"})
     */
    private $created;

    /**
     * @ORM\ManyToOne(targetEntity=Ticket::class, inversedBy="messages")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"view", "list"})
     */
    private $ticket;

    /**
     * @ORM\Column(type="integer")
     * @Gedmo\Versioned()
     * @Groups({"view", "list"})
     */
    private $userId;

    /**
     * @ORM\Column(type="string", length=128)
     * @Gedmo\Versioned()
     * @Groups({"view", "list"})
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     * @Gedmo\Versioned()
     * @Groups({"view", "list"})
     */
    private $content;

    /**
     * @ORM\Column(type="boolean")
     * @Gedmo\Versioned()
     * @Groups({"view", "list"})
     */
    private $internalOnly = false;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Versioned()
     */
    private $archived;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getTicket(): ?Ticket
    {
        return $this->ticket;
    }

    public function setTicket(?Ticket $ticket): self
    {
        $this->ticket = $ticket;

        return $this;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getInternalOnly(): ?bool
    {
        return $this->internalOnly;
    }

    public function setInternalOnly(bool $internalOnly): self
    {
        $this->internalOnly = $internalOnly;

        return $this;
    }

    public function getArchived(): ?\DateTimeInterface
    {
        return $this->archived;
    }

    public function setArchived(?\DateTimeInterface $archived): self
    {
        $this->archived = $archived;

        return $this;
    }

}
