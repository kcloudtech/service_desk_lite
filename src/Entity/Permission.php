<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PermissionRepository")
 * @ORM\Table(uniqueConstraints={@UniqueConstraint(name="unique_permission_idx",columns={"role_id", "page_id"})})
 */
class Permission
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="permission_id", type="integer")
     * @Groups({"view", "list"})
     */
    private $id;

    /**
     * @var Role
     * @ORM\ManyToOne(targetEntity="App\Entity\Role", inversedBy="permissions")
     * @ORM\JoinColumn(name="role_id", onDelete="cascade", nullable=false)
     * @Groups({"view", "list"})
     */
    private $role;

    /**
     * @var SecurePage
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\SecurePage", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="cascade", nullable=false)
     * @Groups({"view", "list"})
     */
    private $page;

    /**
     * @var boolean
     *
     * @ORM\Column(name="can_create", type="boolean", nullable=false)
     * @Groups({"view", "list"})
     */
    private $create = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="can_read", type="boolean", nullable=false)
     * @Groups({"view", "list"})
     */
    private $read = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="can_update", type="boolean", nullable=false)
     * @Groups({"view", "list"})
     */
    private $update = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="can_delete", type="boolean", nullable=false)
     * @Groups({"view", "list"})
     */
    private $delete = false;

    /**
     * @return Role
     */
    public function getRole(): Role
    {
        return $this->role;
    }

    /**
     * @param Role $role
     * @return Permission
     */
    public function setRole(Role $role): Permission
    {
        $this->role = $role;
        return $this;
    }

    /**
     * @return SecurePage
     */
    public function getPage(): ?SecurePage
    {
        return $this->page;
    }

    /**
     * @param SecurePage|null $page
     * @return Permission
     */
    public function setPage(?SecurePage $page): Permission
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return bool
     */
    public function getCreate(): ?bool
    {
        return $this->create;
    }

    /**
     * @param bool $create
     * @return Permission
     */
    public function setCreate(?bool $create): Permission
    {
        $this->create = $create;
        return $this;
    }

    /**
     * @return bool
     */
    public function getRead(): ?bool
    {
        return $this->read;
    }

    /**
     * @param bool $read
     * @return Permission
     */
    public function setRead(?bool $read): Permission
    {
        $this->read = $read;
        return $this;
    }

    /**
     * @return bool
     */
    public function getUpdate(): ?bool
    {
        return $this->update;
    }

    /**
     * @param bool $update
     * @return Permission
     */
    public function setUpdate(?bool $update): Permission
    {
        $this->update = $update;
        return $this;
    }

    /**
     * @return bool
     */
    public function getDelete(): ?bool
    {
        return $this->delete;
    }

    /**
     * @param bool $delete
     * @return Permission
     */
    public function setDelete(?bool $delete): Permission
    {
        $this->delete = $delete;
        return $this;
    }

    /**
     * @return int
     */
    public function getId() : ?int
    {
        return $this->id;
    }
}