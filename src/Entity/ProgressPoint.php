<?php

namespace App\Entity;

use App\Repository\ProgressPointRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ProgressPointRepository::class)
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="archived")
 */
class ProgressPoint
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"view", "list"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=32)
     * @Gedmo\Versioned()
     * @Groups({"view", "list"})
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     * @Gedmo\Versioned()
     * @Groups({"view", "list"})
     */
    private $position;

    /**
     * @ORM\OneToMany(targetEntity=Status::class, mappedBy="progressPoint")
     */
    private $statuses;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Versioned()
     */
    private $archived;

    public function __construct()
    {
        $this->statuses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return Collection|Status[]
     */
    public function getStatuses(): Collection
    {
        return $this->statuses;
    }

    public function addStatus(Status $status): self
    {
        if (!$this->statuses->contains($status)) {
            $this->statuses[] = $status;
            $status->setProgressPoint($this);
        }

        return $this;
    }

    public function removeStatus(Status $status): self
    {
        if ($this->statuses->removeElement($status)) {
            // set the owning side to null (unless already changed)
            if ($status->getProgressPoint() === $this) {
                $status->setProgressPoint(null);
            }
        }

        return $this;
    }

    public function getArchived(): ?\DateTimeInterface
    {
        return $this->archived;
    }

    public function setArchived(?\DateTimeInterface $archived): self
    {
        $this->archived = $archived;

        return $this;
    }

    public function __toString(): string
    {
        return ucwords($this->description);
    }
}
