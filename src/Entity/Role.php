<?php

namespace App\Entity;

use App\Repository\RoleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=RoleRepository::class)
 * @Gedmo\SoftDeleteable(fieldName="archived")
 * @Gedmo\Loggable()
 */
class Role
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"view", "list"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=128)
     * @Groups({"view", "list"})
     * @Gedmo\Versioned()
     */
    private $name;

    /**
     *
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="role")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="user_id", nullable=false)
     */
    private $users;

    /**
     * @var Permission
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Permission", mappedBy="role", cascade={"persist"})
     */
    private $permissions;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Versioned()
     */
    private $archived;


    /**
     * Role constructor.
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->permissions = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Permission[]
     */
    public function getPermissions()
    {
        return $this->permissions;
    }

    public function addPermission(Permission $permission): self
    {
        if (!$this->permissions->contains($permission)) {
            $this->permissions[] = $permission;
            $permission->setRole($this);
        }

        return $this;
    }

    public function removePermission(Permission $permission): self
    {
        if ($this->permissions->contains($permission)) {
            $this->permissions->removeElement($permission);
            // set the owning side to null (unless already changed)
            if ($permission->getRole() === $this) {
                $permission->setRole(null);
            }
        }

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getArchived(): ?\DateTimeInterface
    {
        return $this->archived;
    }

    /**
     * @param \DateTimeInterface|null $archived
     * @return $this
     */
    public function setArchived(?\DateTimeInterface $archived): self
    {
        $this->archived = $archived;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return ucwords(strtolower(str_replace(['ROLE_','_'], ['',' '],$this->name)));
    }
}
