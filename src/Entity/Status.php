<?php

namespace App\Entity;

use App\Entity\Translations\StatusTranslation;
use App\Repository\StatusRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=StatusRepository::class)
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="archived")
 * @Gedmo\TranslationEntity(class="App\Entity\Translations\StatusTranslation")
 */
class Status implements Translatable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"view", "list"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=32)
     * @Gedmo\Versioned()
     * @Groups({"view", "list"})
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=ProgressPoint::class, inversedBy="statuses")
     * @ORM\JoinColumn(nullable=false)
     * @Gedmo\Versioned()
     * @Groups({"view", "list"})
     */
    private $progressPoint;

    /**
     * @ORM\OneToMany(targetEntity=Ticket::class, mappedBy="status")
     */
    private $tickets;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Translations\StatusTranslation", mappedBy="object", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $translations;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Versioned()
     */
    private $archived;

    public function __construct()
    {
        $this->tickets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getProgressPoint(): ?progressPoint
    {
        return $this->progressPoint;
    }

    public function setProgressPoint(?progressPoint $progressPoint): self
    {
        $this->progressPoint = $progressPoint;

        return $this;
    }

    /**
     * @return Collection|Ticket[]
     */
    public function getTickets(): Collection
    {
        return $this->tickets;
    }

    public function addTicket(Ticket $ticket): self
    {
        if (!$this->tickets->contains($ticket)) {
            $this->tickets[] = $ticket;
            $ticket->setStatus($this);
        }

        return $this;
    }

    public function removeTicket(Ticket $ticket): self
    {
        if ($this->tickets->removeElement($ticket)) {
            // set the owning side to null (unless already changed)
            if ($ticket->getStatus() === $this) {
                $ticket->setStatus(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|StatusTranslation[]
     */
    public function getTranslations(): Collection
    {
        return $this->translations;
    }

    public function setTranslations($translations): void
    {
        $this->translations = $translations;
    }

    public function addTranslation(StatusTranslation $t): void
    {
        if (!$this->translations->contains($t)) {
            $this->translations[] = $t;
            $t->setObject($this);
        }
    }

    public function getArchived(): ?\DateTimeInterface
    {
        return $this->archived;
    }

    public function setArchived(\DateTimeInterface $archived): self
    {
        $this->archived = $archived;

        return $this;
    }

    public function __toString(): string
    {
        return ucwords($this->description);
    }
}
