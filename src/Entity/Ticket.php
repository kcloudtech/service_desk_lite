<?php

namespace App\Entity;

use App\Repository\TicketRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=TicketRepository::class)
 * @Gedmo\Loggable()
 *  @Gedmo\SoftDeleteable(fieldName="archived")
 */
class Ticket
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"view", "list"})
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @Groups({"list"})
     */
    private $created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     * @Groups({"list"})
     */
    private $updated;

    /**
     * @ORM\ManyToOne(targetEntity=Customer::class, inversedBy="tickets")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"view", "list"})
     */
    private $customer;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="tickets")
     * @ORM\JoinColumn(nullable=false)
     * @Gedmo\Versioned()
     * @Groups({"view", "list"})
     */
    private $assignedTo;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="reportedTickets")
     * @ORM\JoinColumn(nullable=false)
     * @Gedmo\Versioned()
     * @Groups({"view", "list"})
     */
    private $reporter;

    /**
     * @ORM\ManyToOne(targetEntity=IssueType::class, inversedBy="tickets")
     * @ORM\JoinColumn(nullable=false)
     * @Gedmo\Versioned()
     */
    private $issueType;

    /**
     * @ORM\Column(type="string", length=64)
     * @Gedmo\Versioned()
     * @Groups({"view", "list"})
     */
    private $subject;

    /**
     * @ORM\Column(type="text")
     * @Gedmo\Versioned()
     * @Groups({"view", "list"})
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     * @Gedmo\Versioned()
     * @Groups({"view", "list"})
     */
    private $priority;

    /**
     * @ORM\ManyToOne(targetEntity=Status::class, inversedBy="tickets")
     * @ORM\JoinColumn(nullable=false)
     * @Gedmo\Versioned()
     * @Groups({"view", "list"})
     */
    private $status;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Versioned()
     * @Groups({"view", "list"})
     */
    private $startBy;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Versioned()
     * @Groups({"view", "list"})
     */
    private $dueBy;

    /**
     * @ORM\OneToMany(targetEntity=Attachment::class, mappedBy="ticket")
     */
    private $attachments;

    /**
     * @ORM\OneToMany(targetEntity=Message::class, mappedBy="ticket")
     */
    private $messages;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $archived;

    public function __construct()
    {
        $this->product = new ArrayCollection();
        $this->attachments = new ArrayCollection();
        $this->messages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(?\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getAssignedTo(): ?user
    {
        return $this->assignedTo;
    }

    public function setAssignedTo(?user $assignedTo): self
    {
        $this->assignedTo = $assignedTo;

        return $this;
    }

    public function getReporter(): ?user
    {
        return $this->reporter;
    }

    public function setReporter(?user $reporter): self
    {
        $this->reporter = $reporter;

        return $this;
    }

    public function getIssueType(): ?IssueType
    {
        return $this->issueType;
    }

    public function setIssueType(?IssueType $issueType): self
    {
        $this->issueType = $issueType;

        return $this;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }

    public function setPriority(int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    public function getStatus(): ?Status
    {
        return $this->status;
    }

    public function setStatus(?Status $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getStartBy(): ?\DateTimeInterface
    {
        return $this->startBy;
    }

    public function setStartBy(?\DateTimeInterface $startBy): self
    {
        $this->startBy = $startBy;

        return $this;
    }

    public function getDueBy(): ?\DateTimeInterface
    {
        return $this->dueBy;
    }

    public function setDueBy(?\DateTimeInterface $dueBy): self
    {
        $this->dueBy = $dueBy;

        return $this;
    }

    /**
     * @return Collection|Attachment[]
     */
    public function getAttachments(): Collection
    {
        return $this->attachments;
    }

    public function addAttachment(Attachment $attachment): self
    {
        if (!$this->attachments->contains($attachment)) {
            $this->attachments[] = $attachment;
            $attachment->setTicket($this);
        }

        return $this;
    }

    public function removeAttachment(Attachment $attachment): self
    {
        if ($this->attachments->removeElement($attachment)) {
            // set the owning side to null (unless already changed)
            if ($attachment->getTicket() === $this) {
                $attachment->setTicket(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Message[]
     */
    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function addMessage(Message $message): self
    {
        if (!$this->messages->contains($message)) {
            $this->messages[] = $message;
            $message->setTicket($this);
        }

        return $this;
    }

    public function removeMessage(Message $message): self
    {
        if ($this->messages->removeElement($message)) {
            // set the owning side to null (unless already changed)
            if ($message->getTicket() === $this) {
                $message->setTicket(null);
            }
        }

        return $this;
    }

    public function getArchived(): ?\DateTimeInterface
    {
        return $this->archived;
    }

    public function setArchived(?\DateTimeInterface $archived): self
    {
        $this->archived = $archived;

        return $this;
    }
    
}
