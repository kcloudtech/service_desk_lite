<?php

namespace App\Entity;

use App\Repository\TranslationRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TranslationRepository")
 * @ORM\Table(uniqueConstraints={@UniqueConstraint(name="unique_entity_idx",columns={"domain", "locale", "translation_key"})})
 * @Gedmo\SoftDeleteable(fieldName="archived")
 * @Gedmo\Loggable()
 */
class Translation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(name="translation_id", type="integer")
     * @Groups({"view", "list"})
     *
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=64)
     * @Groups({"view", "list"})
     */
    private $domain;

    /**
     * @ORM\Column(type="string", length=8)
     * @Groups({"view", "list"})
     */
    private $locale;

    /**
     * @ORM\Column(type="string", length=128)
     * @Groups({"view", "list"})
     */
    private $translationKey;

    /**
     * @ORM\Column(type="string", type="text")
     * @Groups({"view"})
     * @Gedmo\Versioned()
     */
    private $content;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $archived;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDomain(): ?string
    {
        return $this->domain;
    }

    public function setDomain(string $domain): self
    {
        $this->domain = $domain;

        return $this;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function getTranslationKey(): ?string
    {
        return $this->translationKey;
    }

    public function setTranslationKey(string $translationKey): self
    {
        $this->translationKey = $translationKey;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getArchived(): ?\DateTime
    {
        return $this->archived;
    }

    /**
     * @param \DateTime|null $archived
     * @return Translation
     */
    public function setArchived(?\DateTime $archived): Translation
    {
        $this->archived = $archived;
        return $this;
    }

}
