<?php

namespace App\Entity;

use App\Repository\UserRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Scheb\TwoFactorBundle\Model\Google as google;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="archived")
 * @Vich\Uploadable
 */
class User implements UserInterface,  google\TwoFactorInterface, \Serializable
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"view", "list"})
     */
    private $id;

    /**
     * @var Role
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Role", inversedBy="users")
     * @ORM\JoinColumn(nullable=false)
     * @Gedmo\Versioned()
     * @Groups({"view", "list"})
     */
    private $role;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserPermission", mappedBy="user", cascade={"persist"})
     */
    private $userPermissions;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     * @Groups({"list"})
     */
    private $updated;

    /**
     * @var string
     * @ORM\Column(name="googleAuthenticatorSecret", type="string", nullable=true)
     *
     */
    private $googleAuthenticatorSecret;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default":0})
     */
    private $totpConfirmed = 0;

    /**
     * @var File | null
     * @Vich\UploadableField(mapping="user", fileNameProperty="path")
     */
    private $profilePicture;

    /**
     * @var string | null
     * @ORM\Column(name="profile_picture", type="string", length=128, nullable=true)
     * @Gedmo\Versioned()
     */
    private $path;

    /**
     * @ORM\Column(type="string", length=128)
     * @Groups({"view", "list"})
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=128)
     * @Groups({"view", "list"})
     */
    private $lastName;

    /**
     * @var string
     * @ORM\Column(type="string", length=128, unique=true)
     * @Groups({"view", "list"})
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(type="string", length=128)
     * @Groups({"view"})
     */
    private $password;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"list"})
     */
    private $lastLogin;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"list"})
     */
    private $lastFailedLogin;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"list"})
     */
    private $failedLoginAttempts = 0;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Gedmo\Versioned()
     */
    private $token;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $tokenExpires;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"view", "list"})
     */
    private $isActive = true;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $archived;

    /**
     * @ORM\OneToMany(targetEntity=Ticket::class, mappedBy="assignedTo")
     */
    private $tickets;

    /**
     * @ORM\OneToMany(targetEntity=Ticket::class, mappedBy="reported")
     */
    private $reportedTickets;

    public function __construct()
    {
        $this->userPermissions = new ArrayCollection();
        $this->products = new ArrayCollection();
        $this->tickets = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }


    /**
     * @return int|null
     */
    public function getTotpConfirmed(): ?int
    {
        return $this->totpConfirmed;
    }

    /**
     * @param int $totpConfirmed
     * @return $this
     */
    public function setTotpConfirmed(int $totpConfirmed): self
    {
        $this->totpConfirmed = $totpConfirmed;

        return $this;
    }

    /**
     * @return File|null
     */
    public function getProfilePicture(): ?File
    {
        return $this->profilePicture;
    }

    /**
     * @param File|null $profilePicture
     */
    public function setProfilePicture(?File $profilePicture): void
    {
        $this->profilePicture = $profilePicture;
    }

    /**
     * @return string|null
     */
    public function getPath(): ?string
    {
        return $this->path;
    }

    /**
     * @param string|null $path
     */
    public function setPath(?string $path): void
    {
        $this->path = $path;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return $this
     */
    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return $this
     */
    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getLastLogin(): ?DateTimeInterface
    {
        return $this->lastLogin;
    }

    /**
     * @param DateTimeInterface|null $lastLogin
     * @return $this
     */
    public function setLastLogin(?DateTimeInterface $lastLogin): self
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getLastFailedLogin(): ?DateTimeInterface
    {
        return $this->lastFailedLogin;
    }

    /**
     * @param DateTimeInterface|null $lastFailedLogin
     * @return $this
     */
    public function setLastFailedLogin(?DateTimeInterface $lastFailedLogin): self
    {
        $this->lastFailedLogin = $lastFailedLogin;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getFailedLoginAttempts(): ?int
    {
        return $this->failedLoginAttempts;
    }

    /**
     * @param int|null $failedLoginAttempts
     * @return $this
     */
    public function setFailedLoginAttempts(?int $failedLoginAttempts): self
    {
        $this->failedLoginAttempts = $failedLoginAttempts;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @param string|null $token
     * @return $this
     */
    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getTokenExpires(): ?DateTimeInterface
    {
        return $this->tokenExpires;
    }

    /**
     * @param DateTimeInterface|null $tokenExpires
     * @return $this
     */
    public function setTokenExpires(?DateTimeInterface $tokenExpires): self
    {
        $this->tokenExpires = $tokenExpires;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     * @return $this
     */
    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getCreated(): ?DateTimeInterface
    {
        return $this->created;
    }

    /**
     * @param DateTimeInterface $created
     * @return $this
     */
    public function setCreated(DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getUpdated(): ?DateTimeInterface
    {
        return $this->updated;
    }

    /**
     * @param DateTimeInterface|null $updated
     * @return $this
     */
    public function setUpdated(?DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getArchived(): ?DateTimeInterface
    {
        return $this->archived;
    }

    /**
     * @param DateTimeInterface|null $archived
     * @return $this
     */
    public function setArchived(?DateTimeInterface $archived): self
    {
        $this->archived = $archived;

        return $this;
    }

    public function getRole(): ?Role
    {
        return $this->role;
    }

    public function setRole(?Role $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function isGoogleAuthenticatorEnabled(): bool
    {
        return $this->googleAuthenticatorSecret && $this->totpConfirmed;
    }

    public function getGoogleAuthenticatorUsername(): string
    {
        return $this->email;
    }

    /**
     * @return string|null
     */
    public function getGoogleAuthenticatorSecret(): ?string
    {
        return $this->googleAuthenticatorSecret;
    }

    /**
     * @param string|null $googleAuthenticatorSecret
     */
    public function setGoogleAuthenticatorSecret(?string $googleAuthenticatorSecret): void
    {
        $this->googleAuthenticatorSecret = $googleAuthenticatorSecret;
    }

    public function getRoles(): array
    {
        return [$this->role->getName()];
    }

    public function getSalt(): string
    {
        //Doesn't matter because salt is randomly generated by Symfony
        return '';
    }

    public function eraseCredentials(): bool
    {
        return false;
    }

    public function getUsername(): string
    {
        return $this->email;
    }

    /**
     * @return Collection|UserPermission[]
     */
    public function getUserPermissions()
    {
        return $this->userPermissions ?? new ArrayCollection();
    }

    public function addUserPermission(UserPermission $userPermission): self
    {
        if($this->userPermissions === null) {
            $this->userPermissions = new ArrayCollection();
        }
        if (!$this->userPermissions->contains($userPermission)) {
            $this->userPermissions[] = $userPermission;
            $userPermission->setUser($this);
        }

        return $this;
    }

    public function removeUserPermission(UserPermission $userPermission): self
    {
        if ($this->userPermissions->contains($userPermission)) {
            $this->userPermissions->removeElement($userPermission);
            // set the owning side to null (unless already changed)
            if ($userPermission->getUser() === $this) {
                $userPermission->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Ticket[]
     */
    public function getTickets(): Collection
    {
        return $this->tickets;
    }

    public function addTicket(Ticket $ticket): self
    {
        if (!$this->tickets->contains($ticket)) {
            $this->tickets[] = $ticket;
            $ticket->setAssignedTo($this);
        }

        return $this;
    }

    public function removeTicket(Ticket $ticket): self
    {
        if ($this->tickets->removeElement($ticket)) {
            // set the owning side to null (unless already changed)
            if ($ticket->getAssignedTo() === $this) {
                $ticket->setAssignedTo(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Ticket[]
     */
    public function getReportedTickets(): Collection
    {
        return $this->reportedTickets;
    }

    public function addReportedTicket(Ticket $ticket): self
    {
        if (!$this->reportedTickets->contains($ticket)) {
            $this->reportedTickets[] = $ticket;
            $ticket->setReporter($this);
        }

        return $this;
    }

    public function removeReportedTicket(Ticket $ticket): self
    {
        if ($this->reportedTickets->removeElement($ticket)) {
            // set the owning side to null (unless already changed)
            if ($ticket->getReporter() === $this) {
                $ticket->setReporter(null);
            }
        }

        return $this;
    }

    public function serialize(): string
    {
        return serialize(array(
            $this->id,
            $this->email,
            $this->password,
            $this->role,
        ));
    }

    /** @param $serialized
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->email,
            $this->password,
            $this->role,
            ) = unserialize($serialized, array('allowed_classes' => false));
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return  ucwords(sprintf('%s %s', $this->firstName, $this->lastName));
    }

}
