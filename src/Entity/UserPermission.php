<?php

namespace App\Entity;

use App\Repository\UserPermissionRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=UserPermissionRepository::class)
 * @ORM\Table(uniqueConstraints={@UniqueConstraint(name="unique_user_permission_idx",columns={"user_id", "page_id"})})
 */
class UserPermission
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(name="user_permission_id", type="integer")
     * @Groups({"list", "view"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userPermissions")
     * @ORM\JoinColumn(name="user_id", nullable=false)
     * @Groups({"list", "view"})
     */
    private $user;

    /**
     * @var SecurePage
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\SecurePage")
     * @ORM\JoinColumn(onDelete="cascade", nullable=false)
     * @Groups({"view", "list"})
     */
    private $page;

    /**
     * @ORM\Column(name="can_create", type="boolean")
     * @Groups({"list", "view"})
     */
    private $create;

    /**
     * @ORM\Column(name="can_read", type="boolean")
     * @Groups({"list", "view"})
     */
    private $read;

    /**
     * @ORM\Column(name="can_update", type="boolean")
     * @Groups({"list", "view"})
     */
    private $update;

    /**
     * @ORM\Column(name="can_delete", type="boolean")
     * @Groups({"list", "view"})
     */
    private $delete;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?user
    {
        return $this->user;
    }

    public function setUser(?user $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return SecurePage
     */
    public function getPage(): ?SecurePage
    {
        return $this->page;
    }

    /**
     * @param SecurePage|null $page
     * @return UserPermission
     */
    public function setPage(?SecurePage $page): UserPermission
    {
        $this->page = $page;
        return $this;
    }

    public function getCreate(): ?bool
    {
        return $this->create;
    }

    public function setCreate(bool $create): self
    {
        $this->create = $create;

        return $this;
    }

    public function getRead(): ?bool
    {
        return $this->read;
    }

    public function setRead(bool $read): self
    {
        $this->read = $read;

        return $this;
    }

    public function getUpdate(): ?bool
    {
        return $this->update;
    }

    public function setUpdate(bool $update): self
    {
        $this->update = $update;

        return $this;
    }

    public function getDelete(): ?bool
    {
        return $this->delete;
    }

    public function setDelete(bool $delete): self
    {
        $this->delete = $delete;

        return $this;
    }
}
