<?php

namespace App\EventListeners;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\Security\Core\Event\AuthenticationEvent;
use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;

/**
 * Class AuthenticationListener
 *
 * Capture login attempt information
 * 
 * @package Listener
 */
class AuthenticationListener implements EventSubscriberInterface
{
    /** @var EntityManagerInterface */
    private $em;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * AuthenticationListener constructor.
     * @param EntityManagerInterface $emMaster
     * @param RequestStack $requestStack
     */
    public function __construct(EntityManagerInterface $emMaster, RequestStack $requestStack)
    {
        $this->em = $emMaster;
        $this->requestStack = $requestStack;
    }


    public static function getSubscribedEvents(): array
    {
        return array(
            AuthenticationEvents::AUTHENTICATION_FAILURE => 'onSecurityAuthenticationFailure',
            AuthenticationEvents::AUTHENTICATION_SUCCESS => 'onSecurityAuthenticationSuccess'
        );
    }

    public function onSecurityAuthenticationSuccess(AuthenticationEvent $event): void
    {
        // Get the User entity.
        /** @var User $user */
        $user = $event->getAuthenticationToken()->getUser();

        if($user instanceof User) {
            //reload to attach entity
            $user = $this->em->getRepository(User::class)->findOneBy(['email' => $user->getEmail()]);

            $user->setLastLogin(date_create())
                ->setFailedLoginAttempts(0);
            $this->em->persist($user);
            $this->em->flush();
        }
    }

    /**
     * @param AuthenticationFailureEvent $event
     */
    public function onSecurityAuthenticationFailure(AuthenticationFailureEvent $event): void
    {
        if(!$this->requestStack->getParentRequest()){
            /** @var User $user */
            $user = $this->em->getRepository(User::class)->findOneBy(['email' => $event->getAuthenticationToken()->getUsername()]);

            if($user)  {
                $user->setFailedLoginAttempts($user->getFailedLoginAttempts()+1)
                    ->setLastFailedLogin(date_create());
                $this->em->persist($user);
                $this->em->flush();
            }
        }
    }
}