<?php

namespace App\Form\FieldTypes;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class AutoCompleteType extends AbstractType
{
    public function getParent(): ?string
    {
        return TextType::class;
    }
}