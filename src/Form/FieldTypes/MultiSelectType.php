<?php

namespace App\Form\FieldTypes;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MultiSelectType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'expanded' => false,
            'multiple' => true,
            'allow_add' =>false,
        ]);
    }

    public function getParent(): ?string
    {
        return EntityType::class;
    }
}