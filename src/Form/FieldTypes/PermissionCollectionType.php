<?php

namespace App\Form\FieldTypes;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class PermissionCollectionType extends AbstractType
{
    public function getParent(): ?string
    {
        return CollectionType::class;
    }
}