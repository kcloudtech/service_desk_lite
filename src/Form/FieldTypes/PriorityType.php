<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PriorityType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'choices' => [
                "Please Select" => 0,
                "Low" => 1,
                "Normal" => 2,
                "High" => 3,
                "Urgent" => 4,
            ]
        ]);
    }

    public function getParent(): ?string
    {
        return ChoiceType::class;
    }
}