<?php

namespace App\Form\FieldTypes;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class ProfilePictureType extends AbstractType
{
    public function getParent(): ?string
    {
        return FileType::class;
    }
}