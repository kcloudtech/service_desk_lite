<?php

namespace App\Form\FormTypes;

use App\Entity\Permission;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Utils\Form\Type\SwitchType;

class PermissionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Permission $data */
        $builder
            ->add('page', TextType::class, [
                'label_attr' => array('class' => 'bmd-label-floating'),
            ])
            ->add('create', SwitchType::class, [
                'label_attr' => array('class' => 'bmd-label-floating'),
            ])
            ->add('read', SwitchType::class, [
                'label_attr' => array('class' => 'bmd-label-floating'),
            ])
            ->add('update', SwitchType::class, [
                'label_attr' => array('class' => 'bmd-label-floating'),
            ])
            ->add('delete', SwitchType::class, [
                'label_attr' => array('class' => 'bmd-label-floating'),
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Permission::class,
        ]);
    }
}
