<?php

namespace App\Form\FormTypes;

use App\Entity\Permission;
use App\Entity\Role;
use App\Entity\User;
use App\Helpers\RoleRestrictions;
use App\Repository\RoleRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Utils\FormType;

class UserForm extends FormType
{
    private $token;

    public function __construct(ValidatorInterface $validator, RouterInterface $router, TranslatorInterface $translator, TokenStorageInterface $token)
    {
        parent::__construct($validator, $router, $translator);
        $this->token = $token;
    }

    public function buildForm(FormBuilderInterface $builder, array $options):void
    {
        parent::buildForm($builder, $options);

        /** @var User $user */
        $user= $this->token->getToken()->getUser();

        $conditions = RoleRestrictions::getRestrictions($user);

        $formOptions = [
            'class' => Role::class,
            'choice_name' => 'name',
            'choice_label' => function (?Role $entity) {
                return $entity ? ucwords(strtolower(str_replace(['ROLE_','_'], ['',' '], $entity->getName()))) : '';
            },
            'query_builder' => function (RoleRepository $roleRepository) use ($conditions) {
                $query = $roleRepository->createQueryBuilder('r')
                    ->leftJoin('r.permissions', 'p');
                foreach ($conditions as $page => $condition) {
                    $actions = '';
                    foreach ($condition as $action) {
                        $actions.= " AND p.$action = 0";
                    }
                    $query->andWhere("(p.page = :page $actions)")
                    ->setParameter('page', $page);
                }
                return $query;
            },
        ];
        $builder->add('role', EntityType::class, $formOptions);
    }
}