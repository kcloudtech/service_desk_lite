<?php

namespace App\Form\FormTypes;

use App\Entity\User;
use App\Entity\UserPermission;
use App\Helpers\RoleRestrictions;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Utils\Form\Type\SwitchType;

class UserPermissionType  extends AbstractType
{
    private $token;

    public function __construct(TokenStorageInterface $token)
    {
        $this->token = $token;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var User $user */
        $user= $this->token->getToken()->getUser();
        $conditions = RoleRestrictions::getRestrictions($user);

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) use ($conditions){
            $form = $event->getForm();

            /** @var UserPermission $data */
            $data = $form->getData();

            if(key_exists($data->getPage(), $conditions)) {
                foreach ($conditions as $page => $condition) {
                    foreach ($condition as $action){
                        $form->add($action, SwitchType::class, ['disabled'=> true]);
                    }
                }
            }
        });

        /** @var UserPermission $data */
        $builder
            ->add('page', TextType::class, [
            ])
            ->add('create', SwitchType::class, [
            ])
            ->add('read', SwitchType::class, [
            ])
            ->add('update', SwitchType::class, [
            ])
            ->add('delete', SwitchType::class, [
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserPermission::class,
        ]);
    }
}