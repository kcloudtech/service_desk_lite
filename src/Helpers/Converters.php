<?php

namespace App\Helpers;

use Utils\TwigHelpers;

class Converters
{
    public static function getStatusColour(string $status): ?string
    {
        switch ($status)
        {
            case 'New':
                return 'badge-warning';
            case 'Active':
                return 'badge-success';
            case 'Inactive':
                return 'grey';
            case 'Expired':
                return 'warning-color-dark';
            case 'Overdue':
                return 'badge-danger';
        }
        return '';
    }

    public static function getPriority(int $priority): string
    {
        switch ($priority)
        {
            case 1:
                return 'low';
            case 2:
                return 'normal';
            case 3:
                return 'high';
            case 4:
                return 'urgent';
        }

        return '';
    }

    public static function getPriorityColour(int $priority): string
    {
        switch ($priority)
        {
            case 1:
                return 'badge-info';
            case 2:
                return 'badge-success';
            case 3:
                return 'badge-warning';
            case 4:
                return 'badge-danger';
        }

        return '';
    }

    public static function getPriorityIcon(int $priority): string
    {
        switch ($priority)
        {
            case 1:
                return 'angle-down';
            case 2:
                return 'circle';
            case 3:
                return 'angle-up';
            case 4:
                return 'angle-double-up';
        }

        return '';
    }

    public function deadlineColour(\DateTime $deadline): string
    {
        try {
            $days = (int)(new TwigHelpers())->diffInDays($deadline);
            if($days < 0) {
                return 'badge-danger';
            }
            if($days === 0 || $days === 1) {
                return 'badge-warning';
            }

            if($days > 7) {
                return '';
            }

            return 'badge-info';

        } catch (\Exception $e) {}
        return '';
    }

    public function deadlineText(\DateTime $deadline): string
    {
        try {
            switch ($days =  (new TwigHelpers())->diffInDays($deadline)) {
                case 0:
                    return 'today';
                case -1:
                    return 'yesterday';
                case 1:
                    return 'tomorrow';
            }
            if($days > 30) {
                return $deadline->format('d/m/Y');
            }
            return  (new TwigHelpers())->humanizeDateTime($deadline);
        } catch (\Exception $e) {
        }
        return '';
    }

    public function lightColourCycle(int $index): string
    {
        switch ($index)
        {
            case 1:
                return 'green';
            case 2:
                return 'light-green';
            case 3:
                return 'yellow';
            case 4:
                return 'orange';
            case 5:
                return 'pink';
            case 6:
                return 'red';
        }
        return 'blue';
    }

    public function colourCycle(int $index): string
    {
        switch ($index)
        {
            case 1:
                return 'blue';
            case 2:
                return 'indigo';
            case 3:
                return 'deep-purple';
            case 4:
                return 'purple';
            case 5:
                return 'pink';
            case 6:
                return 'red';
        }
        return 'blue';
    }

    public static function getFileIcon(string $filename): ?string
    {
        $parts = explode('.', $filename);

        switch (end($parts)) {
            case 'csv':
            case 'xls':
            case 'xlsx':
            return '/assets/images/excel.png';
            case 'doc':
            case 'docx':
            return '/assets/images/word.png';
            case 'txt':
                return '/assets/images/txt.png';
            case  'pdf':
                return '/assets/images/pdf.png';
            case 'png':
            case 'gif':
            case 'jpg':
            case 'jpeg':
                return null;
            default:
                return '/assets/images/file.png';
        }
    }

    public static function truncateText(string $text, int $limit = 15): string
    {
        return strlen($text) > 15 ? (substr($text, 0, $limit) . '...') : $text;
    }

}