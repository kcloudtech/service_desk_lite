<?php

namespace App\Helpers;

use App\Entity\Attachment;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\DirectoryNamerInterface;

class CustomerDirectoryNamer implements DirectoryNamerInterface
{
    public function directoryName($object, PropertyMapping $mapping): string
    {
        return $object->getTicket()->getCustomer();
    }
}