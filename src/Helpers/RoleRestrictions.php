<?php

namespace App\Helpers;

use App\Entity\Permission;
use App\Entity\Role;
use App\Entity\User;

class RoleRestrictions
{
    /**
     * Constructs an array that contains all the pages and crud actions that the user does not have access to
     * Used to determine what the user should not be able to grant or amend for another user
     *
     * @param User $user
     * @return array
     */
    public static function getRestrictions(User  $user): array
    {
        $restrictions = $user->getRole()->getPermissions()->filter(static function (Permission $permission){
            return !$permission->getCreate() || !$permission->getRead() || !$permission->getUpdate() || !$permission->getDelete();
        });
        $conditions = [];
        foreach ($restrictions as $restriction) {
            if (!$restriction->getCreate()) {
                $conditions[$restriction->getPage()][] = 'create';
            }
            if (!$restriction->getRead()) {
                $conditions[$restriction->getPage()][] = 'read';
            }
            if (!$restriction->getUpdate()) {
                $conditions[$restriction->getPage()][] = 'update';
            }
            if (!$restriction->getDelete()) {
                $conditions[$restriction->getPage()][] = 'delete';
            }
        }

        return $conditions;
    }

    /**
     * Constructs an array that contains all the pages and crud actions that the user does not have access to
     * Used to determine what the user should not be able to grant or amend for another user
     *
     * @param Role $role
     * @return array
     */
    public static function getRestrictionsForRole(Role $role): array
    {
        $restrictions = $role->getPermissions()->filter(static function (Permission $permission){
            return !$permission->getCreate() || !$permission->getRead() || !$permission->getUpdate() || !$permission->getDelete();
        });
        $conditions = [];
        foreach ($restrictions as $restriction) {
            if (!$restriction->getCreate()) {
                $conditions[$restriction->getPage()][] = 'create';
            }
            if (!$restriction->getRead()) {
                $conditions[$restriction->getPage()][] = 'read';
            }
            if (!$restriction->getUpdate()) {
                $conditions[$restriction->getPage()][] = 'update';
            }
            if (!$restriction->getDelete()) {
                $conditions[$restriction->getPage()][] = 'delete';
            }
        }

        return $conditions;
    }

    /**
     * Checks if role1 has higher privileges than role2
     *
     * @param Role $role1
     * @param Role $role2
     * @return bool
     */
    public static function hasHigherPrivileges(Role $role1, Role $role2): bool
    {
        $role1Restrictions = self::getRestrictionsForRole($role1);
        $role2Restrictions = self::getRestrictionsForRole($role2);

        $results = array_filter($role2Restrictions, static function($page, $key) use ($role1Restrictions){
            if(!key_exists($key, $role1Restrictions)) {return true;}
            foreach ($page as $action) {
                if(!in_array($action, $role1Restrictions[$key], true)) {return true;}
            }
            return  false;
        }, ARRAY_FILTER_USE_BOTH );

        return count($results) > 0;
    }

}