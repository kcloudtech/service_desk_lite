<?php

namespace App\Loaders;

use App\Entity\Translation;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Translation\Loader\LoaderInterface;
use Symfony\Component\Translation\MessageCatalogue;

/**
 * Class DbLoader
 *
 * A custom translation loader that loads translations not related to an entity from the database
 *
 * @author Kellsey
 * @package Translation
 */
class DbLoader implements LoaderInterface
{

    /**
     * @var EntityManagerInterface
     */
    private $em;


    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function load($resource, string $locale, string $domain = 'messages')
    {
        $catalogue = new MessageCatalogue($locale);

        $translations = $this->em->getRepository(Translation::class)->findByLocalAndDomain($domain, $locale);

        /* @var Translation $translation */
        foreach($translations as $translation)
        {
            $catalogue->set($translation->getTranslationKey(), $translation->getContent(), $translation->getDomain());
        }

        return $catalogue;
    }
}