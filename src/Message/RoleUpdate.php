<?php

namespace App\Message;

class RoleUpdate
{
    /**
     * @var int
     */
    private $roleId;

    /**
     * @var array
     */
    private $overrides;

    /**
     * RoleUpdate constructor.
     * @param int $roleId
     * @param array $overrides
     */
    public function __construct(int $roleId, array $overrides)
    {
        $this->roleId = $roleId;
        $this->overrides = $overrides;
    }

    public function getOverrides() :array
    {
        return $this->overrides;
    }

    public function getRoleId(): int
    {
        return $this->roleId;
    }
}