<?php

namespace App\MessageHandler;

use App\Entity\Permission;
use App\Entity\Role;
use App\Entity\User;
use App\Entity\UserPermission;
use App\Message\RoleUpdate;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class UpdatePermissionsHandler implements MessageHandlerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * UpdatePermissionsHandler constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Updates each users permissions to be the same as the role permissions
     * Skips records that are overriding the existing role permissions
     * @todo  This is to be refactored when the management of user permissions has been improved
     *
     * @param RoleUpdate $message
     */
    public function __invoke(RoleUpdate $message)
    {
        /** @var Role $role */
        $role = $this->em->getRepository(Role::class)->find($message->getRoleId());
        $users = $this->em->getRepository(User::class)->findBy(['role' => $role]);

        $overrides = $message->getOverrides();


        /** @var User $user */
        foreach ($users as $user) {
            //Get the overrides for the user
            $userOverrides =  $overrides[$user->getId()];

            /** @var UserPermission $permission */
            foreach($user->getUserPermissions() as $permission) {
                $criteria = new Criteria();
                $criteria ->where(Criteria::expr()->eq("page", $permission->getPage()))->getFirstResult();

                /** @var ArrayCollection $item */
                $item = ($role->getPermissions()->matching($criteria))->getValues();
                if($item && count($item) > 0) {
                    /** @var Permission $item */
                    $item = current($item);

                    if(!in_array(strtolower($item->getPage()), array_column($userOverrides, 'page'), true)) {
                        $permission
                            ->setCreate($item->getCreate())
                            ->setRead($item->getRead())
                            ->setUpdate($item->getUpdate())
                            ->setDelete($item->getDelete());
                    }
                }
            }
            $this->em->persist($user);
        }

        $this->em->flush();
    }
}