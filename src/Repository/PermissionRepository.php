<?php

namespace App\Repository;

use App\Entity\Permission;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Permission|null find($id, $lockMode = null, $lockVersion = null)
 * @method Permission|null findOneBy(array $criteria, array $orderBy = null)
 * @method array findAll()
 * @method array (array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PermissionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Permission::class);
    }

    /**
     * Gets a distinct list of pages associated to role permissions
     *
     * @return int|mixed|string
     */
    public function getUniquePages()
    {
        return $this->createQueryBuilder('p')
            ->select('p.page')
            ->distinct(true)
            ->getQuery()
            ->execute();
    }

}