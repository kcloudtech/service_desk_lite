<?php

namespace App\Repository;

use App\Entity\ProgressPoint;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProgressPoint|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProgressPoint|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProgressPoint[]    findAll()
 * @method ProgressPoint[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProgressPointRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProgressPoint::class);
    }

    /**
     * Fetches all progress points as array
     *
     * @return array|int|string
     */
    public function fetchAllAsArray()
    {
        return $this->createQueryBuilder('p')
            ->getQuery()
            ->getArrayResult();
    }
}
