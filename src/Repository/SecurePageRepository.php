<?php

namespace App\Repository;

use App\Entity\SecurePage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SecurePage|null find($id, $lockMode = null, $lockVersion = null)
 * @method SecurePage|null findOneBy(array $criteria, array $orderBy = null)
 * @method SecurePage[]    findAll()
 * @method SecurePage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SecurePageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SecurePage::class);
    }

    // /**
    //  * @return SecurePage[] Returns an array of SecurePage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SecurePage
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
