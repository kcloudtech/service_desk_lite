<?php

namespace App\Repository;

use App\Entity\Ticket;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Ticket|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ticket|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ticket[]    findAll()
 * @method Ticket[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TicketRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ticket::class);
    }

    /**
     * Fetches a list of overdue tickets
     *
     * @return Ticket[]
     */
    public function fetchOverdue(): array
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.dueBy < DATE(NOW())')
            ->getQuery()
            ->getResult();
    }

    /**
     * Fetches a list of tickets by assigned user
     *
     * @param User $user
     * @return Ticket[]
     */
    public function fetchByAssignee(User $user): array
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.assignedTo = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }
}
