<?php

namespace App\Repository;

use App\Entity\Translation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Translation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Translation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Translation[]    findAll()
 * @method Translation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TranslationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Translation::class);
    }

    /**
     * Returns a list of translations in array format that match the given domain and locale
     *
     * @param string $domain
     * @param string $locale
     * @return Translation[] | null
     */
    public function findByLocalAndDomain(string $domain, string $locale): ?array
    {
        return $this->createQueryBuilder('t')
            ->where('t.domain = :domain')
            ->setParameter('domain', $domain)
            ->andWhere('t.locale = :locale')
            ->setParameter('locale', $locale)
            ->getQuery()
            ->execute();
    }

    /**
     * @param string $domain
     * @return Translation[] | null
     */
    public function findByDomain(string $domain): ?array
    {
        return $this->createQueryBuilder('t')
            ->where('t.domain = :domain')
            ->setParameter('domain', $domain)
            ->getQuery()
            ->execute();
    }

    /**
     * Gets a list of available locales
     *
     * @return array|null
     */
    public function findLocales(): ?array
    {
        return $this->createQueryBuilder('t')
            ->select('t.locale')
            ->distinct()
            ->getQuery()
            ->execute();
    }
}
