<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method array  findAll()
 * @method array   findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Fetches a list of users whose Id is in the given list
     * @param array $ids
     * @return User[]
     */
    public function findIn(array $ids): array
    {
        return $this->createQueryBuilder('u')
            ->where('u.id in (:ids)')
            ->setParameter('ids', $ids)
            ->getQuery()
            ->getResult();
    }

    /**
     * Fetches a list of users that are in a managers role
     * @return User[]
     */
    public function getManagers(): array
    {
        return $this->createQueryBuilder('u')
            ->innerJoin('u.role', 'r')
            ->where('r.name like :search')
            ->setParameter('search', '%manager%')
            ->getQuery()
            ->getResult();
    }

    /**
     * Fetches a list of users that are not in managers roles and not associated to the manager already
     *
     * @param User $manager
     * @return User[]
     */
    public function getUsers(User $manager): array
    {
        return $this->createQueryBuilder('u')
            ->innerJoin('u.role', 'r')
            ->where('r.name not like :search')
            ->setParameter('search', '%manager%')
            ->andWhere('(u.reportsTo != :manager OR u.reportsTo IS NULL)')
            ->setParameter('manager', $manager)
            ->getQuery()
            ->getResult();
    }

    /**
     * Fetches a list of users that have an associated manager
     * @return User[]
     */
    public function getUsersInTeams(): array
    {
        return $this->createQueryBuilder('u')
            ->innerJoin('u.reportsTo', 'm')
            ->getQuery()
            ->getResult();
    }

    /**
     * Fetches a list of users that are associated to the given manager
     * @param int $manager
     * @return User[]
     */
    public function getUsersForManager(int $manager): array
    {
        return $this->createQueryBuilder('u')
            ->innerJoin('u.reportsTo', 'm')
            ->where('m.id =:manager')
            ->setParameter('manager', $manager)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $roleId
     * @param int $userId
     * @return mixed[]
     * @throws DBALException|Exception
     */
    public function getOverriddenPermissionPages(int $roleId, int $userId): array
    {
        $sql = "SELECT DISTINCT(LOWER(p.page)) as 'page' FROM (
                    SELECT p.page, p.can_create, p.can_read, p.can_update, p.can_delete, 'role' AS 'type' FROM permission p
                      WHERE p.role_id = $roleId
                      UNION
                      SELECT up.page, up.can_create, up.can_read, up.can_update, up.can_delete, 'user' AS 'type' FROM user_permission up
                      WHERE up.user_id = $userId) p
                      GROUP BY p.page, p.can_create, p.can_read, p.can_update, p.can_delete
                      HAVING COUNT(*) = 1";

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();
        return  $stmt->fetchAll();
    }

}
