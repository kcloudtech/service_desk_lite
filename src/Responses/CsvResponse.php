<?php

namespace App\Responses;

use League\Csv\Writer;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class CsvResponse extends BinaryFileResponse
{
    public function __construct(array $headers, array $rows, string $fileName = null, string $disposition = ResponseHeaderBag::DISPOSITION_ATTACHMENT)
    {
        parent::__construct($this->generateCsv($headers, $rows), 200, []);
        $this->setContentDisposition($disposition, ($fileName ?? str_replace('.tmp','.csv',$this->getFile()->getFilename())));
    }

    private function generateCsv(array $headers, array $rows): string
    {
        $tmp = tempnam(sys_get_temp_dir(), "");
        $csvFile = Writer::createFromPath($tmp);
        try {
            $csvFile->insertOne($headers);
            $csvFile->insertAll($rows);
        } catch (\League\Csv\CannotInsertRecord $e) {
        }
        return $tmp;
    }

}