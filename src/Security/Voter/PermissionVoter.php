<?php

namespace App\Security\Voter;

use App\Entity\Permission;
use App\Entity\User;
use App\Entity\UserPermission;
use AutoMapperPlus\AutoMapper;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class PermissionVoter
 *
 * Provides CRUD level authorisation
 * @author Kellsey
 * @package Security Voter
 */
class PermissionVoter extends Voter
{
    private const CREATE = 'create';
    private const READ = 'read';
    private const UPDATE = 'update';
    private const DELETE = 'delete';

    protected function supports($attribute, $subject): bool
    {
        return in_array($attribute, [self::CREATE, self::READ, self::UPDATE, self::DELETE], true);
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        /** @var User $user */
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        if($user->getRoles()[0] === 'ROLE_SUPER_ADMIN') {
            return true;
        }

        switch ($attribute) {
            case self::CREATE:
                return $this->getPagePermissions($subject, $user)->getCreate()?:false;
            case self::READ:
                return $this->getPagePermissions($subject, $user)->getRead()?:false;
            case self::UPDATE:
                return $this->getPagePermissions($subject, $user)->getUpdate()?:false;
            case self::DELETE:
                return $this->getPagePermissions($subject, $user)->getDelete()?:false;
        }

        return false;
    }

    /**
     * Finds the role and user permissoin for the given page name and user 
     * 
     * @param string $page
     * @param User $user
     * @return Permission|mixed
     */
    private function getPagePermissions(string $page, User $user): Permission
    {
        $roleResults = $user->getRole()->getPermissions();
        $userResults = $user->getUserPermissions();

        if($page === 'Home') {
            return (new Permission())->setRead(true);
        }

        $rolePermission =  array_filter($roleResults->toArray(), static function (Permission $e) use (&$page) {
            return  (strtolower($e->getPage()) === strtolower($page)) || (strtolower($e->getPage()->getAlias()) === strtolower($page));
        });

        $userPermission =  array_filter($userResults->toArray(), static function (UserPermission $e) use (&$page) {
            return strtolower($e->getPage()) === strtolower($page) || (strtolower($e->getPage()->getAlias()) === strtolower($page)) ;
        });

        if(count($rolePermission) > 0 ) {
            if(count($userPermission) > 0) {
                //User permissions override role permissions
                $mapper = AutoMapper::initialize(function (AutoMapperConfig $config) {
                    $config->registerMapping(UserPermission::class, Permission::class);
                    $config->getOptions()->createUnregisteredMappings();
                });
                return $mapper->mapToObject(current($userPermission), current($rolePermission));
            }
            return current($rolePermission);
        }

        //Return an empty entity to deny
        return new Permission();
    }
}
