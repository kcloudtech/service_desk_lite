<?php

namespace App\Services;

use App\Entity\Contact;
use App\ViewModels\Contact\ContactVM;
use App\ViewModels\Contact\ContactListVM;
use Utils\Crud;
use Utils\ViewModel;
use Doctrine\ORM\EntityManagerInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Utils\History;
use Utils\SoftDelete;

/**
 * Class ContactService
 *
 * Provides CRUD operations and logic for managing Contacts
 * @package Services
 * @author kells
 */
class ContactService
{
    use Crud;
    use History;
    use SoftDelete;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ContactService constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    
    /**
     *  Fetches a list of Contacts in a list view friendly view model
     * If a page number is provided the results will be paginated otherwise all results will be returned
     *
     * @param int $page The page number if paginating results
     * @param int $perPage The number of items per page
     * @return ContactListVM[]
     */
    public function fetchAll(int$page = 0, int $perPage = 15): array
    {
        if($page  === 0) {
            $page = null;
            $perPage = null;
        }
        $results = $this->em->getRepository(Contact::class)->findBy([], null, $perPage, $page);

        return ContactListVM::mapCollection($results);
    }
    
    /**
     * Adds the Contact to the database
     * The data is mapped to the entity from the provided view model
     *
     * @param ViewModel $vm The view model to map the data from
     * @return int Returns the id of the inserted entity
     * @throws UnregisteredMappingException
     */
    public function addContact(ViewModel $vm): int
    {
        $entity = $this->create($vm, Contact::class);
        return $entity->getId();
    }

    /**
     * Updates the Contact in the database
     * The data is updated using the given View Model
     *
     * @param ViewModel $vm The view model to map the data from
     * @return bool Returns true
     * @throws UnregisteredMappingException
     */
    public function updateContact(ViewModel $vm): bool
    {
        $this->fetchAndUpdate($vm, Contact::class);
        return true;
    }

    /**
     * Delete a Contact
     * The Contact with the given Id will be deleted
     *
     * @param int $id The id of the entity to delete
     * @return bool Returns true
     */
    public function deleteContact(int $id): bool
    {
        return $this->delete($id, Contact::class);
    }
}