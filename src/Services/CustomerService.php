<?php

namespace App\Services;

use App\Entity\Customer;
use App\Entity\Embedded\Address;
use App\ViewModels\Customer\CustomerVM;
use App\ViewModels\Customer\CustomerListVM;
use App\ViewModels\HistoryListVM;
use Gedmo\Loggable\Entity\LogEntry;
use Utils\Crud;
use Utils\ViewModel;
use Doctrine\ORM\EntityManagerInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Utils\History;
use Utils\SoftDelete;

/**
 * Class CustomerService
 *
 * Provides CRUD operations and logic for managing Customers
 * @package Services
 * @author kells
 */
class CustomerService
{
    use Crud;
    use History;
    use SoftDelete;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * CustomerService constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    
    /**
     *  Fetches a list of Customers in a list view friendly view model
     * If a page number is provided the results will be paginated otherwise all results will be returned
     *
     * @param int $page The page number if paginating results
     * @param int $perPage The number of items per page
     * @return CustomerListVM[]
     */
    public function fetchAll(int$page = 0, int $perPage = 15): array
    {
        if($page  === 0) {
            $page = null;
            $perPage = null;
        }
        $results = $this->em->getRepository(Customer::class)->findBy([], null, $perPage, $page);

        return CustomerListVM::mapCollection($results);
    }
    
    /**
     * Adds the Customer to the database
     * The data is mapped to the entity from the provided view model
     *
     * @param ViewModel $vm The view model to map the data from
     * @return int Returns the id of the inserted entity
     */
    public function addCustomer(ViewModel $vm): int
    {
        /** @var Customer $entity */
        $entity = $vm->mapTo(new Customer());
        $entity->setAddress($vm->mapTo(new Address()));

        $this->em->persist($entity);
        $this->em->flush();

        return $entity->getId();
    }

    /**
     * Updates the Customer in the database
     * The data is updated using the given View Model
     *
     * @param CustomerVM $vm The view model to map the data from
     * @return bool Returns true
     */
    public function updateCustomer(CustomerVM $vm): bool
    {
        $entity = $this->em->getRepository(Customer::class)->find($vm->getId());
        $entity = $vm->mapTo($entity);
        $entity->setAddress($vm->mapTo(new Address()));

        $this->em->persist($entity);
        $this->em->flush();

        return true;
    }

    /**
     * Delete a Customer
     * The Customer with the given Id will be deleted
     *
     * @param int $id The id of the entity to delete
     * @return bool Returns true
     */
    public function deleteCustomer(int $id): bool
    {
        return $this->delete($id, Customer::class);
    }
}