<?php

namespace App\Services;

use Utils\Crud;
use Utils\ViewModel;
use App\ViewModels\User\ChangePasswordVM;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;

class HomeService
{
    use Crud;

    /**
     * @var EntityManagerInterface
     */
    private $em;


    /**
     * UserService constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Updates the user in the database
     *
     * The data is updated using the given View Model
     *
     * @param ViewModel $vm The view model to map the data from
     * @return bool Returns true
     * @throws UnregisteredMappingException
     */
    public function updateUser(ViewModel $vm): bool
    {
        $this->fetchAndUpdate($vm, User::class);
        return true;
    }

    /**
     * Changes the users password
     *
     * The data is updated using the given View Model
     *
     * @param ChangePasswordVM $vm The view model to map the data from
     * @return bool Returns true
     */
    public function changePassword(ChangePasswordVM $vm): bool
    {
        /** @var User $user */
        $user = $this->get($vm->getId(), User::class);
        $user->setPassword($vm->getNewPassword());

        $this->em->persist($user);
        $this->em->flush();

        return true;
    }

    /**
     * Sets the secret value used for 2FA for the user
     *
     * @param string|null $secret
     * @param int $userId
     */
    public function setUserSecret(?string $secret, int $userId): void
    {
        /** @var User $user */
        $user = $this->get($userId, User::class);
        $user->setGoogleAuthenticatorSecret($secret);
        $user->setTotpConfirmed(false);
        $this->em->persist($user);
        $this->em->flush();
    }

    /**
     * Enables 2FA once the code has been confirmed
     *
     * @param int $userId
     */
    public function confirmCode(int $userId): void
    {
        /** @var User $user */
        $user = $this->get($userId, User::class);
        $user->setTotpConfirmed(true);
        $this->em->persist($user);
        $this->em->flush();
    }
}