<?php

namespace App\Services;

use App\Entity\IssueType;
use App\ViewModels\IssueType\IssueTypeVM;
use App\ViewModels\IssueType\IssueTypeListVM;
use Utils\Crud;
use Utils\ViewModel;
use Doctrine\ORM\EntityManagerInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Utils\History;
use Utils\SoftDelete;

/**
 * Class IssueTypeService
 *
 * Provides CRUD operations and logic for managing IssueTypes
 * @package Services
 * @author kells
 */
class IssueTypeService
{
    use Crud;
    use History;
    use SoftDelete;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * IssueTypeService constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    
    /**
     *  Fetches a list of IssueTypes in a list view friendly view model
     * If a page number is provided the results will be paginated otherwise all results will be returned
     *
     * @param int $page The page number if paginating results
     * @param int $perPage The number of items per page
     * @return IssueTypeListVM[]
     */
    public function fetchAll(int$page = 0, int $perPage = 15): array
    {
        if($page  === 0) {
            $page = null;
            $perPage = null;
        }
        $results = $this->em->getRepository(IssueType::class)->findBy([], null, $perPage, $page);

        return IssueTypeListVM::mapCollection($results);
    }
    
    /**
     * Adds the IssueType to the database
     * The data is mapped to the entity from the provided view model
     *
     * @param ViewModel $vm The view model to map the data from
     * @return int Returns the id of the inserted entity
     * @throws UnregisteredMappingException
     */
    public function addIssueType(ViewModel $vm): int
    {
        $entity = $this->create($vm, IssueType::class);
        return $entity->getId();
    }

    /**
     * Updates the IssueType in the database
     * The data is updated using the given View Model
     *
     * @param ViewModel $vm The view model to map the data from
     * @return bool Returns true
     * @throws UnregisteredMappingException
     */
    public function updateIssueType(ViewModel $vm): bool
    {
        $this->fetchAndUpdate($vm, IssueType::class);
        return true;
    }

    /**
     * Delete a IssueType
     * The IssueType with the given Id will be deleted
     *
     * @param int $id The id of the entity to delete
     * @return bool Returns true
     */
    public function deleteIssueType(int $id): bool
    {
        return $this->delete($id, IssueType::class);
    }
}