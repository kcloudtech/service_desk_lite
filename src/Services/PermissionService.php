<?php

namespace App\Services;

use App\ViewModels\Permission\PermissionListVM;
use App\Entity\Permission;
use Utils\Crud;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class PermissionService
 *
 * Provides CRUD operations and logic for managing role permissions
 * @author Kellsey
 * @package Services
 */
class PermissionService
{
    use Crud;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * PermissionService constructor.
     * @param EntityManagerInterface $emMaster
     */
    public function __construct(EntityManagerInterface $emMaster)
    {
        $this->em = $emMaster;
    }

    /**
     *  Fetches a list of permissions in a list view friendly view model
     *
     * If a page number is provided the results will be paginated otherwise all results will be returned
     *
     * @param int $page The page number if paginating results
     * @param int $perPage The number of items per page
     * @return PermissionListVM[]
     */
    public function fetchAll(int $page = 0, int $perPage = 15): array
    {
        $results = $this->em->getRepository(Permission::class)->findBy([], null, $perPage, $page);

        return PermissionListVM::mapCollection($results);
    }

}