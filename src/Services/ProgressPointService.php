<?php

namespace App\Services;

use App\Entity\ProgressPoint;
use App\ViewModels\ProgressPoint\ProgressPointVM;
use App\ViewModels\ProgressPoint\ProgressPointListVM;
use Utils\Crud;
use Utils\ViewModel;
use Doctrine\ORM\EntityManagerInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Utils\History;
use Utils\SoftDelete;

/**
 * Class ProgressPointService
 *
 * Provides CRUD operations and logic for managing ProgressPoints
 * @package Services
 * @author kells
 */
class ProgressPointService
{
    use Crud;
    use History;
    use SoftDelete;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ProgressPointService constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    
    /**
     *  Fetches a list of ProgressPoints in a list view friendly view model
     * If a page number is provided the results will be paginated otherwise all results will be returned
     *
     * @param int $page The page number if paginating results
     * @param int $perPage The number of items per page
     * @return ProgressPointListVM[]
     */
    public function fetchAll(int$page = 0, int $perPage = 15): array
    {
        if($page  === 0) {
            $page = null;
            $perPage = null;
        }
        $results = $this->em->getRepository(ProgressPoint::class)->findBy([], null, $perPage, $page);

        return ProgressPointListVM::mapCollection($results);
    }
    
    /**
     * Adds the ProgressPoint to the database
     * The data is mapped to the entity from the provided view model
     *
     * @param ViewModel $vm The view model to map the data from
     * @return int Returns the id of the inserted entity
     * @throws UnregisteredMappingException
     */
    public function addProgressPoint(ViewModel $vm): int
    {
        $entity = $this->create($vm, ProgressPoint::class);
        return $entity->getId();
    }

    /**
     * Updates the ProgressPoint in the database
     * The data is updated using the given View Model
     *
     * @param ViewModel $vm The view model to map the data from
     * @return bool Returns true
     * @throws UnregisteredMappingException
     */
    public function updateProgressPoint(ViewModel $vm): bool
    {
        $this->fetchAndUpdate($vm, ProgressPoint::class);
        return true;
    }

    /**
     * Delete a ProgressPoint
     * The ProgressPoint with the given Id will be deleted
     *
     * @param int $id The id of the entity to delete
     * @return bool Returns true
     */
    public function deleteProgressPoint(int $id): bool
    {
        return $this->delete($id, ProgressPoint::class);
    }
}