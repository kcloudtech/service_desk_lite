<?php

namespace App\Services;

use App\Entity\SecurePage;
use App\Message\RoleUpdate;
use App\ViewModels\HistoryListVM;
use App\Entity\Permission;
use App\Entity\Role;
use App\Entity\User;
use App\ViewModels\Role\RoleVM;
use Gedmo\Loggable\Entity\LogEntry;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\NotifierInterface;
use Utils\Crud;
use App\ViewModels\Role\RoleListVM;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Doctrine\ORM\EntityManagerInterface;
use function Symfony\Component\String\u;

/**
 * Class RoleService
 *
 * Provides CRUD operations and logic for managing user roles
 * @author Kellsey
 * @package Services
 */
class RoleService
{
    use Crud;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var MessageBusInterface
     */
    private $bus;

    /**
     * @var NotifierInterface
     */
    private $notifier;


    /**
     * RoleService constructor.
     * @param EntityManagerInterface $emMaster
     * @param MessageBusInterface $bus
     * @param NotifierInterface $notifier
     */
    public function __construct(EntityManagerInterface $emMaster, MessageBusInterface $bus, NotifierInterface $notifier)
    {
        $this->em = $emMaster;
        $this->bus = $bus;
        $this->notifier = $notifier;
    }

    /**
     * Fetches a list of roles in a list view friendly view model
     *
     * If a page number is provided the results will be paginated otherwise all results will be returned
     *
     * @param int $page The page number if paginating results
     * @param int $perPage The number of items per page
     * @return RoleListVM[]
     */
    public function fetchAll(int $page = 0, int $perPage = 15): array
    {
        $results = $this->em->getRepository(Role::class)->findBy([], null, $perPage, $page);

        return RoleListVM::mapCollection($results);
    }

    public function fetchPermissionSet(): array
    {
        $pages = $this->em->getRepository(SecurePage::class)->findAll();

        $permissions = [];

        /** @var Permission $page */
        foreach ($pages as $page) {
            $permissions[] = (new Permission())->setPage($page);
        }
        return $permissions;
    }

    /**
     * Adds the Role to the database
     *
     * The data is mapped to the entity from the provided view model
     *
     * @param RoleVM $vm The view model to map the data from
     * @return int Returns the id of the inserted entity
     */
    public function addRole(RoleVM $vm): int
    {
        if(u($vm->getName())->trim()->upper()->equalsTo('SUPER ADMIN')) {
            $this->notifier->send((new Notification('Super Admin is a reserved role that cannot be re-used or changed', ['browser'])));
            return false;
        }

        /** @var Role $entity */
        $entity = $vm->mapTo(new Role());

        foreach ($entity->getPermissions() as $permission) {
            $permission->setRole($entity);
        }
        $this->em->persist($entity);
        $this->em->flush();

        return $entity->getId();
    }

    /**
     * Updates the role in the database
     *
     * The data is updated using the given View Model
     *
     * @param RoleVM $vm The view model to map the data from
     * @return bool
     * @throws UnregisteredMappingException
     */
    public function updateRole(RoleVM $vm): bool
    {

        if(u($vm->getName())->upper()->startsWith('INTERNAL_')) {
            $this->notifier->send((new Notification('Internal is reserved. You cannot create a role with the prefix internal', ['browser'])));
            return false;
        }

        //must get overrides per user before updating the role
        //Todo: Move all this logic to a worker
        $users = $this->em->getRepository(User::class)->findBy(['role' => $vm->getId()]);

        $overrides = [];
        foreach ($users as $user) {
            $overrides[$user->getId()] = $this->em->getRepository(User::class)->getOverriddenPermissionPages($vm->getId(), $user->getId());
        }

        $this->fetchAndUpdate($vm, Role::class);

        $this->bus->dispatch(new RoleUpdate($vm->getId(), $overrides));

        return true;
    }

    /**
     * Delete a Role
     *
     * The role with the given Id will be soft deleted from the database providing there are no active users with the role
     *
     * @param int $id The id of the entity to delete
     * @return bool Returns false if there are users associated to the role
     */
    public function deleteRole(int $id): bool
    {
        $role = $this->em->getRepository(User::class)->findBy(['role' => $id]);
        if(count($role) > 0){
            $this->notifier->send((new Notification('Roles can only be deleted when there are no users associated with them', ['browser'])));
        }

        return $this->delete($id, Role::class);
    }

    /**
     * Gets a list of entities that have been soft deleted
     *
     * @return RoleListVM[]
     */
    public function fetchArchived(): array
    {
        $this->em->getFilters()->disable('softdeleteable');
        $results = $this->em->getRepository(Role::class)
            ->createQueryBuilder('r')
            ->where('r.archived IS NOT NULL')
            ->orderBy('r.archived', 'DESC')
            ->getQuery()
            ->execute();
        $this->em->clear();
        $this->em->getFilters()->enable('softdeleteable');

        if(!$results) {
            return [];
        }

        return RoleListVM::mapCollection($results);
    }

    /**
     * Restores a soft deleted Role
     *
     * The role with the given Id will be restored so that it is once again a live entity
     *
     * @param int $id The id of the entity to restore
     * @return bool
     */
    public function restore(int $id): bool
    {
        $this->em->getFilters()->disable('softdeleteable');
        /** @var Role $role */
        $role = $this->em->getRepository(Role::class)->find($id);
        if(!$role){
            $this->notifier->send((new Notification('Unable to restore record', ['browser'])));
        }

        $role->setArchived(null);
        $this->em->persist($role);
        $this->em->flush();

        $this->em->clear();
        $this->em->getFilters()->enable('softdeleteable');

        return true;
    }

    /**
     * Gets a list of logged history for the entity
     *
     * Only the columns tagged as versioned are logged
     *
     * @param int $id
     * @return HistoryListVM[]
     */
    public function fetchHistory(int $id): ?array
    {
        $entity = $this->get($id, Role::class);
        if(!$entity) {
            $this->notifier->send((new Notification('Record not found', ['browser'])));
        }

        $repo = $this->em->getRepository(LogEntry::class);
        $logs = $repo->getLogEntries($entity);

        return HistoryListVM::mapCollection($logs);

    }

    /**
     * Reverts the entity to the given version number
     *
     * @param int $id
     * @param int $version
     * @return bool|null
     */
    public function revert(int $id, int $version): ?bool
    {
        $entity = $this->get($id, Role::class);
        if(!$entity) {
            $this->notifier->send((new Notification('Unable to revert record', ['browser'])));
        }

        $repo = $this->em->getRepository(LogEntry::class);
        $repo->revert($entity, $version);
        $this->em->persist($entity);
        $this->em->flush();

        return true;
    }

}