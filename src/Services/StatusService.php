<?php

namespace App\Services;

use App\Entity\ProgressPoint;
use App\Entity\Status;
use App\ViewModels\Status\StatusVM;
use App\ViewModels\Status\StatusListVM;
use Utils\Crud;
use Utils\ViewModel;
use Doctrine\ORM\EntityManagerInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Utils\History;
use Utils\SoftDelete;

/**
 * Class StatusService
 *
 * Provides CRUD operations and logic for managing Stati
 * @package Services
 * @author kells
 */
class StatusService
{
    use Crud;
    use History;
    use SoftDelete;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * StatusService constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     *  Fetches a list of Stati in a list view friendly view model
     * If a page number is provided the results will be paginated otherwise all results will be returned
     *
     * @param int $page The page number if paginating results
     * @param int $perPage The number of items per page
     * @return StatusListVM[]
     */
    public function fetchAll(int $page = 0, int $perPage = 15): array
    {
        if ($page === 0) {
            $page = null;
            $perPage = null;
        }
        $results = $this->em->getRepository(Status::class)->findBy([], null, $perPage, $page);

        return StatusListVM::mapCollection($results);
    }

    /**
     * @return array
     */
    public function fetchProgressPoints(): array
    {
        $results = $this->em->getRepository(ProgressPoint::class)->fetchAllAsArray();

        return array_column($results, 'description');
    }

    /**
     * Adds the Status to the database
     * The data is mapped to the entity from the provided view model
     *
     * @param ViewModel $vm The view model to map the data from
     * @return int Returns the id of the inserted entity
     * @throws UnregisteredMappingException
     */
    public function addStatus(ViewModel $vm): int
    {
        $entity = $this->create($vm, Status::class);
        return $entity->getId();
    }

    /**
     * Updates the Status in the database
     * The data is updated using the given View Model
     *
     * @param ViewModel $vm The view model to map the data from
     * @return bool Returns true
     * @throws UnregisteredMappingException
     */
    public function updateStatus(ViewModel $vm): bool
    {
        $this->fetchAndUpdate($vm, Status::class);
        return true;
    }

    /**
     * Delete a Status
     * The Status with the given Id will be deleted
     *
     * @param int $id The id of the entity to delete
     * @return bool Returns true
     */
    public function deleteStatus(int $id): bool
    {
        return $this->delete($id, Status::class);
    }
}