<?php

namespace App\Services;

use App\Entity\ProgressPoint;
use App\Entity\Status;
use App\Entity\Ticket;
use App\Entity\User;
use App\ViewModels\Ticket\TicketVM;
use App\ViewModels\Ticket\TicketListVM;
use Utils\Crud;
use Utils\ViewModel;
use Doctrine\ORM\EntityManagerInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Utils\History;
use Utils\SoftDelete;

/**
 * Class TicketService
 *
 * Provides CRUD operations and logic for managing Tickets
 * @package Services
 * @author kells
 */
class TicketService
{
    use Crud;
    use History;
    use SoftDelete;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * TicketService constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    
    /**
     *  Fetches a list of Tickets in a list view friendly view model
     * If a page number is provided the results will be paginated otherwise all results will be returned
     *
     * @param int $page The page number if paginating results
     * @param int $perPage The number of items per page
     * @return TicketListVM[]
     */
    public function fetchAll(int$page = 0, int $perPage = 15): array
    {
        if($page  === 0) {
            $page = null;
            $perPage = null;
        }
        $results = $this->em->getRepository(Ticket::class)->findBy([], null, $perPage, $page);

        return TicketListVM::mapCollection($results);
    }

    /**
     *  Fetches a list of overdue Tickets in a list view friendly view model
     * @return TicketListVM[]
     */
    public function fetchOverdue(): array
    {
        $results = $this->em->getRepository(Ticket::class)->fetchOverdue();

        return TicketListVM::mapCollection($results);
    }

    /**
     *  Fetches a list of Tickets assigned to the given user in a list view friendly view model
     * @param User $user
     * @return TicketListVM[]
     */
    public function fetchUserTickets(User $user): array
    {
        $results = $this->em->getRepository(Ticket::class)->fetchByAssignee($user);

        return TicketListVM::mapCollection($results);
    }

    /**
     * Fetches a list of statuses
     *
     * @return array
     */
    public function fetchStatuses(): array
    {
        $results = $this->em->getRepository(Status::class)->fetchAsArray();

        return array_column($results, 'description');
    }

    /**
     * Fetches a list of progress points
     *
     * @return array
     */
    public function fetchProgressPoints(): array
    {
        $results = $this->em->getRepository(ProgressPoint::class)->fetchAllAsArray();

        return array_column($results, 'description');
    }
    
    /**
     * Adds the Ticket to the database
     * The data is mapped to the entity from the provided view model
     *
     * @param ViewModel $vm The view model to map the data from
     * @return int Returns the id of the inserted entity
     * @throws UnregisteredMappingException
     */
    public function addTicket(ViewModel $vm): int
    {
        $entity = $this->create($vm, Ticket::class);
        return $entity->getId();
    }

    /**
     * Updates the Ticket in the database
     * The data is updated using the given View Model
     *
     * @param ViewModel $vm The view model to map the data from
     * @return bool Returns true
     * @throws UnregisteredMappingException
     */
    public function updateTicket(ViewModel $vm): bool
    {
        $this->fetchAndUpdate($vm, Ticket::class);
        return true;
    }

    /**
     * Delete a Ticket
     * The Ticket with the given Id will be deleted
     *
     * @param int $id The id of the entity to delete
     * @return bool Returns true
     */
    public function deleteTicket(int $id): bool
    {
        return $this->delete($id, Ticket::class);
    }
}