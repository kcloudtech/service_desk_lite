<?php

namespace App\Services;

use App\Entity\Admin\Translation;
use App\ViewModels\Translation\TranslationVM;
use App\ViewModels\Translation\TranslationListVM;
use Utils\Crud;
use Utils\SoftDelete;
use Utils\History;
use Utils\ViewModel;
use Doctrine\ORM\EntityManagerInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;

/**
 * Class TranslationService
 *
 * Provides CRUD operations and logic for managing Translations
 * @package Services
 */
class TranslationService
{
    use Crud;
    use SoftDelete;
    use History;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * TranslationService constructor.
     * @param EntityManagerInterface $emMaster
     */
    public function __construct(EntityManagerInterface $emMaster)
    {
        $this->em = $emMaster;
    }
    
    /**
     *  Fetches a list of Translations in a list view friendly view model
     * If a page number is provided the results will be paginated otherwise all results will be returned
     *
     * @param int $page The page number if paginating results
     * @param int $perPage The number of items per page
     * @return TranslationVM[]
     */
    public function fetchAll(int$page = 0, int $perPage = 15): array
    {
        if($page  === 0) {
            $page = null;
            $perPage = null;
        }
        $results = $this->em->getRepository(Translation::class)->findBy([], null, $perPage, $page);

        return TranslationListVM::mapCollection($results);
    }
    
    /**
     * Adds the Translation to the database
     * The data is mapped to the entity from the provided view model
     *
     * @param ViewModel $vm The view model to map the data from
     * @return int Returns the id of the inserted entity
     * @throws UnregisteredMappingException
     */
    public function addTranslation(ViewModel $vm): int
    {
        $entity = $this->create($vm, Translation::class);
        return $entity->getId();
    }

    /**
     * Updates the Translation in the database
     * The data is updated using the given View Model
     *
     * @param ViewModel $vm The view model to map the data from
     * @return bool Returns true
     * @throws UnregisteredMappingException
     */
    public function updateTranslation(ViewModel $vm): bool
    {
        $this->fetchAndUpdate($vm, Translation::class);
        return true;
    }

    /**
     * Delete a Translation
     * The Translation with the given Id will be deleted
     *
     * @param int $id The id of the entity to delete
     * @return bool Returns true
     */
    public function deleteTranslation(int $id): bool
    {
        return $this->delete($id, Translation::class);
    }

    /**
     * Gets a list of the available locals
     *
     * @return array
     */
    public function getAvailableLocales(): array
    {
        $results = $this->em->getRepository(Translation::class)->findLocales();

       return array_column($results, 'locale');
    }


}