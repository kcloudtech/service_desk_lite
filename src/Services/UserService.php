<?php

namespace App\Services;

use App\Entity\User;
use App\ViewModels\User\RegistrationVM;
use Exception;
use Utils\Crud;
use Utils\HashidHelper;
use Utils\History;
use Utils\SoftDelete;
use Utils\ViewModel;
use App\ViewModels\User\ChangePasswordVM;
use App\ViewModels\User\ResetPasswordVM;
use App\ViewModels\User\UserListVM;
use App\ViewModels\User\UserVM;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class UserService
 *
 * Provides CRUD operations and logic for managing users
 * @author Kellsey
 * @package Services
 */
class UserService
{
    use Crud;
    use History;
    use SoftDelete;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var MailerInterface
     */
    private $mailer;

    /**
     * UserService constructor.
     *
     * @param EntityManagerInterface $em
     * @param TranslatorInterface $translator
     * @param MailerInterface $mailer
     */
    public function __construct(EntityManagerInterface $em, TranslatorInterface $translator, MailerInterface $mailer)
    {
        $this->em = $em;
        $this->translator = $translator;
        $this->mailer = $mailer;
    }

    /**
     * Registers a new user using the data in the provided View Model
     *
     * The users profile picture will be automatically uploaded to the server based on default path
     *
     * @see https://symfony.com/doc/master/bundles/StofDoctrineExtensionsBundle/configuration.html#configure-the-entity-managers
     *
     * @param ViewModel $vm The view model to map the data from
     * @return int Returns the id of the inserted entity
     * @throws UnregisteredMappingException
     * @throws TransportExceptionInterface
     * @throws Exception
     */
    public function registerUser(ViewModel $vm): int
    {
        $user = new User();
        $sendReset = true;

        if(!$vm instanceof RegistrationVM) {
            //Set a temp password
            $user->setPassword(random_int(100,500));
        } else {
            $sendReset = false;
        }

        /** @var User $user */
        $user = $this->create($vm, $user);

        if(!$user->getIsActive()) {
            $sendReset = false;
        }

        if($sendReset) {
            $this->generateResetToken($user->getEmail(), false);
        } else {
            //Send welcome email
            $email = (new TemplatedEmail())
                ->from(new Address('info@24marketing.co.uk',$this->translator->trans('App Brand')))
                ->to(new Address($user->getEmail()))
                ->subject(ucwords(( $this->translator->trans('welcome to').' '.$this->translator->trans('App Brand'))))
                ->htmlTemplate( 'emails/welcome.html.twig')
                ->context([
                    'name' => $user->getFirstName()
                ]);

            $this->mailer->send($email);
        }

        return $user->getId();
    }

    /**
     * Finds a user by the email address
     *
     * @param string $email The email to search for
     * @return UserVM | null
     */
    public function findUserByEmail(string $email):? UserVM
    {
        $this->em->getFilters()->disable('softdeleteable');
        $user =  (new UserVM())->mapFrom($this->em->getRepository(User::class)->findOneBy(['email' => $email]));
        $this->em->getFilters()->enable('softdeleteable');
        return $user;
    }

    /**
     * Fetches a list of active and inactive users in a list view friendly view model
     *
     * If a page number is provided the results will be paginated otherwise all results will be returned. Results are ordered by name
     *
     * @return UserListVM[]
     */
    public function fetchAll(): array
    {
        $users = $this->em->getRepository(User::class)->findBy([], ['firstName'=> 'ASC', 'lastName'=> 'ASC']);

        return UserListVM::mapCollection($users);
    }

    /**
     * Updates the user in the database
     *
     * The data is updated using the given View Model
     *
     * @param UserVM $vm The view model to map the data from
     * @return bool Returns true
     * @throws UnregisteredMappingException
     * @throws TransportExceptionInterface
     */
    public function updateUser(UserVM $vm): bool
    {
        /** @var User $user */
        $user = $this->get($vm->getId(), User::class);
        $this->update($vm, $user);

        if($user->getLastLogin() === null && $user->getToken() === null) {
            $this->generateResetToken($user->getEmail(), false);
        }

        return true;
    }

    /**
     * Changes the users password
     *
     * The data is updated using the given View Model
     *
     * @param ChangePasswordVM $vm The view model to map the data from
     * @return bool Returns true
     */
    public function changePassword(ChangePasswordVM $vm): bool
    {
        /** @var User $user */
        $user = $this->get($vm->getId(), User::class);
        $user->setPassword($vm->getNewPassword());

        $this->em->persist($user);
        $this->em->flush();

        return true;
    }

    /**
     * Resets the users password
     *
     * The data is updated using the given View Model
     *
     * @param ResetPasswordVM $vm The view model to map the data from
     * @param string $token The hashid token emailed to the user
     * @return bool Returns true
     */
    public function resetPassword(ResetPasswordVM $vm, string $token): bool
    {
        /** @var User $user */
        $user = $this->findUserByResetToken($token);
        $user->setPassword($vm->getNewPassword());

        $this->em->persist($user);
        $this->em->flush();

        return true;
    }

    /**
     * Generates a temporary token and sends the reset password email to the user
     * Hasid is used to create a url friendly token
     * @see https://github.com/vinkla/hashids
     *
     * @todo Generate the certificate and encrypt the sent email
     *
     * @param string $email The email address provided by the user
     * @param bool $isReset
     * @throws TransportExceptionInterface
     * @throws Exception
     * @noinspection CallableParameterUseCaseInTypeContextInspection
     */
    public function generateResetToken(string $email, bool $isReset): void
    {
        /** @var User $user */
        $user = $this->em->getRepository(User::class)->findOneBy(['isActive' => true, 'email' => $email]);
        if($user) {
            $token =  random_int(100,500). $user->getId();
            $user->setToken($token)->setTokenExpires(date_create('+1 days'));
            $this->em->persist($user);
            $this->em->flush();

            $subject = $isReset ?  $this->translator->trans('Password Reset') : ( $this->translator->trans('welcome to').' '.$this->translator->trans('App Brand'));


            $email = (new TemplatedEmail())
                ->from(new Address('info@24marketing.co.uk',$this->translator->trans('App Brand')))
                ->to(new Address($email))
                ->subject(ucwords($subject))
                ->htmlTemplate($isReset ? 'emails/reset_password.html.twig' : 'emails/new_account.html.twig')
                ->context([
                    'token' => HashidHelper::encode($token),
                    'name' => $user->getFirstName()
                ]);

            $this->mailer->send($email);
        }
    }

    /**
     * Validates the reset password token
     *
     * @param string $token The hashid token emailed to the user
     * @return bool
     */
    public function validateToken(string $token): bool
    {
        /** @var User $user */
        $user = $this->findUserByResetToken($token);

        return $user && $user->getTokenExpires() > date_create();
    }

    /**
     * Finds an active user record by the reset token
     *
     * @param string $token The hashid token emailed to the user
     * @return User|null
     */
    private function findUserByResetToken(string $token): ?User
    {
        $token = HashidHelper::decode($token)[0];

        /** @var User $user */
        return $this->em->getRepository(User::class)->findOneBy(['isActive' => true, 'token' => $token]);
    }
}