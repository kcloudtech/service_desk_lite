<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Utils\HashidHelper;

class HashidExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('hashid_encode', [$this, 'encode']),
        ];
    }

    public function encode($value): string
    {
        if(!is_int($value)) { return $value; }
        return HashidHelper::encode($value);
    }
}
