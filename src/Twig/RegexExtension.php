<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class RegexExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('regexReplace', [$this, 'regexReplace']),
        ];
    }

    public function regexReplace($value, string $pattern, string $replacement)
    {
        $value =  preg_replace($pattern, $replacement, (string)$value);

        return $value;
    }
}