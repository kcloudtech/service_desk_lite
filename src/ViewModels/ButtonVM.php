<?php

namespace App\ViewModels;

use Utils\ViewModel;

/**
 * Class ButtonVM
 * Used to maintain consistent formatting for buttons used with scaffolded templates
 * @package View Model
 */
class ButtonVM extends ViewModel
{

    /**
     * @var string
     */
    private $route;

    /**
     * @var string
     */
    private $icon = '';

    /**
     * @var array
     */
    private $params = [];

    /**
     * @var string
     */
    private $colour = '';

    /**
     * @var string
     */
    private $title = '';

    /**
     * ButtonVM constructor.
     * @param array $options
     *    $options = [
     *      'route' => (string) name of the route to generate
     *      'params' => (array) params to be used in the route generation
     *      'title' => (string) title to show on the button
     *      'colour' => (string) colour class to apply to the button
     *      'icon' => (string | null) the font awesome icon to use without the fa fa- prefix
     *    ]
     */
    public function __construct(array $options = [])
    {
        parent::__construct($options);
    }

    /**
     * @return string
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @param string $route
     * @return ButtonVM
     */
    public function setRoute($route): ButtonVM
    {
        $this->route = $route;
        return $this;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     * @return ButtonVM
     */
    public function setIcon($icon): ButtonVM
    {
        $this->icon = $icon;
        return $this;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param array $params
     * @return ButtonVM
     */
    public function setParams($params): ButtonVM
    {
        $this->params = $params;
        return $this;
    }

    /**
     * @return string
     */
    public function getColour(): string
    {
        return $this->colour;
    }

    /**
     * @param string $colour
     * @return ButtonVM
     */
    public function setColour(string $colour): ButtonVM
    {
        $this->colour = $colour;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return ButtonVM
     */
    public function setTitle(string $title): ButtonVM
    {
        $this->title = $title;
        return $this;
    }

}