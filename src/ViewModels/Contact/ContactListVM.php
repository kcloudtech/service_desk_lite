<?php

namespace App\ViewModels\Contact;

use App\Entity\Customer;
use Utils\ViewModel;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class ContactListVM
 * @package ViewModel
 * @author kells
 */
class ContactListVM extends ViewModel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var Customer
     * @Serializer\Groups({"list"})
     * @Serializer\Accessor(getter="getCustomer")
     */
    private $customer;

    /**
     * @var string
     * @Serializer\Groups({"list"})
     * @Serializer\Accessor(getter="getFirstName")
     */
    private $firstName;

    /**
     * @var string
     * @Serializer\Groups({"list"})
     * @Serializer\Accessor(getter="getLastName")
     */
    private $lastName;

    /**
     * @var string
     * @Serializer\Groups({"list"})
     * @Serializer\Accessor(getter="getRole")
     */
    private $role;

    /**
     * @var string | null
     * @Serializer\Groups({"list"})
     */
    private $landline;

    /**
     * @var string | null
     * @Serializer\Groups({"list"})
     */
    private $mobile;

    /**
     * @var string | null
     * @Serializer\Groups({"list"})
     */
    private $email;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string | null
     */
    public function getCustomer(): ?string
    {
        return $this->customer;
    }

    /**
     * @param Customer | null $customer
     * @return $this
     */
    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return ucwords($this->firstName);
    }

    /**
     * @param string $firstName
     * @return $this
     */
    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return ucwords($this->lastName);
    }

    /**
     * @param string $lastName
     * @return $this
     */
    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getRole(): string
    {
        return ucwords($this->role);
    }

    /**
     * @param string $role
     * @return $this
     */
    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }

    /**
     * @return string | null
     */
    public function getLandline(): ?string
    {
        return $this->landline;
    }

    /**
     * @param string | null $landline
     * @return $this
     */
    public function setLandline(?string $landline): self
    {
        $this->landline = $landline;

        return $this;
    }

    /**
     * @return string | null
     */
    public function getMobile(): ?string
    {
        return $this->mobile;
    }

    /**
     * @param string | null $mobile
     * @return $this
     */
    public function setMobile(?string $mobile): self
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * @return string | null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string | null $email
     * @return $this
     */
    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }
}
