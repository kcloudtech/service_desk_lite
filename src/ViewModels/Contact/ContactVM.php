<?php

namespace App\ViewModels\Contact;

use App\Entity\Customer;
use Utils\Constraint\CssClass;
use Utils\Constraint\FieldType;
use Utils\ViewModel;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class ContactVM
 * @package ViewModel
 * @author kells
 */
class ContactVM extends ViewModel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var Customer
     * @Serializer\Groups({"view"})
     * @FieldType(type="Entity", entity="Customer")
     * @Assert\NotBlank(message="{{ label }} is required")
     */
    private $customer;

    /**
     * @var string
     * @Serializer\Groups({"view"})
     * @Assert\Length(max="64")
     * @Assert\NotBlank(message="{{ label }} is required")
     */
    private $firstName;

    /**
     * @var string
     * @Serializer\Groups({"view"})
     * @Assert\Length(max="64")
     * @Assert\NotBlank(message="{{ label }} is required")
     */
    private $lastName;

    /**
     * @var string
     * @Serializer\Groups({"view"})
     * @Assert\Length(max="64")
     * @Assert\NotBlank(message="{{ label }} is required")
     */
    private $role;

    /**
     * @var string | null
     * @Serializer\Groups({"view"})
     * @Assert\Length(max="12")
     * @CssClass(class="input-phone")
     */
    private $landline;

    /**
     * @var string | null
     * @Serializer\Groups({"view"})
     * @Assert\Length(max="12")
     * @CssClass(class="input-phone")
     */
    private $mobile;

    /**
     * @var string | null
     * @Serializer\Groups({"view"})
     * @Assert\Length(max="128")
     * @Assert\Email()
     * @FieldType(type="Email")
     */
    private $email;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return $this
     */
    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return Customer | null
     */
    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    /**
     * @param Customer | null $customer
     * @return $this
     */
    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     * @return $this
     */
    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     * @return $this
     */
    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getRole(): ?string
    {
        return $this->role;
    }

    /**
     * @param string|null $role
     * @return $this
     */
    public function setRole(?string $role): self
    {
        $this->role = $role;

        return $this;
    }

    /**
     * @return string | null
     */
    public function getLandline(): ?string
    {
        return $this->landline;
    }

    /**
     * @param string | null $landline
     * @return $this
     */
    public function setLandline(?string $landline): self
    {
        $this->landline = $landline;

        return $this;
    }

    /**
     * @return string | null
     */
    public function getMobile(): ?string
    {
        return $this->mobile;
    }

    /**
     * @param string | null $mobile
     * @return $this
     */
    public function setMobile(?string $mobile): self
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * @return string | null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string | null $email
     * @return $this
     */
    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }
}
