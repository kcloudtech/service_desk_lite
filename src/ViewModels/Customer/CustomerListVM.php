<?php

namespace App\ViewModels\Customer;

use Doctrine\Common\Collections\Collection;
use Utils\HashidHelper;
use Utils\UrlVM;
use Utils\ViewModel;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class CustomerListVM
 * @package ViewModel
 * @author kells
 */
class CustomerListVM extends ViewModel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     * @Serializer\Groups({"list"})
     * @Serializer\Type("DateTime<'d/m/Y'>")
     */
    private $created;

    /**
     * @var string
     * @Serializer\Groups({"list"})
     * @Serializer\Accessor(getter="getName")
     */
    private $name;

    /**
     * @var bool
     * @Serializer\Groups({"list"})
     */
    private $active;

    /**
     * @var Collection
     * @Serializer\Groups({"list"})
     * @Serializer\Accessor(getter="getContacts")
     */
    private $contacts = [];

    /**
     * @var Collection
     * @Serializer\Groups({"list"})
     * @Serializer\Accessor(getter="getTickets")
     */
    private $tickets = [];

    /**
     * @var \DateTime | null
     * @Serializer\Groups({"list"})
     * @Serializer\Type("DateTime<'d/m/Y'>")
     */
    private $updated;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     * @return $this
     */
    public function setCreated(\DateTime $created): self
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return array
     */
    public function getName(): array
    {
        return (new UrlVM())
            ->setLabel(ucwords($this->name))
            ->setRoute('view_customer')
            ->setParams(['hashid' => HashidHelper::encode($this->getId())])
            ->toArray();
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return bool
     */
    public function getActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return $this
     */
    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return string | null
     */
    public function getContacts(): ?int
    {
        return count($this->contacts);
    }

    /**
     * @param string | null $contacts
     * @return $this
     */
    public function setContacts(?string $contacts): self
    {
        $this->contacts = $contacts;

        return $this;
    }

    /**
     * @return int | null
     */
    public function getTickets(): ?int
    {
        return count($this->tickets);
    }

    /**
     * @param string | null $tickets
     * @return $this
     */
    public function setTickets(?string $tickets): self
    {
        $this->tickets = $tickets;

        return $this;
    }

    /**
     * @return \DateTime | null
     */
    public function getUpdated(): ?\DateTime
    {
        return $this->updated;
    }

    /**
     * @param \DateTime | null $updated
     * @return $this
     */
    public function setUpdated(?\DateTime $updated): self
    {
        $this->updated = $updated;

        return $this;
    }
}
