<?php

namespace App\ViewModels\Customer;

use App\Entity\Contact;
use App\Entity\Embedded\Address;
use App\Entity\Ticket;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;
use Utils\Constraint\CssClass;
use Utils\Constraint\FieldType;
use Utils\ViewModel;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class CustomerVM
 * @package ViewModel
 * @author kells
 */
class CustomerVM extends ViewModel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var Address | null
     */
    protected $address;

    /**
     * @var string
     * @Serializer\Groups({"view"})
     * @Assert\Length(max="128")
     * @Assert\NotBlank(message="{{ label }} is required")
     * @CssClass(class="text-capitalize")
     */
    private $name;

    /**
     * @var string | null
     * @Serializer\Groups({"view"})
     * @Assert\Length(max="128")
     * @Assert\NotBlank(message="{{ label }} is required")
     * @CssClass(class="text-capitalize")
     */
    private $address1;

    /**
     * @var string | null
     * @Serializer\Groups({"view"})
     * @Assert\Length(max="128")
     * @CssClass(class="text-capitalize")
     */
    private $address2;

    /**
     * @var string | null
     * @Serializer\Groups({"view"})
     * @Assert\Length(max="64")
     * @Assert\NotBlank(message="{{ label }} is required")
     * @CssClass(class="text-capitalize")
     */
    private $townOrCity;

    /**
     * @var string | null
     * @Serializer\Groups({"view"})
     * @Assert\Length(max="64")
     * @Assert\NotBlank(message="{{ label }} is required")
     * @FieldType(type="AutoComplete")
     * @CssClass(class="text-capitalize")
     */
    private $county;

    /**
     * @var string | null
     * @Serializer\Groups({"view"})
     * @Assert\Length(max="9")
     * @Assert\NotBlank(message="{{ label }} is required")
     * @CssClass(class="text-uppercase input-postcode")
     */
    private $postcode;

    /**
     * @var bool
     * @Serializer\Groups({"view"})
     */
    private $active = false;

    /**
     * @var Ticket[] | Collection
     */
    private $tickets;

    /**
     * @var Contact[] | Collection
     */
    private $contacts;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return Address|null
     */
    public function getAddress(): ?Address
    {
        return $this->address;
    }

    /**
     * @param Address|null $address
     * @return CustomerVM
     */
    public function setAddress(?Address $address): self
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress1(): ?string
    {
        return $this->address1;
    }

    /**
     * @param string|null $address1
     * @return CustomerVM
     */
    public function setAddress1(?string $address1): self
    {
        $this->address1 = $address1;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress2(): ?string
    {
        return $this->address2;
    }

    /**
     * @param string|null $address2
     * @return CustomerVM
     */
    public function setAddress2(?string $address2): self
    {
        $this->address2 = $address2;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTownOrCity(): ?string
    {
        return $this->townOrCity;
    }

    /**
     * @param string|null $townOrCity
     * @return CustomerVM
     */
    public function setTownOrCity(?string $townOrCity): self
    {
        $this->townOrCity = $townOrCity;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCounty(): ?string
    {
        return $this->county;
    }

    /**
     * @param string|null $county
     * @return CustomerVM
     */
    public function setCounty(?string $county): self
    {
        $this->county = $county;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPostcode(): ?string
    {
        return $this->postcode;
    }

    /**
     * @param string|null $postcode
     * @return CustomerVM
     */
    public function setPostcode(?string $postcode): self
    {
        $this->postcode = $postcode;
        return $this;
    }

    /**
     * @return bool
     */
    public function getActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return $this
     */
    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return Ticket[]|Collection
     */
    public function getTickets()
    {
        return $this->tickets;
    }

    /**
     * @param Ticket[]|Collection $tickets
     * @return CustomerVM
     */
    public function setTickets($tickets)
    {
        $this->tickets = $tickets;
        return $this;
    }

    /**
     * @return Contact[]|Collection
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * @param Contact[]|Collection $contacts
     * @return CustomerVM
     */
    public function setContacts($contacts)
    {
        $this->contacts = $contacts;
        return $this;
    }


}
