<?php

namespace App\ViewModels;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Utils\ViewModel;

class HistoryListVM extends ViewModel
{
    /**
     * @var int
     *
     */
    private $objectId;

    /**
     * @var string
     * @Serializer\Groups(groups={"list"})
     */
    private $action;

    /**
     * @var int
     * @Serializer\Groups(groups={"list"})
     */
    private $version;

    /**
     * @var array
     * @Serializer\Groups(groups={"list"})
     */
    private $data;

    /**
     * @var string
     * @Serializer\Groups(groups={"list"})
     */
    private $username = 'System';

    /**
     * @var \DateTime
     * @Serializer\Groups(groups={"list"})
     * @Type("DateTime<'D d M Y'>")
     */
    private $loggedAt;

    /**
     * @return int
     */
    public function getObjectId()
    {
        return $this->objectId;
    }

    /**
     * @param int $objectId
     * @return HistoryListVM
     */
    public function setObjectId($objectId): HistoryListVM
    {
        $this->objectId = $objectId;
        return $this;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     * @return HistoryListVM
     */
    public function setAction($action): HistoryListVM
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLoggedAt()
    {
        return $this->loggedAt;
    }

    /**
     * @param \DateTime $loggedAt
     * @return HistoryListVM
     */
    public function setLoggedAt($loggedAt): HistoryListVM
    {
        $this->loggedAt = $loggedAt;
        return $this;
    }

    /**
     * @return int
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param int $version
     * @return HistoryListVM
     */
    public function setVersion($version): HistoryListVM
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return HistoryListVM
     */
    public function setData($data): HistoryListVM
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return HistoryListVM
     */
    public function setUsername($username): HistoryListVM
    {
        $this->username = $username;
        return $this;
    }

}