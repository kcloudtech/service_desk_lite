<?php

namespace App\ViewModels\IssueType;

use Symfony\Component\HttpFoundation\File\File;
use Utils\ViewModel;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class IssueTypeListVM
 * @package ViewModel
 * @author kells
 */
class IssueTypeListVM extends ViewModel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     * @Serializer\Groups({"list"})
     * @Serializer\Accessor(getter="getDescription")
     */
    private $description;

    /**
     * @var string
     * @Serializer\Groups({"list"})
     * @Serializer\Accessor(getter="getIcon")
     */
    private $icon;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return ucwords($this->description);
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getIcon(): string
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     * @return $this
     */
    public function setIcon(string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }
}
