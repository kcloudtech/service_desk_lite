<?php

namespace App\ViewModels\IssueType;

use Symfony\Component\HttpFoundation\File\File;
use Utils\ViewModel;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class IssueTypeVM
 * @package ViewModel
 * @author kells
 */
class IssueTypeVM extends ViewModel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     * @Serializer\Groups({ "view"})
     * @Assert\Length(max="64")
     * @Assert\NotBlank(message="{{ label }} is required")
     */
    private $description;

    /**
     * @var string
     * @Serializer\Groups({ "view"})
     * @Assert\Length(max="32")
     * @Assert\NotBlank(message="{{ label }} is required")
     */
    private $icon;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getIcon(): string
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     * @return $this
     */
    public function setIcon(string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }
}
