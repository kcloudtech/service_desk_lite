<?php

namespace App\ViewModels\Permission;

use App\Entity\Role;
use JMS\Serializer\Annotation\Groups;
use Utils\ViewModel;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class PermissionListVM
 * @package ViewModel
 */
class PermissionListVM extends ViewModel
{
    /**
     * @var int
     * @Assert\NotBlank(message="Please provide the id")
     * @Groups({"api"})
     */
    private $id;

    private $role;

    /**
     * @var string
     * @Assert\Length(max="64", maxMessage="The page cannot be longer than {{ limit }} characters long")
     * @Assert\NotBlank(message="Please provide the page")
     * @Groups({"api", "list"})
     */
    private $page;

    /**
     * @var bool
     * @Assert\NotBlank(message="Please provide the create")
     * @Groups({"api", "list"})
     */
    private $create;

    /**
     * @var bool
     * @Assert\NotBlank(message="Please provide the read")
     * @Groups({"api", "list"})
     */
    private $read;

    /**
     * @var bool
     * @Assert\NotBlank(message="Please provide the update")
     * @Groups({"api", "list"})
     */
    private $update;

    /**
     * @var bool
     * @Assert\NotBlank(message="Please provide the delete")
     * @Groups({"api", "list"})
     */
    private $delete;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return Role | null
     */
    public function getRole(): ?Role
    {
        return $this->role;
    }

    /**
     * @param Role | null $role
     * @return $this
     */
    public function setRole(?Role $role): self
    {
        $this->role = $role;

        return $this;
    }

    /**
     * @return string
     */
    public function getPage(): string
    {
        return $this->page;
    }

    /**
     * @param string $page
     * @return $this
     */
    public function setPage(string $page): self
    {
        $this->page = $page;

        return $this;
    }

    /**
     * @return bool
     */
    public function getCreate(): bool
    {
        return $this->create;
    }

    /**
     * @param bool $create
     * @return $this
     */
    public function setCreate(bool $create): self
    {
        $this->create = $create;

        return $this;
    }

    /**
     * @return bool
     */
    public function getRead(): bool
    {
        return $this->read;
    }

    /**
     * @param bool $read
     * @return $this
     */
    public function setRead(bool $read): self
    {
        $this->read = $read;

        return $this;
    }

    /**
     * @return bool
     */
    public function getUpdate(): bool
    {
        return $this->update;
    }

    /**
     * @param bool $update
     * @return $this
     */
    public function setUpdate(bool $update): self
    {
        $this->update = $update;

        return $this;
    }

    /**
     * @return bool
     */
    public function getDelete(): bool
    {
        return $this->delete;
    }

    /**
     * @param bool $delete
     * @return $this
     */
    public function setDelete(bool $delete): self
    {
        $this->delete = $delete;

        return $this;
    }
}
