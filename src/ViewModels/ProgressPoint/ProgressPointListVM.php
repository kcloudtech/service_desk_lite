<?php

namespace App\ViewModels\ProgressPoint;

use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;
use Utils\ViewModel;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class ProgressPointListVM
 * @package ViewModel
 * @author kells
 */
class ProgressPointListVM extends ViewModel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     * @Serializer\Groups({"list"})
     * @Serializer\Accessor(getter="getDescription")
     */
    private $description;

    /**
     * @var int
     * @Serializer\Groups({"list"})
     */
    private $position;

    /**
     * @var Collection
     * @Serializer\Groups({"list"})
     * @Serializer\Accessor(getter="getStatuses")
     */
    private $statuses;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return ucwords($this->description);
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @param int $position
     * @return $this
     */
    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getStatuses(): int
    {
        return count($this->statuses);
    }

    /**
     * @param Collection $statuses
     */
    public function setStatuses(Collection $statuses): void
    {
        $this->statuses = $statuses;
    }


}
