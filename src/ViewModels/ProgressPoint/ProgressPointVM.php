<?php

namespace App\ViewModels\ProgressPoint;

use Symfony\Component\HttpFoundation\File\File;
use Utils\ViewModel;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class ProgressPointVM
 * @package ViewModel
 * @author kells
 */
class ProgressPointVM extends ViewModel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     * @Serializer\Groups({"view"})
     * @Assert\Length(max="32")
     * @Assert\NotBlank(message="{{ label }} is required")
     */
    private $description;

    /**
     * @var int
     * @Serializer\Groups({"view"})
     * @Assert\NotBlank(message="{{ label }} is required")
     * @Assert\Type(type="integer")
     * @Assert\PositiveOrZero()
     */
    private $position;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return $this
     */
    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return $this
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int|null $position
     * @return $this
     */
    public function setPosition($position): self
    {
        $this->position = $position;

        return $this;
    }
}
