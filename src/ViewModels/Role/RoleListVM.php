<?php

namespace App\ViewModels\Role;

use JMS\Serializer\Annotation as Serializer;
use Utils\ViewModel;

/**
 * Class RoleListVM
 * @package View Model
 *  @Serializer\AccessorOrder("custom", custom = {"name", "permissions", "users"})
 */
class RoleListVM extends ViewModel
{

    /**
     * @var int
     * @Serializer\Groups({"api"})
     */
    private $id;

    /**
     * @var string
     * @Serializer\Accessor(getter="getName")
     * @Serializer\Groups({"api", "list"})
     */
    private $name;

    /**
     * @var int
     * @Serializer\Accessor(getter="getPermissions")
     * @Serializer\Groups({"api", "list"})
     */
    private $permissions;

    /**
     * @var int
     * @Serializer\Accessor(getter="getUsers")
     * @Serializer\Groups({"api", "list"})
     */
    private $users;

    /**
     * @return string
     */
    public function getName()
    {
        return ucwords(strtolower(str_replace(['ROLE_','_'],['',' '], $this->name)));
    }

    /**
     * @param string $name
     * @return RoleListVM
     */
    public function setName($name): RoleListVM
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return RoleListVM
     */
    public function setId($id): RoleListVM
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getPermissions()
    {
        return count($this->permissions);
    }

    /**
     * @param int $permissions
     * @return RoleListVM
     */
    public function setPermissions($permissions): RoleListVM
    {
        $this->permissions = $permissions;
        return $this;
    }

    /**
     * @return int
     */
    public function getUsers()
    {
        return count($this->users);
    }

    /**
     * @param int $users
     * @return RoleListVM
     */
    public function setUsers($users): RoleListVM
    {
        $this->users = $users;
        return $this;
    }

    public function count($object)
    {
        return count($object);
    }
}