<?php

namespace App\ViewModels\Role;

use App\Entity\Admin\Account;
use Doctrine\ORM\PersistentCollection;
use JMS\Serializer\Annotation as Serializer;
use Utils\Constraint\CssClass;
use Utils\Constraint\FieldType;
use Utils\ViewModel;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class RoleVM
 * @package View Model
 */
class RoleVM extends ViewModel
{
    /**
     * @var int
     * @FieldType(type="Hidden")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Please enter a role name")
     * @Assert\Length(min="3", max="128",
     *     minMessage="The role name must be at least {{ limit }} characters long",
     *     maxMessage="The role name cannot be longer than {{ limit }} characters")
     * @CssClass(class="text-capitalize")
     * @Serializer\Groups({"api", "view"})
     */
    private $name;

    /**
     * @var Account
     */
    private $account;

    /**
     * @var array | PersistentCollection
     *
     * @FieldType(type="PermissionCollection", formType="Permission")
     */
    private $permissions;

    /**
     * @return int
     */
    public function getId():? int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return RoleVM
     */
    public function setId($id): RoleVM
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName():? string
    {
        return ucwords(strtolower(str_replace(['ROLE_','_'],['',' '], $this->name)));
    }

    /**
     * @param string $name
     * @return RoleVM
     */
    public function setName($name): RoleVM
    {
        $this->name = 'ROLE_'. str_replace(' ', '_', strtoupper($name));
        return $this;
    }

    /**
     * @return Account
     */
    public function getAccount(): Account
    {
        return $this->account;
    }

    /**
     * @param Account $account
     * @return RoleVM
     */
    public function setAccount(Account $account): self
    {
        $this->account = $account;
        return $this;
    }


    /**
     * @return PersistentCollection | array
     */
    public function getPermissions()
    {
        return $this->permissions;
    }

    /**
     * @param PersistentCollection | array $permissions
     * @return RoleVM
     */
    public function setPermissions($permissions): RoleVM
    {
        $this->permissions = $permissions;
        return $this;
    }
}