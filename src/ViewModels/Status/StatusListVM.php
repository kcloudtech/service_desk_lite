<?php

namespace App\ViewModels\Status;

use App\Entity\ProgressPoint;
use Symfony\Component\HttpFoundation\File\File;
use Utils\ViewModel;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class StatusListVM
 * @package ViewModel
 * @author kells
 */
class StatusListVM extends ViewModel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     * @Serializer\Groups({ "list"})
     * @Serializer\Accessor(getter="getDescription")
     */
    private $description;

    /**
     * @var ProgressPoint
     * @Serializer\Groups({ "list"})
     * @Serializer\Accessor(getter="getProgressPoint")
     */
    private $progressPoint;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return ucwords($this->description);
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string | null
     */
    public function getProgressPoint(): ?string
    {
        return $this->progressPoint;
    }

    /**
     * @param ProgressPoint|null $progressPoint
     * @return $this
     */
    public function setProgressPoint(?ProgressPoint $progressPoint): self
    {
        $this->progressPoint = $progressPoint;

        return $this;
    }
}
