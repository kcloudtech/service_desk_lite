<?php

namespace App\ViewModels\Status;

use App\Entity\ProgressPoint;
use Symfony\Component\HttpFoundation\File\File;
use Utils\Constraint\FieldType;
use Utils\ViewModel;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class StatusVM
 * @package ViewModel
 * @author kells
 */
class StatusVM extends ViewModel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var ProgressPoint
     * @Serializer\Groups({ "view"})
     * @FieldType(type="Entity", entity="ProgressPoint")
     * @Assert\NotBlank(message="{{ label }} is required")
     */
    private $progressPoint;

    /**
     * @var string
     * @Serializer\Groups({ "view"})
     * @Assert\Length(max="32")
     * @Assert\NotBlank(message="{{ label }} is required")
     */
    private $description;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return ProgressPoint | null
     */
    public function getProgressPoint(): ?ProgressPoint
    {
        return $this->progressPoint;
    }

    /**
     * @param ProgressPoint | null $progressPoint
     * @return $this
     */
    public function setProgressPoint(?ProgressPoint $progressPoint): self
    {
        $this->progressPoint = $progressPoint;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
