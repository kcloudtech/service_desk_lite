<?php

namespace App\ViewModels;

class TableFilter
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $icon;

    /**
     * @var string
     */
    private $title;

    /**
     * @var int
     */
    private $col;

    /**
     * @var  string
     */
    private $searchVal;

    /**
     * TableOption constructor.
     * @param string $id
     * @param string $icon
     * @param string $title
     * @param int $col
     * @param string $searchVal
     */
    public function __construct(string $id, string $icon, string $title, int $col, string $searchVal)
    {
        $this->id = $id;
        $this->icon = $icon;
        $this->title = $title;
        $this->col = $col;
        $this->searchVal = $searchVal;
    }


    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return TableFilter
     */
    public function setId($id): TableFilter
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     * @return TableFilter
     */
    public function setIcon($icon): TableFilter
    {
        $this->icon = $icon;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return TableFilter
     */
    public function setTitle($title): TableFilter
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return int
     */
    public function getCol()
    {
        return $this->col;
    }

    /**
     * @param int $col
     * @return TableFilter
     */
    public function setCol($col): TableFilter
    {
        $this->col = $col;
        return $this;
    }

    /**
     * @return string
     */
    public function getSearchVal()
    {
        return $this->searchVal;
    }

    /**
     * @param string $searchVal
     * @return TableFilter
     */
    public function setSearchVal($searchVal): TableFilter
    {
        $this->searchVal = $searchVal;
        return $this;
    }
}