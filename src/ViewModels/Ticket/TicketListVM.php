<?php

namespace App\ViewModels\Ticket;

use App\Entity\Customer;
use App\Entity\Status;
use App\Entity\User;
use Utils\HashidHelper;
use Utils\UrlVM;
use Utils\ViewModel;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class TicketListVM
 * @package ViewModel
 * @author kells
 */
class TicketListVM extends ViewModel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     * @Serializer\Groups({"list"})
     * @Serializer\Type("DateTime<'D d M H:i'>")
     */
    private $created;

    /**
     * @var Customer
     * @Serializer\Groups({"list"})
     * @Serializer\Accessor(getter="getCustomer")
     */
    private $customer;

    /**
     * @var User
     * @Serializer\Groups({"list"})
     * @Serializer\Accessor(getter="getAssignedTo")
     */
    private $assignedTo;

    /**
     * @var User
     * @Serializer\Groups({"list"})
     * @Serializer\Accessor(getter="getReporter")
     */
    private $reporter;

    /**
     * @var string
     * @Serializer\Groups({"list"})
     * @Serializer\Accessor(getter="getSubject")
     */
    private $subject;

    /**
     * @var string
     * @Serializer\Groups({"list"})
     */
    private $description;

    /**
     * @var int
     * @Serializer\Groups({"list"})
     */
    private $priority;

    /**
     * @var Status
     * @Serializer\Groups({"list"})
     * @Serializer\Accessor(getter="getStatus")
     */
    private $status;

    /**
     * @var \DateTime | null
     * @Serializer\Groups({"list"})
     * @Serializer\Type("DateTime<'d/m/Y H:i'>")
     */
    private $startBy;

    /**
     * @var \DateTime | null
     * @Serializer\Groups({"list"})
     * @Serializer\Type("DateTime<'d/m/Y H:i'>")
     */
    private $dueBy;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     * @return $this
     */
    public function setCreated(\DateTime $created): self
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return array | null
     */
    public function getCustomer(): ?array
    {
        return (new UrlVM())
            ->setLabel(ucwords($this->customer->getName()))
            ->setRoute('view_customer')
            ->setParams(['hashid' => HashidHelper::encode($this->customer->getId())])
            ->toArray();
    }

    /**
     * @param Customer | null $customer
     * @return $this
     */
    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return string | null
     */
    public function getAssignedTo(): ?string
    {
        return $this->assignedTo;
    }

    /**
     * @param User | null $assignedTo
     * @return $this
     */
    public function setAssignedTo(?User $assignedTo): self
    {
        $this->assignedTo = $assignedTo;

        return $this;
    }

    /**
     * @return string | null
     */
    public function getReporter(): ?string
    {
        return $this->reporter;
    }

    /**
     * @param User | null $reporter
     * @return $this
     */
    public function setReporter(?User $reporter): self
    {
        $this->reporter = $reporter;

        return $this;
    }

    /**
     * @return array
     */
    public function getSubject(): array
    {
        return (new UrlVM())
            ->setLabel(ucwords($this->subject))
            ->setRoute('view_ticket')
            ->setParams(['hashid' => HashidHelper::encode($this->getId())])
            ->toArray();
    }

    /**
     * @param string $subject
     * @return $this
     */
    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return int
     */
    public function getPriority(): int
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     * @return $this
     */
    public function setPriority(int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * @return string | null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param Status | null $status
     * @return $this
     */
    public function setStatus(?Status $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return \DateTime | null
     */
    public function getStartBy(): ?\DateTime
    {
        return $this->startBy;
    }

    /**
     * @param \DateTime | null $startBy
     * @return $this
     */
    public function setStartBy(?\DateTime $startBy): self
    {
        $this->startBy = $startBy;

        return $this;
    }

    /**
     * @return \DateTime | null
     */
    public function getDueBy(): ?\DateTime
    {
        return $this->dueBy;
    }

    /**
     * @param \DateTime | null $dueBy
     * @return $this
     */
    public function setDueBy(?\DateTime $dueBy): self
    {
        $this->dueBy = $dueBy;

        return $this;
    }
}
