<?php

namespace App\ViewModels\Ticket;

use App\Entity\IssueType;
use App\Entity\Status;
use App\Entity\User;
use App\Entity\Customer;
use Utils\Constraint\Data;
use Utils\Constraint\FieldType;
use Utils\ViewModel;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class TicketVM
 * @package ViewModel
 * @author kells
 */
class TicketVM extends ViewModel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var Customer
     * @Serializer\Groups({"view"})
     * @FieldType(type="Entity", entity="Customer")
     * @Assert\NotBlank(message="{{ label }} is required")
     */
    private $customer;

    /**
     * @var User
     * @Serializer\Groups({"view"})
     * @FieldType(type="Entity", entity="User")
     * @Assert\NotBlank(message="{{ label }} is required")
     */
    private $assignedTo;

    /**
     * @var User
     * @Serializer\Groups({"view"})
     * @FieldType(type="Entity", entity="User")
     * @Assert\NotBlank(message="{{ label }} is required")
     */
    private $reporter;

    /**
     * @var IssueType
     * @Serializer\Groups({"view"})
     * @FieldType(type="Entity", entity="IssueType")
     * @Assert\NotBlank(message="{{ label }} is required")
     */
    private $issueType;

    /**
     * @var string
     * @Serializer\Groups({"view"})
     * @Assert\Length(max="64")
     * @Assert\NotBlank(message="{{ label }} is required")
     */
    private $subject;

    /**
     * @var string
     * @Serializer\Groups({"view"})
     * @Assert\NotBlank(message="{{ label }} is required")
     * @FieldType(type="Textarea")
     */
    private $description;

    /**
     * @var int
     * @Serializer\Groups({"view"})
     * @Assert\NotBlank(message="{{ label }} is required")
     * @FieldType(type="Priority")
     * @Assert\Range(min="1", max="4")
     */
    private $priority;

    /**
     * @var Status
     * @Serializer\Groups({"view"})
     * @FieldType(type="Entity", entity="Status")
     * @Assert\NotBlank(message="{{ label }} is required")
     */
    private $status;

    /**
     * @var \DateTime | null
     * @Serializer\Groups({"view"})
     * @FieldType(type="Date")
     * @Data(attribute="date-format", value="YYYY-MM-DD")
     */
    private $startBy;

    /**
     * @var \DateTime | null
     * @Serializer\Groups({"view"})
     * @FieldType(type="Date")
     * @Data(attribute="date-format", value="YYYY-MM-DD")
     */
    private $dueBy;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return Customer | null
     */
    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    /**
     * @param Customer | null $customer
     * @return $this
     */
    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return IssueType
     */
    public function getIssueType(): ?IssueType
    {
        return $this->issueType;
    }

    /**
     * @param IssueType|null $issueType
     * @return TicketVM
     */
    public function setIssueType(?IssueType $issueType): self
    {
        $this->issueType = $issueType;
        return $this;
    }

    /**
     * @return User | null
     */
    public function getAssignedTo(): ?User
    {
        return $this->assignedTo;
    }

    /**
     * @param User | null $assignedTo
     * @return $this
     */
    public function setAssignedTo(?User $assignedTo): self
    {
        $this->assignedTo = $assignedTo;

        return $this;
    }

    /**
     * @return User | null
     */
    public function getReporter(): ?User
    {
        return $this->reporter;
    }

    /**
     * @param User | null $reporter
     * @return $this
     */
    public function setReporter(?User $reporter): self
    {
        $this->reporter = $reporter;

        return $this;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     * @return $this
     */
    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return int
     */
    public function getPriority(): int
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     * @return $this
     */
    public function setPriority(int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * @return Status | null
     */
    public function getStatus(): ?Status
    {
        return $this->status;
    }

    /**
     * @param Status | null $status
     * @return $this
     */
    public function setStatus(?Status $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return \DateTime | null
     */
    public function getStartBy(): ?\DateTime
    {
        return $this->startBy;
    }

    /**
     * @param \DateTime | null $startBy
     * @return $this
     */
    public function setStartBy(?\DateTime $startBy): self
    {
        $this->startBy = $startBy;

        return $this;
    }

    /**
     * @return \DateTime | null
     */
    public function getDueBy(): ?\DateTime
    {
        return $this->dueBy;
    }

    /**
     * @param \DateTime | null $dueBy
     * @return $this
     */
    public function setDueBy(?\DateTime $dueBy): self
    {
        $this->dueBy = $dueBy;

        return $this;
    }
}
