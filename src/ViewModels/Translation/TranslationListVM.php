<?php

namespace App\ViewModels\Translation;

use JMS\Serializer\Annotation\Groups;
use Utils\ViewModel;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class TranslationListVM
 * @package ViewModel
 */
class TranslationListVM extends ViewModel
{
    /**
     * @var int
     * @Assert\NotBlank(message="Please provide the id")
     * @Groups({"api"})
     */
    private $id;

    /**
     * @var string
     * @Assert\Length(max="64", maxMessage="The domain cannot be longer than {{ limit }} characters long")
     * @Assert\NotBlank(message="Please provide the domain")
     * @Groups({"api", "list"})
     */
    private $domain;

    /**
     * @var string
     * @Assert\Length(max="8", maxMessage="The locale cannot be longer than {{ limit }} characters long")
     * @Assert\NotBlank(message="Please provide the locale")
     * @Groups({"api", "list"})
     */
    private $locale;

    /**
     * @var string
     * @Assert\Length(max="128", maxMessage="The translation key cannot be longer than {{ limit }} characters long")
     * @Assert\NotBlank(message="Please provide the translation key")
     * @Groups({"api", "list"})
     */
    private $translationKey;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getDomain(): string
    {
        return $this->domain;
    }

    /**
     * @param string $domain
     * @return $this
     */
    public function setDomain(string $domain): self
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * @return string
     */
    public function getLocale(): string
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     * @return $this
     */
    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return string
     */
    public function getTranslationKey(): string
    {
        return $this->translationKey;
    }

    /**
     * @param string $translationKey
     * @return $this
     */
    public function setTranslationKey(string $translationKey): self
    {
        $this->translationKey = $translationKey;

        return $this;
    }

}
