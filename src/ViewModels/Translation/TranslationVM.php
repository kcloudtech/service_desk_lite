<?php

namespace App\ViewModels\Translation;

use Symfony\Component\Serializer\Annotation\Groups;
use Utils\ViewModel;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class TranslationVM
 * @package ViewModel
 */
class TranslationVM extends ViewModel
{
    /**
     * @var int
     * @Assert\NotBlank(message="Please provide the id")
     * @Groups({"api"})
     */
    private $id;

    /**
     * @var string
     * @Assert\Length(max="64", maxMessage="The domain cannot be longer than {{ limit }} characters long")
     * @Assert\NotBlank(message="Please provide the domain")
     * @Groups({"api"})
     */
    private $domain;

    /**
     * @var string
     * @Assert\Length(max="8", maxMessage="The locale cannot be longer than {{ limit }} characters long")
     * @Assert\NotBlank(message="Please provide the locale")
     * @Groups({"api"})
     */
    private $locale;

    /**
     * @var string
     * @Assert\Length(max="128", maxMessage="The translation key cannot be longer than {{ limit }} characters long")
     * @Assert\NotBlank(message="Please provide the translation key")
     * @Groups({"api"})
     */
    private $translationKey;

    /**
     * @var string
     * @Assert\NotBlank(message="Please provide the content")
     * @Groups({"api"})
     */
    private $content;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * @param string $domain
     * @return $this
     */
    public function setDomain(string $domain): self
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     * @return $this
     */
    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return string
     */
    public function getTranslationKey()
    {
        return $this->translationKey;
    }

    /**
     * @param string $translationKey
     * @return $this
     */
    public function setTranslationKey(string $translationKey): self
    {
        $this->translationKey = $translationKey;

        return $this;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return $this
     */
    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }
}
