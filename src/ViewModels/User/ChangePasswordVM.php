<?php

namespace App\ViewModels\User;

use JMS\Serializer\Annotation as Serializer;
use Utils\Constraint\FieldType;
use Utils\ViewModel;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints as Assert;
use Rollerworks\Component\PasswordStrength\Validator\Constraints as RollerworksPassword;

/**
 * Class ChangePasswordVM
 * @package View Model
 */
class ChangePasswordVM extends ViewModel
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     *
     * @UserPassword(message="Current password entered is not valid")
     * @FieldType(type="Password")
     * @Serializer\Groups(groups={"view"})
     */
    protected $currentPassword;

    /**
     * @var string
     *
     * @Assert\Length(min="6", minMessage="The password must be at least {{ limit }} characters long")
     * @RollerworksPassword\PasswordRequirements(requireLetters=true, requireNumbers=true, requireCaseDiff=true,
     *     missingNumbersMessage="The password must be alphanumeric",
     * missingLettersMessage="The password must be alphanumeric",
     * requireCaseDiffMessage="The password must contain at least 1 lower and upper case character")
     * @FieldType(type="Password")
     * @Serializer\Groups(groups={"view"})
     */
    protected $newPassword;

    /**
     * @var string
     * @Assert\Expression("value == this.getNewPassword()", message="The passwords do not match")
     * @FieldType(type="Password")
     * @Serializer\Groups(groups={"view"})
     */
    protected $confirmPassword;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ChangePasswordVM
     */
    public function setId($id): ChangePasswordVM
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrentPassword()
    {
        return $this->currentPassword;
    }

    /**
     * @param string $currentPassword
     * @return ChangePasswordVM
     */
    public function setCurrentPassword($currentPassword): ChangePasswordVM
    {
        $this->currentPassword = $currentPassword;
        return $this;
    }

    /**
     * @return string
     */
    public function getNewPassword()
    {
        return $this->newPassword;
    }

    /**
     * @param string $newPassword
     * @return ChangePasswordVM
     */
    public function setNewPassword($newPassword): ChangePasswordVM
    {
        $this->newPassword = $newPassword;
        return $this;
    }

    /**
     * @return string
     */
    public function getConfirmPassword()
    {
        return $this->confirmPassword;
    }

    /**
     * @param string $confirmPassword
     * @return ChangePasswordVM
     */
    public function setConfirmPassword($confirmPassword): ChangePasswordVM
    {
        $this->confirmPassword = $confirmPassword;
        return $this;
    }
}