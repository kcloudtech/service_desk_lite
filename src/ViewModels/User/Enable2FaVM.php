<?php

namespace App\ViewModels\User;

use JMS\Serializer\Annotation as Serializer;
use Utils\ViewModel;
use Symfony\Component\Validator\Constraints as Assert;

class Enable2FaVM extends ViewModel
{
    /**
     * @var string
     *
     * @Assert\NotBlank(message="Please enter your code in the authenticator app")
     * @Serializer\Groups(groups={"view"})
     */
    private $code;

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return Enable2FaVM
     */
    public function setCode($code): Enable2FaVM
    {
        $this->code = $code;
        return $this;
    }
}