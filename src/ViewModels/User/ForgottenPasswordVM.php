<?php

namespace App\ViewModels\User;

use JMS\Serializer\Annotation as Serializer;
use Utils\ViewModel;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ForgottenPasswordVM
 * @package View Model
 */
class ForgottenPasswordVM extends ViewModel
{
    /**
     * @var string
     *  @Assert\Email(message="Please enter a valid email")
     * @Assert\NotBlank(message="Please enter a valid email address")
     * @Serializer\Groups(groups={"view"})
     */
    protected $email;

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return ForgottenPasswordVM
     */
    public function setEmail($email): ForgottenPasswordVM
    {
        $this->email = $email;
        return $this;
    }

}