<?php

namespace App\ViewModels\User;

use JMS\Serializer\Annotation as Serializer;
use Utils\Constraint\CssClass;
use Utils\Constraint\FieldType;
use Utils\ViewModel;
use Symfony\Component\Validator\Constraints as Assert;
use Rollerworks\Component\PasswordStrength\Validator\Constraints as RollerworksPassword;

/**
 * Class RegistrationVM
 * @package View Model
 *
 */
class RegistrationVM extends ViewModel
{

    /**
     * @var string
     * @Assert\NotBlank(message="Please enter your first name")
     * @Assert\Length(min="3", max="128",
     *     minMessage="First name must be at least {{ limit }} characters long",
     *     maxMessage="First name cannot be longer than {{ limit }} characters")
     * @CssClass(class="text-capitalize")
     * @Serializer\Groups(groups={"view"})
     *
     */
    private $firstName;

    /**
     * @var string
     * @Assert\NotBlank(message="Please enter your last name")
     * @Assert\Length(min="3", max="128",
     *     minMessage="Last name must be at least {{ limit }} characters long",
     *     maxMessage="Last name cannot be longer than {{ limit }} characters")
     * @CssClass(class="text-capitalize")
     * @Serializer\Groups(groups={"view"})
     */
    private $lastName;

    /**
     * @var string
     * @Assert\Email(message="Please enter a valid email")
     * @Assert\NotBlank(message="Please enter a valid email address")
     * @Assert\Length(min="5", max="255",
     *     minMessage="Email must be at least {{ limit }} characters long",
     *     maxMessage="Email cannot be longer than {{ limit }} characters")
     * @Serializer\Groups(groups={"view"})
     */
    private $email;

    /**
     * @var string
     *
     * @Assert\Length(min="6", minMessage="The password must be at least {{ limit }} characters long")
     * @Assert\NotBlank(message="Please enter a valid password")
     * @RollerworksPassword\PasswordRequirements(requireLetters=true, requireNumbers=true, requireCaseDiff=true,
     *     missingNumbersMessage="The password must include at least 1 number",
     *      missingLettersMessage="The password must include at least 1 letter",
     *      requireCaseDiffMessage="The password must contain at least 1 lower and upper case character")
     * @FieldType(type="password")
     * @Serializer\Groups(groups={"view"})
     */
    private $password;

    /**
     * @var string
     * @Assert\Expression("value == this.getPassword()", message="The passwords do not match")
     * @FieldType(type="password")
     * @Serializer\Groups(groups={"view"})
     */
    private $confirmPassword;

    /**
     * @return string
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     * @return RegistrationVM
     */
    public function setFirstName(?string $firstName): RegistrationVM
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     * @return RegistrationVM
     */
    public function setLastName(?string $lastName): RegistrationVM
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return RegistrationVM
     */
    public function setEmail(?string $email): RegistrationVM
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     * @return RegistrationVM
     */
    public function setPassword(?string $password): RegistrationVM
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getConfirmPassword(): ?string
    {
        return $this->confirmPassword;
    }

    /**
     * @param string|null $confirmPassword
     * @return RegistrationVM
     */
    public function setConfirmPassword(?string $confirmPassword): RegistrationVM
    {
        $this->confirmPassword = $confirmPassword;
        return $this;
    }

}