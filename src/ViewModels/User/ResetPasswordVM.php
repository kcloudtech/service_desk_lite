<?php

namespace App\ViewModels\User;

use JMS\Serializer\Annotation as Serializer;
use Utils\Constraint\FieldType;
use Utils\ViewModel;
use Rollerworks\Component\PasswordStrength\Validator\Constraints as RollerworksPassword;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ResetPasswordVM
 * @package View Model
 */
class ResetPasswordVM extends ViewModel
{

    /**
     * @var string
     *
     * @Assert\Length(min="6", minMessage="The password must be at least {{ limit }} characters long")
     * @RollerworksPassword\PasswordRequirements(requireLetters=true, requireNumbers=true, requireCaseDiff=true,
     *     missingNumbersMessage="The password must be alphanumeric",
     * missingLettersMessage="The password must be alphanumeric",
     * requireCaseDiffMessage="The password must contain at least 1 lower and upper case character")
     * @FieldType(type="Password")
     * @Serializer\Groups(groups={"view"})
     */
    private $newPassword;

    /**
     * @var string
     * @Assert\Expression("value == this.getNewPassword()", message="The passwords do not match")
     * @FieldType(type="Password")
     * @Serializer\Groups(groups={"view"})
     */
    private $confirmPassword;

    /**
     * @return string
     */
    public function getNewPassword(): ?string
    {
        return $this->newPassword;
    }

    /**
     * @param string|null $newPassword
     * @return ResetPasswordVM
     */
    public function setNewPassword(?string $newPassword): ResetPasswordVM
    {
        $this->newPassword = $newPassword;
        return $this;
    }

    /**
     * @return string
     */
    public function getConfirmPassword():?string
    {
        return $this->confirmPassword;
    }

    /**
     * @param string|null $confirmPassword
     * @return ResetPasswordVM
     */
    public function setConfirmPassword(?string $confirmPassword): ResetPasswordVM
    {
        $this->confirmPassword = $confirmPassword;
        return $this;
    }


}