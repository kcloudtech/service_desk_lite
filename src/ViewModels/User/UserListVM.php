<?php

namespace App\ViewModels\User;

use App\Entity\Role;
use App\Entity\UserPermission;
use JMS\Serializer\Annotation as Serializer;
use Utils\ViewModel;
use DateTime;

/**
 * Class UserListVM
 * @package View Model
 */
class UserListVM extends ViewModel
{
    /**
     * @var int
     * @Serializer\Groups({"api"})
     */
    private $id;

    /**
     * @var string
     * @Serializer\Groups({"api", "list"})
     * @Serializer\Accessor(getter="getFirstName")
     */
    private $firstName;

    /**
     * @var string
     * @Serializer\Groups({"api", "list"})
     *  @Serializer\Accessor(getter="getLastName")
     */
    private $lastName;

    /**
     * @var string
     * @Serializer\Groups({"api", "list"})
     */
    private $email;

    /**
     * @var string | Role
     * @Serializer\Groups({"api", "list"})
     * @Serializer\Accessor(getter="getRole")
     *
     */
    private $role;

    /**
     * @var DateTime
     * @Serializer\Groups({"api", "list"})
     * @Serializer\Type("DateTime<'d/m/Y H:i'>")
     */
    private $lastLogin;

    /**
     * @var DateTime
     * @Serializer\Groups({"api", "list"})
     * @Serializer\Type("DateTime<'d/m/Y H:i'>")
     */
    private $lastFailedLogin;

    /**
     * @var int
     * @Serializer\Groups({"api", "list"})
     */
    private $failedLoginAttempts = 0;

    /**
     * @var boolean
     * @Serializer\Groups({"api", "list"})
     */
    private $isActive = true;

    /**
     * @var DateTime
     * @Serializer\Groups({"api", "list"})
     * @Serializer\Type("DateTime<'d/m/Y H:i'>")
     */
    private $created;

    /**
     * @var DateTime
     * @Serializer\Groups({"api", "list"})
     * @Serializer\Type("DateTime<'d/m/Y H:i'>")
     */
    private $updated;

    /**
     * @var UserPermission[] | null
     */
    protected $userPermissions;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return UserListVM
     */
    public function setId(?int $id): UserListVM
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getRole() :string
    {
        return $this->role;
    }

    /**
     * @return Role
     */
    public function getRoleObject() :Role
    {
        return $this->role;
    }

    /**
     * @param Role $role
     * @return UserListVM
     */
    public function setRole(Role $role): UserListVM
    {
        $this->role = $role;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): ?string
    {
        return ucwords($this->firstName);
    }

    /**
     * @param string|null $firstName
     * @return UserListVM
     */
    public function setFirstName(?string $firstName): UserListVM
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): ?string
    {
        return ucwords($this->lastName);
    }

    /**
     * @param string|null $lastName
     * @return UserListVM
     */
    public function setLastName(?string $lastName): UserListVM
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return UserListVM
     */
    public function setEmail(?string $email): UserListVM
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getLastLogin(): ?string
    {
        return $this->lastLogin;
    }

    /**
     * @param DateTime|null $lastLogin
     * @return UserListVM
     */
    public function setLastLogin(?DateTime $lastLogin): UserListVM
    {
        $this->lastLogin = $lastLogin;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getLastFailedLogin(): ?DateTime
    {
        return $this->lastFailedLogin;
    }

    /**
     * @param DateTime|null $lastFailedLogin
     * @return UserListVM
     */
    public function setLastFailedLogin(?DateTime $lastFailedLogin): UserListVM
    {
        $this->lastFailedLogin = $lastFailedLogin;
        return $this;
    }

    /**
     * @return int
     */
    public function getFailedLoginAttempts(): ?int
    {
        return $this->failedLoginAttempts;
    }

    /**
     * @param int|null $failedLoginAttempts
     * @return UserListVM
     */
    public function setFailedLoginAttempts(?int $failedLoginAttempts): UserListVM
    {
        $this->failedLoginAttempts = $failedLoginAttempts;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     * @return UserListVM
     */
    public function setIsActive(bool $isActive): UserListVM
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreated(): ?DateTime
    {
        return $this->created;
    }

    /**
     * @param DateTime|null $created
     * @return UserListVM
     */
    public function setCreated(?DateTime $created): UserListVM
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUpdated(): ?DateTime
    {
        return $this->updated;
    }

    /**
     * @param DateTime|null $updated
     * @return UserListVM
     */
    public function setUpdated(?DateTime $updated): UserListVM
    {
        $this->updated = $updated;
        return $this;
    }

    /**
     * @return UserPermission[]|null
     */
    public function getUserPermissions(): ?array
    {
        return $this->userPermissions;
    }

}