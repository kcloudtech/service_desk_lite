<?php

namespace App\ViewModels\User;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\PersistentCollection;

class UserPermissionsVM
{
    /**
     * @var UserVM
     */
    public  $userDetails;

    /**
     * @var PersistentCollection
     */
    public $permissions;

    /**
     * @return UserVM
     */
    public function getUserDetails(): ?UserVM
    {
        return $this->userDetails;
    }

    /**
     * @param UserVM|null $userDetails
     * @return UserPermissionsVM
     */
    public function setUserDetails(?UserVM $userDetails): self
    {
        $this->userDetails = $userDetails;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getPermissions(): ?Collection
    {
        return $this->permissions;
    }

    /**
     * @param Collection|null $permissions
     * @return UserPermissionsVM
     */
    public function setPermissions(?Collection $permissions): self
    {
        $this->permissions = $permissions;
        return $this;
    }

}