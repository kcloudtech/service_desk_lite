<?php

namespace App\ViewModels\User;

use DateTime;
use JMS\Serializer\Annotation as Serializer;
use Utils\Constraint\CssClass;
use Utils\Constraint\FieldType;
use Utils\ViewModel;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class UserProfile
 * @package View Model
 */
class UserProfileVM extends ViewModel
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var UploadedFile
     * @Assert\File(mimeTypes={"image/jpeg", "image/jpg", "image/png", "image/gif"}, maxSize = "2M")
     * @FieldType(type="ProfilePicture")
     * @Serializer\Groups(groups={"view"})
     */
    private $profilePicture;

    /**
     * @var string
     * @Assert\NotBlank(message="Please enter your first name")
     * @Assert\Length(min="3", max="128",
     *     minMessage="First name must be at least {{ limit }} characters long",
     *     maxMessage="First name cannot be longer than {{ limit }} characters")
     * @CssClass(class="text-capitalize")
     * @Serializer\Groups(groups={"view"})
     */
    private $firstName;

    /**
     * @var string
     * @Assert\NotBlank(message="Please enter your last name")
     * @Assert\Length(min="3", max="128",
     *     minMessage="Last name must be at least {{ limit }} characters long",
     *     maxMessage="Last name cannot be longer than {{ limit }} characters")
     * @CssClass(class="text-capitalize")
     * @Serializer\Groups(groups={"view"})
     */
    private $lastName;

    /**
     * @var string
     * @Assert\Email(message="Please enter a valid email")
     * @Assert\NotBlank(message="Please enter a valid email address")
     * @Assert\Length(min="5", max="255",
     *     minMessage="emaill must be at least {{ limit }} characters long",
     *     maxMessage="email cannot be longer than {{ limit }} characters")
     * @Serializer\Groups(groups={"view"})
     */
    private $email;

    /**
     * @var DateTime | null
     */
    private $updated;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return UserProfileVM
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     * @return UserProfileVM
     */
    public function setFirstName(?string $firstName): UserProfileVM
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     * @return UserProfileVM
     */
    public function setLastName(?string $lastName): UserProfileVM
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return UserProfileVM
     */
    public function setEmail(?string $email): UserProfileVM
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return UploadedFile
     */
    public function getProfilePicture(): ?UploadedFile
    {
        return $this->profilePicture;
    }

    /**
     * @param UploadedFile|null $profilePicture
     * @return UserProfileVM
     */
    public function setProfilePicture(?UploadedFile $profilePicture): UserProfileVM
    {
        $this->profilePicture = $profilePicture;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdated(): ?DateTime
    {
        return $this->updated;
    }

    /**
     * @param DateTime|null $updated
     * @return UserProfileVM
     */
    public function setUpdated(?DateTime $updated): self
    {
        $this->updated = $updated;
        return $this;
    }

    public static function getAlias(): string
    {
        return 'User';
    }
}