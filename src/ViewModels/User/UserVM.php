<?php

namespace App\ViewModels\User;

use App\Entity\Role;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\PersistentCollection;
use JMS\Serializer\Annotation as Serializer;
use Utils\Constraint\CssClass;
use Utils\Constraint\Data;
use Utils\Constraint\FieldType;
use Utils\Constraint\Remote;
use Utils\ViewModel;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class UserVM
 *
 * Description
 *
 * @package View Model
 */
class UserVM extends ViewModel
{
    /**
     * @var int | null
     * @FieldType(type="Hidden")
     * @Serializer\Groups({"api", "view"})
     */
    private $id;

    /**
     * @var UploadedFile
     * @Assert\File(mimeTypes={"image/jpeg", "image/jpg", "image/png", "image/gif"}, maxSize = "2M")
     * @FieldType(type="ProfilePicture")
     * @Serializer\Groups({"api", "view"})
     */
    private $profilePicture;

    /**
     * @var string | null
     * @Assert\NotBlank(message="Please enter your first name")
     * @Assert\Length(min="3", max="128",
     *     minMessage="First name must be at least {{ limit }} characters long",
     *     maxMessage="First name cannot be longer than {{ limit }} characters")
     * @CssClass(class="text-capitalize")
     * @Serializer\Groups({"api", "view"})
     */
    private $firstName;

    /**
     * @var string | null
     * @Assert\NotBlank(message="Please enter your last name")
     * @Assert\Length(min="3", max="128",
     *     minMessage="Last name must be at least {{ limit }} characters long",
     *     maxMessage="Last name cannot be longer than {{ limit }} characters")
     * @CssClass(class="text-capitalize")
     * @Serializer\Groups({"api", "view"})
     */
    private $lastName;

    /**
     * @var Role | Role[]
     * @FieldType(type="Entity", entity="Role")
     * @Assert\NotBlank(message="Please select a role")
     * @Serializer\Groups({"api", "view"})
     */
    private $role;

    /**
     * @var PersistentCollection
     *  @Serializer\Groups({"api"})
     * @FieldType(type="ignore")
     */
    private $userPermissions;

    /**
     * @var string | null
     * @Assert\Email(message="Please enter a valid email")
     * @Assert\NotBlank(message="Please enter a valid email")
     * @Remote(url="user_email_unique", message="Sorry, that email address cant be used")
     * @Data(attribute="remote-additionalfield", value="form_userDetails_id")
     * @Assert\Length(min="5", max="255",
     *     minMessage="value must be at least {{ limit }} characters long",
     *     maxMessage="value cannot be longer than {{ limit }} characters")
     * @Serializer\Groups({"api", "view"})
     */
    private $email;

    /**
     * @var bool
     * @Serializer\Groups({"api", "view"})
     */
    private $isActive = false;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return UserVM
     */
    public function setId(?int $id): UserVM
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     * @return UserVM
     */
    public function setFirstName(?string $firstName): UserVM
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     * @return UserVM
     */
    public function setLastName(?string $lastName): UserVM
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return Role|Role[]
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param Role|Role[] $role
     * @return UserVM
     */
    public function setRole($role): self
    {
        $this->role = $role;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getUserPermissions()
    {
        return $this->userPermissions;
    }

    /**
     * @param Collection $userPermissions
     * @return UserVM
     */
    public function setUserPermissions(Collection $userPermissions): UserVM
    {
        $this->userPermissions = $userPermissions;
        return $this;
    }

    /**
     * @return File|UploadedFile
     */
    public function getProfilePicture()
    {
        return $this->profilePicture;
    }

    /**
     * @param UploadedFile $profilePicture
     * @return UserVM
     */
    public function setProfilePicture(UploadedFile $profilePicture): UserVM
    {
        $this->profilePicture = $profilePicture;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return UserVM
     */
    public function setEmail(?string $email): UserVM
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     * @return UserVM
     */
    public function setIsActive(bool $isActive): UserVM
    {
        $this->isActive = $isActive;
        return $this;
    }
}
