<?php

namespace App\Tests;

use App\Repository\CustomerRepository;
use App\Repository\UserRepository;
use App\Repository\ContactRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Utils\HashidHelper;

/**
 * Class ContactTest
 *
 * Basic CRUD integration tests for contact controller
 */
class ContactTest extends WebTestCase
{
    /**
     * @var RouterInterface
     */
    private $router;

    private $client;

    protected function setUp(): void
    {
        $this->client =  static::createClient();
        $this->router = static::$container->get('router');

        // login the user in
        $userRepository = static::$container->get(UserRepository::class);
        $user  = $userRepository->findOneByEmail('superadmin@test.com');
        $this->client->loginUser($user);
    }

    public function testListContacts(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('list_contacts'));

        self::assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        self::assertGreaterThanOrEqual(1, $crawler->filter('#data_table tbody tr')->count());
        self::assertStringContainsString('test contact', $crawler->filter('#data_table tbody tr')->text());
    }

    public function testCreateContact(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('new_contact'));

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $repo = static::$container->get(CustomerRepository::class);
        $entity  = current($repo->findAll());

        $form = $buttonCrawlerNode->form([
            'form[customer]' => $entity->getId(),
            'form[firstName]' => 'testing',
            'form[lastName]' => 'contact',
            'form[role]' => 'test role',
            'form[landline]' => '01282 612303',
            'form[mobile]' => '07872930655',
            'form[email]' => 'testing@test.com',
        ]);

        $this->client->submit($form);

        self::assertResponseRedirects();
    }

    public function testCreateContactMissingRequired(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('new_contact'));

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $form = $buttonCrawlerNode->form([
            'form[landline]' => '01282 612303',
            'form[mobile]' => '07872930655',
            'form[email]' => 'testing@test.com',
        ]);

        $this->client->submit($form);

        self::assertStringContainsString('Customer* is required', $this->client->getResponse()->getContent());
        self::assertStringContainsString('First Name* is required', $this->client->getResponse()->getContent());
        self::assertStringContainsString('Last Name* is required', $this->client->getResponse()->getContent());
        self::assertStringContainsString('Role* is required', $this->client->getResponse()->getContent());
    }

    public function testLandlineTooLong(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('new_contact'));

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $repo = static::$container->get(CustomerRepository::class);
        $entity  = current($repo->findAll());

        $form = $buttonCrawlerNode->form([
            'form[customer]' => $entity->getId(),
            'form[firstName]' => 'testing',
            'form[lastName]' => 'contact',
            'form[role]' => 'test role',
            'form[landline]' => '0128256123503',
            'form[mobile]' => '07872930655',
            'form[email]' => 'testing@test.com',
        ]);

        $this->client->submit($form);

        self::assertStringContainsString('form_landline_errors', $this->client->getResponse()->getContent());
        self::assertStringContainsString('<span class="form-error-message">This value is too long. It should have 12 characters or less.</span>', $this->client->getResponse()->getContent());
    }

    public function testPhoneMobileTooLong(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('new_contact'));

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $repo = static::$container->get(CustomerRepository::class);
        $entity  = current($repo->findAll());

        $form = $buttonCrawlerNode->form([
            'form[customer]' => $entity->getId(),
            'form[firstName]' => 'testing',
            'form[lastName]' => 'contact',
            'form[role]' => 'test role',
            'form[landline]' => '01282 612303',
            'form[mobile]' => '0787268465930655',
            'form[email]' => 'testing@test.com',
        ]);

        $this->client->submit($form);

        self::assertStringContainsString('form_mobile_errors', $this->client->getResponse()->getContent());
        self::assertStringContainsString('<span class="form-error-message">This value is too long. It should have 12 characters or less.</span>', $this->client->getResponse()->getContent());
    }

    public function testInvalidEmail(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('new_contact'));

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $repo = static::$container->get(CustomerRepository::class);
        $entity  = current($repo->findAll());

        $form = $buttonCrawlerNode->form([
            'form[customer]' => $entity->getId(),
            'form[firstName]' => 'testing',
            'form[lastName]' => 'contact',
            'form[role]' => 'test role',
            'form[landline]' => '01282 612303',
            'form[mobile]' => '07872930600',
            'form[email]' => 'testing@diknvbdofn',
        ]);

        $this->client->submit($form);

        self::assertStringContainsString('form_email_errors', $this->client->getResponse()->getContent());
        self::assertStringContainsString('<span class="form-error-message">This value is not a valid email address.</span>', $this->client->getResponse()->getContent());
    }

    public function testUpdateContact(): void
    {
        $repo = static::$container->get(ContactRepository::class);
        $entity  = current($repo->findAll());

        $id = HashidHelper::encode($entity->getId());

        $crawler = $this->client->request('GET', $this->router->generate('update_contact', ['hashid' => $id]));

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $form = $buttonCrawlerNode->form([
            'form[firstName]' => 'updated',
            'form[lastName]' => 'updated',
        ]);

        $this->client->submit($form);

        self::assertResponseRedirects();
    }

    public function testDeleteContact(): void
    {
        $repo = static::$container->get(ContactRepository::class);
        $entity  = current($repo->findAll());

        $id = HashidHelper::encode($entity->getId());

        $this->client->request('GET', $this->router->generate('delete_contact', ['hashid' => $id]));

        $this->client->followRedirect();
        self::assertStringContainsString('The contact has been deleted', $this->client->getResponse()->getContent());
    }

    public function testListArchived(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('list_archived_contacts'));

        self::assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        self::assertGreaterThanOrEqual(1, $crawler->filter('#data_table tbody tr')->count());
        self::assertStringContainsString('deleted contact', $crawler->filter('#data_table tbody tr')->text());
    }

    public function testListHistory(): void
    {
        $repo = static::$container->get(ContactRepository::class);
        $entity  = current($repo->findAll());

        $id = HashidHelper::encode($entity->getId());

        $crawler = $this->client->request('GET', $this->router->generate('contact_history', ['hashid' => $id]));

        self::assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        self::assertGreaterThanOrEqual(1, $crawler->filter('#data_table tbody tr')->count());
        self::assertStringContainsString('create', $crawler->filter('#data_table tbody tr')->text());
    }
}
