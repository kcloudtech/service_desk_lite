<?php

namespace App\Tests;

use App\Repository\CustomerRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Utils\HashidHelper;

/**
 * Class CustomerTest
 *
 * Basic CRUD integration tests for customer controller
 */
class CustomerTest extends WebTestCase
{
    /**
     * @var RouterInterface
     */
    private $router;

    private $client;

    protected function setUp(): void
    {
        $this->client =  static::createClient();
        $this->router = static::$container->get('router');

        // login the user in
        $userRepository = static::$container->get(UserRepository::class);
        $user  = $userRepository->findOneByEmail('superadmin@test.com');
        $this->client->loginUser($user);
    }

    public function testListCustomers(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('list_customers'));

        self::assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        self::assertGreaterThanOrEqual(1, $crawler->filter('#data_table tbody tr')->count());
        self::assertStringContainsString('Test Customer', $crawler->filter('#data_table tbody tr')->text());
    }

    public function testCreateCustomer(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('new_customer'));

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $form = $buttonCrawlerNode->form([
            'form[name]' => 'test name',
            'form[address1]' => 'test lane 1',
            'form[address2]' => 'test street',
            'form[townOrCity]' => 'test town',
            'form[county]' => 'test county',
            'form[postcode]' => 'TT1 1TT',
        ]);

        $this->client->submit($form);

        self::assertResponseRedirects();
    }

    public function testCreateCustomerMissingRequired(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('new_customer'));

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $form = $buttonCrawlerNode->form([
            'form[address2]' => 'test street',
        ]);

        $this->client->submit($form);

        self::assertStringContainsString('Name* is required', $this->client->getResponse()->getContent());
        self::assertStringContainsString('Address1* is required', $this->client->getResponse()->getContent());
        self::assertStringContainsString('Town Or City* is required', $this->client->getResponse()->getContent());
        self::assertStringContainsString('County* is required', $this->client->getResponse()->getContent());
        self::assertStringContainsString('Postcode* is required', $this->client->getResponse()->getContent());
    }

    public function testUpdateCustomer(): void
    {
        $repo = static::$container->get(CustomerRepository::class);
        $entity  = current($repo->findAll());

        $id = HashidHelper::encode($entity->getId());

        $crawler = $this->client->request('GET', $this->router->generate('update_customer', ['hashid' => $id]));

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $form = $buttonCrawlerNode->form([
            'form[name]' => 'test update',
            'form[address1]' => 'test lane 2',
            'form[address2]' => 'test street',
            'form[townOrCity]' => 'test town',
            'form[county]' => 'test county',
            'form[postcode]' => 'TT1 1TT',
        ]);

        $this->client->submit($form);

        self::assertResponseRedirects();
    }

    public function testDeleteCustomer(): void
    {
        $repo = static::$container->get(CustomerRepository::class);
        $entity  = current($repo->findAll());

        $id = HashidHelper::encode($entity->getId());

        $this->client->request('GET', $this->router->generate('delete_customer', ['hashid' => $id]));

        $this->client->followRedirect();
        self::assertStringContainsString('The customer has been deleted', $this->client->getResponse()->getContent());
    }

    public function testListArchived(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('list_archived_customers'));

        self::assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        self::assertGreaterThanOrEqual(1, $crawler->filter('#data_table tbody tr')->count());
        self::assertStringContainsString('Deleted Customer', $crawler->filter('#data_table tbody tr')->text());
    }

    public function testListHistory(): void
    {
        $repo = static::$container->get(CustomerRepository::class);
        $entity  = current($repo->findAll());

        $id = HashidHelper::encode($entity->getId());

        $crawler = $this->client->request('GET', $this->router->generate('customer_history', ['hashid' => $id]));

        self::assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        self::assertGreaterThanOrEqual(1, $crawler->filter('#data_table tbody tr')->count());
        self::assertStringContainsString('create', $crawler->filter('#data_table tbody tr')->text());
    }
}