<?php

namespace App\Tests;

use App\Repository\UserRepository;
use App\Repository\IssueTypeRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Utils\HashidHelper;

class IssueTypeTest extends WebTestCase
{
    /**
     * @var RouterInterface
     */
    private $router;

    private $client;

    protected function setUp(): void
    {
        $this->client =  static::createClient();
        $this->router = static::$container->get('router');

        // login the user in
        $userRepository = static::$container->get(UserRepository::class);
        $user  = $userRepository->findOneByEmail('superadmin@test.com');
        $this->client->loginUser($user);
    }

    public function testListIssueTypes(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('list_issue_types'));

        self::assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        self::assertGreaterThanOrEqual(1, $crawler->filter('#data_table tbody tr')->count());
        self::assertStringContainsString('Type 1', $crawler->filter('#data_table tbody tr')->text());
    }

    public function testCreateIssueType(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('new_issue_type'));

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $form = $buttonCrawlerNode->form([
            'form[description]' => 'test type',
            'form[icon]' => 'fa fa-bug',
        ]);

        $this->client->submit($form);

        self::assertResponseRedirects();
    }

    public function testCreateIssueTypeMissingFields(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('new_issue_type'));

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $form = $buttonCrawlerNode->form([
            'form[description]' => 'test type',
            'form[icon]' => 'fa fa-bug',
        ]);

        $this->client->submit($form);

        self::assertStringContainsString('Description* is required', $this->client->getResponse()->getContent());
        self::assertStringContainsString('Icon* is required', $this->client->getResponse()->getContent());
    }

    public function testUpdateIssueType(): void
    {
        $repo = static::$container->get(IssueTypeRepository::class);
        $entity  = current($repo->findAll());

        $id = HashidHelper::encode($entity->getId());

        $crawler = $this->client->request('GET', $this->router->generate('update_issue_type', ['hashid' => $id]));

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $form = $buttonCrawlerNode->form([
            'form[description]' => 'test update',
            'form[icon]' => 'fa fa-trash',
        ]);

        $this->client->submit($form);

        self::assertResponseRedirects();
    }

    public function testDeleteIssueType(): void
    {
        $repo = static::$container->get(IssueTypeRepository::class);
        $entity  = current($repo->findAll());

        $id = HashidHelper::encode($entity->getId());

        $this->client->request('GET', $this->router->generate('delete_issue_type', ['hashid' => $id]));

        $this->client->followRedirect();
        self::assertStringContainsString('The issue type has been deleted', $this->client->getResponse()->getContent());
    }

    public function testListArchived(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('list_archived_issue_types'));

        self::assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        self::assertGreaterThanOrEqual(1, $crawler->filter('#data_table tbody tr')->count());
        self::assertStringContainsString('deleted type', $crawler->filter('#data_table tbody tr')->text());
    }

    public function testListHistory(): void
    {
        $repo = static::$container->get(IssueTypeRepository::class);
        $entity  = current($repo->findAll());

        $id = HashidHelper::encode($entity->getId());

        $crawler = $this->client->request('GET', $this->router->generate('issue_type_history', ['hashid' => $id]));

        self::assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        self::assertGreaterThanOrEqual(1, $crawler->filter('#data_table tbody tr')->count());
        self::assertStringContainsString('create', $crawler->filter('#data_table tbody tr')->text());
    }
}
