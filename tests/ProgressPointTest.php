<?php

namespace App\Tests;

use App\Repository\UserRepository;
use App\Repository\ProgressPointRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Utils\HashidHelper;

/**
 * Class ProgressPointTest
 *
 * Basic CRUD integration tests for progress points controller
 */
class ProgressPointTest extends WebTestCase
{
    /**
     * @var RouterInterface
     */
    private $router;

    private $client;

    protected function setUp(): void
    {
        $this->client =  static::createClient();
        $this->router = static::$container->get('router');

        // login the user in
        $userRepository = static::$container->get(UserRepository::class);
        $user  = $userRepository->findOneByEmail('superadmin@test.com');
        $this->client->loginUser($user);
    }

    public function testListProgressPoints(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('list_progress_points'));

        self::assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        self::assertGreaterThanOrEqual(1, $crawler->filter('#data_table tbody tr')->count());
        self::assertStringContainsString('Progress Point', $crawler->filter('#data_table tbody tr')->text());
    }

    public function testCreateProgressPoint(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('new_progress_point'));

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $form = $buttonCrawlerNode->form([
            'form[description]' => 'test point',
            'form[position]' => '1',
        ]);

        $this->client->submit($form);

        self::assertResponseRedirects();
    }

    public function testCreateProgressPointMissingRequired(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('new_progress_point'));

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $form = $buttonCrawlerNode->form([
        ]);

        $this->client->submit($form);

        self::assertStringContainsString('Description* is required', $this->client->getResponse()->getContent());
        self::assertStringContainsString('Position* is required', $this->client->getResponse()->getContent());
    }

    public function testInvalidPosition(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('new_progress_point'));

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $form = $buttonCrawlerNode->form([
            'form[description]' => 'test point',
            'form[position]' => 'dhfdgf',
        ]);

        $this->client->submit($form);

        self::assertStringContainsString('form_position_errors', $this->client->getResponse()->getContent());
        self::assertStringContainsString('<span class="form-error-message">This value is not valid.</span>', $this->client->getResponse()->getContent());
    }

    public function testNegativePosition(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('new_progress_point'));

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $form = $buttonCrawlerNode->form([
            'form[description]' => 'test point',
            'form[position]' => '-5',
        ]);

        $this->client->submit($form);

        self::assertStringContainsString('form_position_errors', $this->client->getResponse()->getContent());
        self::assertStringContainsString('<span class="form-error-message">This value should be either positive or zero.</span>', $this->client->getResponse()->getContent());
    }

    public function testUpdateProgressPoint(): void
    {
        $repo = static::$container->get(ProgressPointRepository::class);
        $entity  = current($repo->findAll());

        $id = HashidHelper::encode($entity->getId());

        $crawler = $this->client->request('GET', $this->router->generate('update_progress_point', ['hashid' => $id]));

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $form = $buttonCrawlerNode->form([
            'form[description]' => 'test update',
            'form[position]' => '2',
        ]);

        $this->client->submit($form);

        self::assertResponseRedirects();
    }

    public function testDeleteProgressPoint(): void
    {
        $repo = static::$container->get(ProgressPointRepository::class);
        $entity  = current($repo->findAll());

        $id = HashidHelper::encode($entity->getId());

        $this->client->request('GET', $this->router->generate('delete_progress_point', ['hashid' => $id]));

        $this->client->followRedirect();
        self::assertStringContainsString('The progress point has been deleted', $this->client->getResponse()->getContent());
    }

    public function testListArchived(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('list_archived_progress_points'));

        self::assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        self::assertGreaterThanOrEqual(1, $crawler->filter('#data_table tbody tr')->count());
        self::assertStringContainsString('Deleted Point', $crawler->filter('#data_table tbody tr')->text());
    }

    public function testListHistory(): void
    {
        $repo = static::$container->get(ProgressPointRepository::class);
        $entity  = current($repo->findAll());

        $id = HashidHelper::encode($entity->getId());

        $crawler = $this->client->request('GET', $this->router->generate('progress_point_history', ['hashid' => $id]));

        self::assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        self::assertGreaterThanOrEqual(1, $crawler->filter('#data_table tbody tr')->count());
        self::assertStringContainsString('create', $crawler->filter('#data_table tbody tr')->text());
    }
}
