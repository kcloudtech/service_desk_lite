<?php

namespace App\Tests;

use App\Repository\ProgressPointRepository;
use App\Repository\StatusRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Utils\HashidHelper;

/**
 * Class StatusTest
 *
 * Basic CRUD integration tests for progress points controller
 */
class StatusTest extends WebTestCase
{
    /**
     * @var RouterInterface
     */
    private $router;

    private $client;

    protected function setUp(): void
    {
        $this->client =  static::createClient();
        $this->router = static::$container->get('router');

        // login the user in
        $userRepository = static::$container->get(UserRepository::class);
        $user  = $userRepository->findOneByEmail('superadmin@test.com');
        $this->client->loginUser($user);
    }

    public function testListStatuses(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('list_stati'));

        self::assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        self::assertGreaterThanOrEqual(1, $crawler->filter('#data_table tbody tr')->count());
        self::assertStringContainsString('Status 1', $crawler->filter('#data_table tbody tr')->text());
    }

    public function testCreateStatus(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('new_status'));

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $repo = static::$container->get(ProgressPointRepository::class);
        $entity  = current($repo->findAll());

        $form = $buttonCrawlerNode->form([
            'form[progressPoint]' => $entity->getId(),
            'form[description]' => 'my test',
        ]);

        $this->client->submit($form);

        self::assertResponseRedirects();
    }

    public function testCreateStatusMissingRequired(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('new_status'));

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $form = $buttonCrawlerNode->form([
        ]);

        $this->client->submit($form);

        self::assertStringContainsString('Description* is required', $this->client->getResponse()->getContent());
        self::assertStringContainsString('Progress Point* is required', $this->client->getResponse()->getContent());
    }

    public function testUpdateStatus(): void
    {
        $repo = static::$container->get(StatusRepository::class);
        $entity  = current($repo->findAll());

        $id = HashidHelper::encode($entity->getId());

        $crawler = $this->client->request('GET', $this->router->generate('update_status', ['hashid' => $id]));

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $form = $buttonCrawlerNode->form([
            'form[description]' => 'my test update',
        ]);

        $this->client->submit($form);

        self::assertResponseRedirects();
    }

    public function testDeleteStatus(): void
    {
        $repo = static::$container->get(StatusRepository::class);
        $entity  = current($repo->findAll());

        $id = HashidHelper::encode($entity->getId());

        $this->client->request('GET', $this->router->generate('delete_status', ['hashid' => $id]));

        $this->client->followRedirect();
        self::assertStringContainsString('The status has been deleted', $this->client->getResponse()->getContent());
    }

    public function testListArchived(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('list_archived_statuses'));

        self::assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        self::assertGreaterThanOrEqual(1, $crawler->filter('#data_table tbody tr')->count());
        self::assertStringContainsString('Deleted Status', $crawler->filter('#data_table tbody tr')->text());
    }

    public function testListHistory(): void
    {
        $repo = static::$container->get(StatusRepository::class);
        $entity  = current($repo->findAll());

        $id = HashidHelper::encode($entity->getId());

        $crawler = $this->client->request('GET', $this->router->generate('status_history', ['hashid' => $id]));

        self::assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        self::assertGreaterThanOrEqual(1, $crawler->filter('#data_table tbody tr')->count());
        self::assertStringContainsString('create', $crawler->filter('#data_table tbody tr')->text());
    }
}
