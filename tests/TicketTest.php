<?php

namespace App\Tests;

use App\Repository\CustomerRepository;
use App\Repository\IssueTypeRepository;
use App\Repository\StatusRepository;
use App\Repository\UserRepository;
use App\Repository\TicketRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Utils\HashidHelper;

class TicketTest extends WebTestCase
{
    /**
     * @var RouterInterface
     */
    private $router;

    private $client;

    protected function setUp(): void
    {
        $this->client =  static::createClient();
        $this->router = static::$container->get('router');

        // login the user in
        $userRepository = static::$container->get(UserRepository::class);
        $user  = $userRepository->findOneByEmail('superadmin@test.com');
        $this->client->loginUser($user);
    }

    public function testListTickets(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('list_tickets'));

        self::assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        self::assertGreaterThanOrEqual(1, $crawler->filter('#data_table tbody tr')->count());
        self::assertStringContainsString('Test Ticket', $crawler->filter('#data_table tbody tr')->text());
    }

    public function testCreateTicket(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('new_ticket'));

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $userRepository = static::$container->get(UserRepository::class);
        $user  = $userRepository->findOneByEmail('superadmin@test.com');

        $customerRepo = static::$container->get(CustomerRepository::class);
        $customer  = current($customerRepo->findAll());

        $statusRepo = static::$container->get(StatusRepository::class);
        $status  = current($statusRepo->findAll());

        $issueRepo = static::$container->get(IssueTypeRepository::class);
        $issueType  = current($issueRepo->findAll());

        $form = $buttonCrawlerNode->form([
            'form[customer]' => $customer->getId(),
            'form[assignedTo]' => $user->getId(),
            'form[reporter]' => $user->getId(),
            'form[issueType]' => $issueType->getId(),
            'form[subject]' => 'test subject',
            'form[description]' => 'test description',
            'form[priority]' => 1,
            'form[status]' =>  $status->getId(),
            'form[startBy]' => date_create()->format('Y-m-d'),
            'form[dueBy]' => date_create()->format('Y-m-d'),
        ]);

        $this->client->submit($form);

        self::assertResponseRedirects();
    }

    public function testUpdateTicket(): void
    {
        $repo = static::$container->get(TicketRepository::class);
        $entity  = current($repo->findAll());

        $id = HashidHelper::encode($entity->getId());

        $crawler = $this->client->request('GET', $this->router->generate('update_ticket', ['hashid' => $id]));

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $form = $buttonCrawlerNode->form([
            'form[subject]' => 'test subject update',
            'form[description]' => 'test description update',
            'form[priority]' => 4,
        ]);

        $this->client->submit($form);

        self::assertResponseRedirects();
    }

    public function testDeleteTicket(): void
    {
        $repo = static::$container->get(TicketRepository::class);
        $entity  = current($repo->findAll());

        $id = HashidHelper::encode($entity->getId());

        $this->client->request('GET', $this->router->generate('delete_ticket', ['hashid' => $id]));

        $this->client->followRedirect();
        self::assertStringContainsString('The ticket has been deleted', $this->client->getResponse()->getContent());
    }

    public function testListArchived(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('list_archived_tickets'));

        self::assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        self::assertGreaterThanOrEqual(1, $crawler->filter('#data_table tbody tr')->count());
        self::assertStringContainsString('Deleted Ticket', $crawler->filter('#data_table tbody tr')->text());
    }

    public function testListHistory(): void
    {
        $repo = static::$container->get(TicketRepository::class);
        $entity  = current($repo->findAll());

        $id = HashidHelper::encode($entity->getId());

        $crawler = $this->client->request('GET', $this->router->generate('ticket_history', ['hashid' => $id]));

        self::assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        self::assertGreaterThanOrEqual(1, $crawler->filter('#data_table tbody tr')->count());
        self::assertStringContainsString('create', $crawler->filter('#data_table tbody tr')->text());
    }
}
