<?php

namespace App\Tests;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Utils\HashidHelper;

/**
 * Class UserTest
 *
 * Basic CRUD tests for users
 */
class UserTest extends WebTestCase
{
    /**
     * @var RouterInterface
     */
    private $router;

    private $client;

    private $user;

    protected function setUp(): void
    {
        $this->client =  static::createClient();
        $this->router = static::$container->get('router');

        // login the user in
        $userRepository = static::$container->get(UserRepository::class);
        $user  = $userRepository->findOneByEmail('superadmin@test.com');
        $this->client->loginUser($user);

        $this->user  = $userRepository->findOneByEmail('testuser@test.com');
    }

    public function testListUsers(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('list_users'));

        self::assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        self::assertGreaterThanOrEqual(1, $crawler->filter('#data_table tbody tr')->count());
        self::assertStringContainsString('superadmin@test.com', $crawler->filter('#data_table tbody tr')->text());
    }

    public function testCreateUser(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('new_user'));

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $form = $buttonCrawlerNode->form([
            'user_form[firstName]' => 'firstname',
            'user_form[lastName]' => 'lastname',
            'user_form[email]' => 'test@test.com',
            'user_form[isActive]' => 'on',
        ]);

        $this->client->submit($form);

        $email = self::getMailerMessage(0);

        self::assertResponseRedirects();
        self::assertEmailCount(1);
        self::assertEmailHeaderSame($email, 'To', 'test@test.com');
    }

    public function testUpdateUser(): void
    {
        $id = HashidHelper::encode($this->user->getId());

        $crawler = $this->client->request('GET', $this->router->generate('update_user', ['hashid' => $id]));

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $form = $buttonCrawlerNode->form([
            'form[userDetails][firstName]' => 'firstname',
            'form[userDetails][lastName]' => 'lastname',
            'form[userDetails][email]' => 'test@test.com',
            'form[userDetails][isActive]' => 'on',
        ]);

        $this->client->submit($form);

        self::assertResponseRedirects();

    }

    public function testDeleteUser(): void
    {
        $id = HashidHelper::encode($this->user->getId());

        $this->client->request('GET', $this->router->generate('delete_user', ['hashid' => $id]));

        $this->client->followRedirect();
        self::assertStringContainsString('The user has been deleted', $this->client->getResponse()->getContent());
    }

    public function testListArchived(): void
    {
        $crawler = $this->client->request('GET', $this->router->generate('list_archived_users'));

        self::assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        self::assertGreaterThanOrEqual(1, $crawler->filter('#data_table tbody tr')->count());
        self::assertStringContainsString('deleteduser@test.com', $crawler->filter('#data_table tbody tr')->text());
    }

    public function testListUserHistory(): void
    {
        $id = HashidHelper::encode($this->user->getId());

        $crawler = $this->client->request('GET', $this->router->generate('user_history', ['hashid' => $id]));

        self::assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        self::assertGreaterThanOrEqual(1, $crawler->filter('#data_table tbody tr')->count());
        self::assertStringContainsString('create', $crawler->filter('#data_table tbody tr')->text());
    }

    public function testUniqueUserEmail(): void
    {
        $this->client->xmlHttpRequest('POST', $this->router->generate('user_email_unique'),
            ['user_form' => ['email' => 'test@test.com']]);
        self::assertStringContainsString('true', $this->client->getResponse()->getContent());
    }
}
