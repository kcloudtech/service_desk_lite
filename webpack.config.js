const Encore = require('@symfony/webpack-encore');

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .autoProvidejQuery()
    .addEntry('app', './build/js/entrypoints/app.js')
    .addEntry('dd-upload', './build/js/entrypoints/dd-upload.js')
    .addStyleEntry('css/theme', './build/scss/theme.scss')
    .addStyleEntry('css/auth', './build/scss/auth.scss')
    .addStyleEntry('css/error_pages', './build/scss/error-pages.scss')
    .splitEntryChunks()
    .enableSingleRuntimeChunk()
    .enableSassLoader()
    .enableSourceMaps(!Encore.isProduction())
    .copyFiles({
        from: 'build/js/mdb-admin',
        to: 'js/mdb-admin/[path][name].[ext]'
    })
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .enableVueLoader()


const mainConfig = Encore.getWebpackConfig();

mainConfig.name = 'mainConfig';

Encore.reset();

// Second config for email css
Encore
    .setOutputPath('public/build/email_build')
    .setPublicPath('/build/email_build')
    .addStyleEntry('css/email', './build/email_build/scss/email.scss')
    .splitEntryChunks()
    .enableSingleRuntimeChunk()
    .enableSassLoader()
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())

const emailConfig = Encore.getWebpackConfig();

emailConfig.name = 'emailConfig';

module.exports = [mainConfig, emailConfig];
